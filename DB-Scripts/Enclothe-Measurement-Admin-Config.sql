SELECT `blc_admin_section`.`ADMIN_SECTION_ID`,
    `blc_admin_section`.`CEILING_ENTITY`,
    `blc_admin_section`.`DISPLAY_CONTROLLER`,
    `blc_admin_section`.`DISPLAY_ORDER`,
    `blc_admin_section`.`NAME`,
    `blc_admin_section`.`SECTION_KEY`,
    `blc_admin_section`.`URL`,
    `blc_admin_section`.`USE_DEFAULT_HANDLER`,
    `blc_admin_section`.`ADMIN_MODULE_ID`
select * FROM `enclothe`.`blc_admin_section`;
_xref where ADMIN_ROLE_ID = '-1';

select * from enclothe.blc_admin_module;


use enclothe;

insert into `blc_admin_section` (`ADMIN_SECTION_ID`, `CEILING_ENTITY`, `DISPLAY_CONTROLLER`, `DISPLAY_ORDER`, `NAME`, `SECTION_KEY`, `URL`, `USE_DEFAULT_HANDLER`, `ADMIN_MODULE_ID`) values('-21','com.enclothe.core.measurement.domain.Measurement',NULL,'1700','Measurements','Measurements','/Measurement',NULL,'-3');
commit;
INSERT INTO BLC_ADMIN_SEC_PERM_XREF (ADMIN_SECTION_ID, ADMIN_PERMISSION_ID) VALUES (-21,-12);
INSERT INTO BLC_ADMIN_SEC_PERM_XREF (ADMIN_SECTION_ID, ADMIN_PERMISSION_ID) VALUES (-21,-13);
select * from BLC_ADMIN_MODULE;
select * from blc_admin_section;
select * from blc_admin_permission where description like'%order%';
select * from blc_admin_permission where admin_permission_id in ('-19','-18','-119','-118','-12','-13','-112','-113');

select * from blc_admin_permission_entity;
select * from blc_admin_permission_xref where admin_permission_id  in('-118','-119','-113','-112');
select * from blc_admin_role_permission_xref where admin_role_id = '-1';
select * from blc_admin_section;
select * from BLC_ADMIN_SEC_PERM_XREF;
-13	All Order	0	PERMISSION_ALL_ORDER	ALL
-12	Read Order	0	PERMISSION_READ_ORDER	READ
use enclothe;
insert into enclothe.blc_admin_permission
(select '-113',"Maintain Order",1,"PERMISSION_ORDER","ALL" from dual);
insert into enclothe.blc_admin_permission
(select '-112',"View Order",1,"PERMISSION_ORDER","READ" from dual);
commit;

insert into enclothe.blc_admin_permission_xref
(select '-12','-112' from dual);
commit;
insert into enclothe.blc_admin_permission_xref
(select '-13','-113' from dual);
commit;

insert into enclothe.blc_admin_sec_perm_xref
(select '-21','-113');
commit;

select * from BLC_ADMIN_SEC_PERM_XREF;