package com.broadleafcommerce.rest.api.endpoint.solrreindex;

import javax.annotation.Resource;

import org.broadleafcommerce.common.web.controller.annotation.FrameworkMapping;
import org.broadleafcommerce.common.web.controller.annotation.FrameworkRestController;
import org.broadleafcommerce.core.search.service.solr.index.SolrIndexService;
import org.springframework.http.MediaType;

@FrameworkRestController
@FrameworkMapping(value = "/solr",
produces = {MediaType.APPLICATION_JSON_VALUE})
public class SolrReindexEndpoint {
	
	@Resource(name="blSolrIndexService")
	protected SolrIndexService sis;
	
	@FrameworkMapping("/reindex")
	public void solrReindex(){
		try{
			sis.rebuildIndex();
		}
		catch(Exception e){
			
		}		
	}

}
