package com.community.api.catalog.services;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.broadleafcommerce.common.persistence.EntityConfiguration;
import org.broadleafcommerce.common.sandbox.SandBoxHelper;
import org.broadleafcommerce.core.catalog.dao.CategoryDaoExtensionManager;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.broadleafcommerce.core.catalog.domain.CategoryImpl;
import org.broadleafcommerce.core.catalog.domain.Product;
import org.broadleafcommerce.core.catalog.domain.ProductImpl;
import org.broadleafcommerce.core.catalog.service.CatalogServiceImpl;
import org.hibernate.query.*;
import org.hibernate.jpa.QueryHints;
import org.springframework.stereotype.Service;

import com.community.api.product.dao.EncProductDao;
import com.community.api.product.dao.EncProductDaoImpl;
@Service("encCatalogService")
public class EnclotheCatalogService  {
	
	@PersistenceContext(unitName="blPU")
    protected EntityManager em;
	@Resource(name = "blSandBoxHelper")
    protected SandBoxHelper sandBoxHelper;
	
	 @Resource(name="EncProductDao")
	    protected EncProductDaoImpl encProductDao;
	/*
	 * @Resource(name="blEntityConfiguration") protected EntityConfiguration
	 * entityConfiguration;
	 */
	/*
	 * @Resource(name = "blSandBoxHelper") protected SandBoxHelper sandBoxHelper;
	 * 
	 * @Resource(name = "blCategoryDaoExtensionManager") protected
	 * CategoryDaoExtensionManager extensionManager;
	 */
	 
	 public List<Product>findProductsByCategoryExternalId(String externalId)
	 {
		
		 
		 return encProductDao.readActiveProductsByCategoryExternalId(externalId);
		 
	 }
	
	 public List<Category>findCategoryByExternalId(String externalId)
	 {
		
		 
		 return encProductDao.readActiveCategoryByExternalId(externalId);
		 
	 }
	public List<Product> findProductsMultipleCategory(List<Long> categoryIds,String extenalId){
		
	//	List<Category> categories = new ArrayList<Category>();
		try {
			
		//Query nativequery = em.createNativeQuery("SELECT temp.PRODUCT_ID FROM enclothe.BLC_CATEGORY_PRODUCT_XREF temp where temp.CATEGORY_ID in (:ids)");
		List<BigInteger> productids = new ArrayList<BigInteger>();

Query nativeQuery = em.createNativeQuery("select prod.PRODUCT_ID from enclothe.BLC_PRODUCT prod inner join enclothe.BLC_CATEGORY_PRODUCT_XREF catmapping on prod.PRODUCT_ID = catmapping.PRODUCT_ID inner join BLC_CATEGORY cat on catmapping.CATEGORY_ID = cat.CATEGORY_ID where cat.EXTERNAL_ID =:externalId and catmapping.CATEGORY_ID in (:categoryIds)");
nativeQuery.setParameter("externalId", extenalId);
nativeQuery.setParameter("categoryIds", categoryIds);
productids=nativeQuery.getResultList();

			
			List<Long> pids = new ArrayList<Long>();
			
			
		if(!productids.isEmpty())
		{
			
				
				  for(int i=0;i<productids.size();i++) {
				  pids.add(productids.get(i).longValue()); }
				 
			
			
			 TypedQuery<Product> query = em.createQuery(
		                "select product from org.broadleafcommerce.core.catalog.domain.Product product "
		                        + "where product.id in :ids",
		                Product.class);
			 		      query.setParameter("ids", sandBoxHelper.mergeCloneIds(ProductImpl.class, pids.toArray(new Long[pids.size()])));
			 //query.setParameter("ids", productids);
		        query.setHint(QueryHints.HINT_CACHEABLE, true);
		        query.setHint(QueryHints.HINT_CACHE_REGION, "query.Catalog");

		        return query.getResultList();
		}
		}catch(Exception e)
		{
			System.out.println(e);
			return null;
		}
	return	null;
	

}
}

