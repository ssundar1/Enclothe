package com.community.api.configuration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.profile.web.core.security.RestApiCustomerStateFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelDecisionManagerImpl;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.community.api.service.JwtUserDetailsService;

import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.Filter;

/**
 * @author Elbert Bautista (elbertbautista)
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class ApiSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Log LOG = LogFactory.getLog(ApiSecurityConfig.class);
    
    @Value("${asset.server.url.prefix.internal}")
    protected String assetServerUrlPrefixInternal;
    
    
    @Autowired
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
	private JwtUserDetailsService jwtUserDetailsService;

	@Autowired
	private JwtRequestFilter jwtRequestFilter;

    @Bean(name="blAuthenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManager();
    }
    
    @Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
	}
    
    @Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
    
   @Override
   public void configure(WebSecurity web) throws Exception {
       web.ignoring()
           .antMatchers("/api/**/webjars/**",
               "/api/**/images/favicon-*",
               "/api/**/jhawtcode/**",
               "/api/**/swagger-ui.html",
               "/api/**/swagger-resources/**",
               "/api/**/v2/api-docs");
   }
    
   //.authorizeRequests().antMatchers("/authenticate","/public").permitAll().
   
   @Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {		
		httpSecurity.csrf().disable().authorizeRequests().antMatchers("/authenticate","/authenticateRegister","/authenticatePartner","/enclothe-reseller/registerPartner","/initUser","/refreshTokenWithGivenExpiry","/refreshToken","/public","/encproducts/getallcategories","/swagger-ui.html","/**/webjars/**",
	              "/**/images/favicon-*",
	              "/**/jhawtcode/**",
	              "/**/swagger-ui.html",
	              "/**/swagger-resources/**",
	              "/**/v2/api-docs").permitAll().
				anyRequest().authenticated().and().
				exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}
   
	/* Below is to add cors configuration
	 * httpSecurity.cors().and().csrf().disable().authorizeRequests().antMatchers(
	 * "/authenticate","/public","/swagger-ui.html","/
	////use the existing anthmatchers escape urls from above snippet
	// httpSecurity.addFilterBefore(jwtRequestFilter,
	// UsernamePasswordAuthenticationFilter.class);
*/   

	/*
	 * @Bean CorsConfigurationSource corsConfigurationSource() { final
	 * UrlBasedCorsConfigurationSource source = new
	 * UrlBasedCorsConfigurationSource(); source.registerCorsConfiguration("/**",
	 * new CorsConfiguration().applyPermitDefaultValues()); return source; }
	 */
	/*
	 * @Bean public CorsFilter corsFilter() { UrlBasedCorsConfigurationSource source
	 * = new UrlBasedCorsConfigurationSource();
	 * 
	 * // Allow anyone and anything access. Probably ok for Swagger spec
	 * CorsConfiguration config = new CorsConfiguration();
	 * config.setAllowCredentials(true); config.addAllowedOrigin("*");
	 * config.addAllowedHeader("*"); config.addAllowedMethod("*");
	 * 
	 * source.registerCorsConfiguration("/v2/api-docs", config); return new
	 * CorsFilter(source); }
	 */
    
    @Bean
    public Filter apiCustomerStateFilter() {
        return new RestApiCustomerStateFilter();
    }
    
}
