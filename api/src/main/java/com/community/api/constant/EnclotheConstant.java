package com.community.api.constant;

public interface EnclotheConstant {
	
	// Rest API Error response Code and Description
	String ERROR_CODE_INVALID_REQUEST = "INVALID_REQUEST";
	String ERROR_CODE_INVALID_REQUEST_DESC = "Invalid request Incorrect parameter provided. Check and send the correct input parameter";
	String ERROR_CODE_INVALID_REQUEST_DUPLICATE_EMAIL_DESC = "Email ID already exists";
	String ERROR_CODE_INVALID_REQUEST_DUPLICATE_MOBILENO_DESC = "Mobile No already exists";
	
	String ERROR_CODE_INVALID_USER = "INVALID_USER";
	String ERROR_CODE_INVALID_USER_DESC = "User does not exist. Provide a valid user and resend the request";
	
	String ERROR_CODE_INVALID_PASSWORD = "INVALID_PASSWORD";
	String ERROR_CODE_INVALID_PASSWORD_DESC = "Invalid password. Provide a valid password";
	
	String ERROR_CODE_PROFILE_STATUS_INVALID = "PROFILE_STATUS_INVALID";
	String ERROR_CODE_PROFILE_STATUS_INVALID_DESC = "Profile is not active. The state of the profile is not active.";
	String ERROR_CODE_PROFILE_STATUS_NOT_NEW_DESC = "The state of the profile is not new status.";
	
	String ERROR_CODE_PERMISSION_DENIED = "PERMISSION_DENIED";
	String ERROR_CODE_PERMISSION_DENIED_DESC = "No permission for the requested operation";
			
	String ERROR_CODE_INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";
	String ERROR_CODE_INTERNAL_SERVER_ERROR_DESC = "An internal server error has occurred";
	
	String ERROR_CODE_TOKEN_EXPIRED = "TOKEN_EXPIRED";
	String ERROR_CODE_TOKEN_EXPIRED_DESC = "Expired Token";
	
	String ERROR_CODE_TOKEN_INVALID = "TOKEN_INVALID";
	String ERROR_CODE_TOKEN_INVALID_DESC = "Invalid Token";
	
	String ERROR_CODE_TOKEN_RENEW_TIME_LAPSED = "RENEW_TIME_LAPSED";
	String ERROR_CODE_TOKEN_RENEW_TIME_LAPSED_DESC = "Renew time lapsed";
	
	String ERROR_CODE_REQUIRED_HEADER_NOT_FOUND = "REQUIRED_HEADER_NOT_FOUND";
	String ERROR_CODE_REQUIRED_HEADER_NOT_FOUND_DESC = "Required Header value not found";
	
	String ERROR_CODE_OTP_INVALID = "OTP_INVALID";
	String ERROR_CODE_OTP_INVALID_DESC = "Invalid OTP";
	
	String ERROR_CODE_OTP_EXPIRED = "OTP_EXPIRED";
	String ERROR_CODE_OTP_EXPIRED_DESC = "Expired OTP";
	
	String ERROR_CODE_ACTIVATION_CODE_EXPIRED = "ACTIVATION_CODE_EXPIRED";
	String ERROR_CODE_ACTIVATION_CODE_EXPIRED_DESC = "Activation Code Expired";
	
	String 	ERROR_CODE_ACTIVATION_CODE_CREATE_LOCKED = "ACTIVATION_CODE_CREATE_LOCKED";
	String 	ERROR_CODE_ACTIVATION_CODE_CREATE_LOCKED_DESC = "You have exceeded maximum number of activation code creation. Please try again later";
	
	String 	ERROR_CODE_ACTIVATION_CODE_LOCKED = "ACTIVATION_CODE_LOCKED";
	String 	ERROR_CODE_ACTIVATION_CODE_LOCKED_DESC = "You have exceeded maximum number of activation attempts. Please try again later";
	
	String 	ERROR_CODE_VERIFICATION_CODE_CREATE_LOCKED = "VERIFICATION_CODE_CREATE_LOCKED";
	String 	ERROR_CODE_VERIFICATION_CODE_CREATE_LOCKED_DESC = "You have exceeded maximum number of verification code creation. Please try again later";
	
	String ERROR_CODE_VERIFICATION_CODE_EXPIRED = "VERIFICATION_CODE_EXPIRED";
	String ERROR_CODE_VERIFICATION_CODE_EXPIRED_DESC = "Verification Code Expired";
	
	String 	ERROR_CODE_VERIFICATION_CODE_LOCKED = "VERIFICATION_CODE_LOCKED";
	String 	ERROR_CODE_VERIFICATION_CODE_LOCKED_DESC = "You have exceeded maximum number of verification attempts. Please try again later";
	
	String ERROR_CODE_VERIFICATION_CODE_INVALID = "VERIFICATION_CODE_INVALID";
	String ERROR_CODE_VERIFICATION_CODE_INVALID_DESC = "Invalid Verification Code";
	
	String ERROR_CODE_EMPTY_FILE = "EMPTY_FILE";
	String ERROR_CODE_EMPTY_FILE_DESC = "file was empty";
	
	String ERROR_CODE_INVALID_EMAIL = "INVALID_EMAIL";
	String ERROR_CODE_INVALID_EMAIL_DESC = "Invalid email address";
	
	String ERROR_CODE_INVALID_EMAIL_CONTENT = "INVALID_EMAIL_CONTENT";
	String ERROR_CODE_INVALID_EMAIL_CONTENT_DESC = "Email content is empty";
	
	String ERROR_CODE_INVALID_EMAIL_SUBJECT = "INVALID_EMAIL_CONTENT";
	String ERROR_CODE_INVALID_EMAIL_SUBJECT_DESC = "Email Subject is empty";
	
	String ERROR_CODE_DUPLICATE_LIKE = "DUPLICATE_LIKE";
	String ERROR_CODE_DUPLICATE_LIKE_DESC = "Already liked the Content";
	
		// User Type
	String USER_TYPE_ADMIN = "ADMIN";
	String USER_TYPE_USER = "USER";
	
	// User Status
	String USER_STATUS_NEW = "NEW";
	String USER_STATUS_PENDING = "PENDING";
	String USER_STATUS_ACTIVE = "ACTIVE";
	String USER_STATUS_IN_ACTIVE = "IN_ACTIVE";
	
		//Country
	String COUNTRY_CODE_INDIA = "IN";
	
}