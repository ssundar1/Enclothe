package com.community.api.enccustomer.dao;

import java.util.List;

import com.enclothe.core.customer.domain.EncCustomer;
import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.Measurement;

public interface EncCustomerDao {
	
	EncCustomer save(EncCustomer customer);

	void delete(EncCustomer customer);

	EncCustomer create();

	 public EncCustomer readCustomerById(Long id);


}
