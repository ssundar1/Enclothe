package com.community.api.enccustomer.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.broadleafcommerce.common.persistence.ArchiveStatus;
import org.broadleafcommerce.common.persistence.EntityConfiguration;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.domain.CustomerImpl;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import com.enclothe.core.customer.domain.EncCustomer;
import com.enclothe.core.customer.domain.EncCustomerImpl;
import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.Measurement;

@Repository("encCustomerDao")
public class EncCustomerDaoImpl implements EncCustomerDao {

	@PersistenceContext(unitName = "blPU")
    protected EntityManager em;
	
	@Resource(name = "blEntityConfiguration")
    protected EntityConfiguration entityConfiguration;
	
	 public EncCustomer readCustomerById(Long id) {
	        return em.find(EncCustomerImpl.class, id);
	    }
    
	
			
	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public EntityConfiguration getEntityConfiguration() {
		return entityConfiguration;
	}

	public void setEntityConfiguration(EntityConfiguration entityConfiguration) {
		this.entityConfiguration = entityConfiguration;
	}

	@Override
    public EncCustomer save(EncCustomer customer) {
        return em.merge(customer);
    }
	
	@Override
	public void delete(EncCustomer customer){
		ArchiveStatus as = new ArchiveStatus();
		as.setArchived('Y');
		customer.setDeactivated(true);
		em.merge(customer);
	}

    @Override
    public EncCustomer create() {
    	System.out.println("entiteyconf"+entityConfiguration);
    	
    	return (EncCustomer) entityConfiguration.createEntityInstance("com.enclothe.core.customer.domain.EncCustomer");
    }
}
