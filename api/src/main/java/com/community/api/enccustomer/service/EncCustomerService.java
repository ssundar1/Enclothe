package com.community.api.enccustomer.service;

import java.util.List;

import com.enclothe.core.customer.domain.EncCustomer;
import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.Measurement;

public interface EncCustomerService {
	

	EncCustomer createEncCustomer();


		EncCustomer readEncCustomerById(Long customer_id);


		EncCustomer saveEncCustomer(EncCustomer customer);

		

	


}
