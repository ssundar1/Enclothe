package com.community.api.enccustomer.service;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import java.util.List;

import org.springframework.stereotype.Service;
import org.broadleafcommerce.common.id.domain.IdGenerationImpl;
//import org.broadleafcommerce.profile.core.service.IdGenerationService;

import com.community.api.enccustomer.dao.EncCustomerDao;
import com.community.api.measurement.dao.EncCustomerMeasurementDao;
import com.community.api.measurement.dao.MeasurementDao;
import com.enclothe.core.customer.domain.EncCustomer;
import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.Measurement;

import javax.annotation.Resource;

@Service("encCustomerService")
public class EncCustomerServiceImpl implements EncCustomerService {

	@Resource(name = "encCustomerDao")
	protected EncCustomerDao encCustomerDao;

	@Override
	public EncCustomer createEncCustomer() {
		return encCustomerDao.create();
	}

	@Override
	public EncCustomer readEncCustomerById(Long customer_id) {
		if (customer_id != null) {
			return encCustomerDao.readCustomerById(customer_id);
		} else {
			System.out.println(
					"Customer id is null...please check  before passing null customer id to EncCustomerService readyEncCustomerById");
			return null;

		}

	}

	@Override
	public EncCustomer saveEncCustomer(EncCustomer customer) {
		return encCustomerDao.save(customer);
	}

	/*
	 * @Resource(name="blIdGenerationService") protected IdGenerationImpl
	 * idGenerationService;
	 */

}
