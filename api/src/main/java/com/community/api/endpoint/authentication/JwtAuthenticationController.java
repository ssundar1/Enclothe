package com.community.api.endpoint.authentication;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.broadleafcommerce.core.order.service.OrderService;
import org.broadleafcommerce.jwt.model.JwtRequest;
import org.broadleafcommerce.jwt.model.JwtResponse;
import org.broadleafcommerce.jwt.model.JwtResponseWithFUVersion;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.domain.CustomerAttribute;
import org.broadleafcommerce.profile.core.domain.CustomerAttributeImpl;
import org.broadleafcommerce.profile.core.domain.CustomerPhone;
import org.broadleafcommerce.profile.core.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.broadleafcommerce.common.audit.Auditable;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.broadleafcommerce.core.geolocation.GeolocationDTO;
import org.broadleafcommerce.core.order.domain.Order;

import com.broadleafcommerce.rest.api.wrapper.CategoryWrapper;
import com.broadleafcommerce.rest.api.wrapper.CustomerWrapper;
import com.broadleafcommerce.rest.api.wrapper.OrderWrapper;
import com.community.api.catalog.services.EnclotheCatalogService;
import com.community.api.configuration.JwtTokenUtil;
import com.community.api.reseller.service.EnclotheResellerService;
import com.community.api.service.JwtUserDetailsService;
import com.community.api.wrapper.EnclotheCustomerWrapper;
import com.enclothe.core.reseller.domain.EncReseller;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@Resource(name="blCustomerService")
	protected CustomerService customerService;
	
	@Resource(name="blOrderService")
	protected OrderService orderService;
	
	@Resource(name="encCatalogService")
	protected EnclotheCatalogService enclotheCatalogService;
	
	@Resource(name = "enclotheResellerService")
	protected EnclotheResellerService enclotheResellerService;
	
	 @Autowired
	 private ApplicationContext context;


	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
		
		//authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
		//just verify the username not in password. in future we need to modify this.
		System.out.println("Find user details for :"+ authenticationRequest.getUsername() );
		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);
		JwtResponse jwt = new JwtResponse(token);
		

		Customer customer = customerService.readCustomerByEmail(userDetails.getUsername());

		CustomerWrapper customerWrapper = new CustomerWrapper();
		OrderWrapper orderWrapper = new OrderWrapper();

		//customerWrapper.wrapDetails(customer, null);
		
		
		if(customer!=null)
		{
			
			
			
			CustomerAttribute customerAttributes = new CustomerAttributeImpl();
			HashMap hm = new HashMap();
			customerAttributes.setCustomer(customer);
			customerAttributes.setName("Authentication_Source");
			customerAttributes.setValue(authenticationRequest.getAuthentication_source());
			customerAttributes.setCustomer(customer);
			customerAttributes.setName("app_id");
			customerAttributes.setValue(authenticationRequest.getApp_id());
			hm.put("Authentication_Source",customerAttributes)	;
			/*
			 * customerAttributes.setName("deviceId");
			 * customerAttributes.setValue(authenticationRequest.getDeviceid());
			 * customerAttributes.setName("phoneno");
			 * customerAttributes.setValue(authenticationRequest.getPhoneno());
			 * customerAttributes.setName("app_id");
			 * customerAttributes.setValue(authenticationRequest.getApp_id());
			 * customerAttributes.setName("geolocation");
			 * customerAttributes.setValue(authenticationRequest.getGeolocation().toString()
			 * ); customerAttributes.setName("customer_type");
			 * customerAttributes.setValue(authenticationRequest.getCustomer_type());
			 */
			//customer.setCustomerAttributes(hm);
			Auditable cust = customer.getAuditable();
			cust.setDateUpdated(new Date());
			cust.setUpdatedBy(customer.getId());
			
			customerService.saveCustomer(customer);
			customerWrapper.wrapDetails(customer, null);

			jwt.setCustomerWrapper(customerWrapper);

			Order cart = orderService.findCartForCustomer(customer);
			if(cart!=null)
			{
		//	orderWrapper.wrapDetails(cart, null);
		
		jwt.setCartCount(cart.getItemCount());	
			}
		}
		
		return ResponseEntity.ok(jwt);
	}
	
	@RequestMapping(value = "/refreshToken", method = RequestMethod.POST)
	public ResponseEntity<?> refreshToken(HttpServletRequest request,@RequestParam(value = "token", required = true) String token,@RequestParam(value = "app_version", required = false) String app_version) throws Exception {
		
		
		String userName =null;
		JwtResponseWithFUVersion jwt =null;
		EnclotheCustomerWrapper customerWrapper = new EnclotheCustomerWrapper();
		OrderWrapper orderWrapper = new OrderWrapper();

		try {
			userName = jwtTokenUtil.getUsernameFromToken(token);
		} catch (IllegalArgumentException e) {
			System.out.println("Unable to get JWT Token IllegalArgumentException START");
			System.out.println(e.getMessage());
			System.out.println("Unable to get JWT Token IllegalArgumentException END");
		} catch (ExpiredJwtException e) {
			System.out.println("Unable to get JWT Token ExpiredJwtException START");
			userName = e.getClaims().getSubject();
			System.out.println(userName);
			System.out.println("Unable to get JWT Token ExpiredJwtException END");		
		}
		catch(Exception e) {
			System.out.println("Inside Exception in refresh token START");
			System.out.println(e.getMessage());
			System.out.println("Inside Exception in refresh token END");
		}
		finally {
			
			try {
				Customer customer = customerService.readCustomerByEmail(userName);
	
				if(customer.isDeactivated()) {
					return   new ResponseEntity<Object>(HttpStatus.UNAUTHORIZED);
				}
				else {
					JwtRequest authenticationRequest = new JwtRequest();
					authenticationRequest.setApp_version(app_version);
					jwt =authenticateCustomer(customer,authenticationRequest);
					return ResponseEntity.ok(jwt);
				}
			}
			catch(Exception e) {
				System.out.println("Inside Finally Block Exception in refresh token START");
				System.out.println(e.getMessage());
				System.out.println("Inside Finally Block Exception in refresh token END");
				return null;
			}
		}
	}
	@RequestMapping(value = "/authenticateRegister", method = RequestMethod.POST)
	public ResponseEntity<?> authenticateRegister(@RequestBody JwtRequest authenticationRequest) throws Exception {

		Customer customer = customerService.readCustomerByEmail(authenticationRequest.getUsername());
		if(customer!=null && customer.isDeactivated()) {
			return   new ResponseEntity<Object>(HttpStatus.UNAUTHORIZED);
		}
		if (customer == null || customer.getUsername() == null) {
			customer = customerService.createNewCustomer();

			customer.setUsername(authenticationRequest.getUsername());

			if (authenticationRequest.getFirstName() == null)
				customer.setFirstName("unknown");
			else
				customer.setFirstName(authenticationRequest.getFirstName());

			if (authenticationRequest.getLastName() == null)
				customer.setLastName("unknown");
			else
				customer.setLastName(authenticationRequest.getLastName());

			customer.setEmailAddress(authenticationRequest.getUsername());
			customer = customerService.registerCustomer(customer, authenticationRequest.getPassword(),
					authenticationRequest.getPassword());
		}
		JwtResponseWithFUVersion jwt;
		jwt = authenticateCustomer(customer, authenticationRequest);
		
		return ResponseEntity.ok(jwt);
	}
	@RequestMapping(value = "/authenticatePartner", method = RequestMethod.POST)
	public ResponseEntity<?> authenticatePartner(@RequestBody JwtRequest authenticationRequest) throws Exception {
		// Need to verify Token from firebase
		Customer customer = customerService.readCustomerByEmail(authenticationRequest.getUsername());
		if(customer!=null && customer.isDeactivated()) {
			return   new ResponseEntity<Object>(HttpStatus.UNAUTHORIZED);
		}
		if (customer == null || customer.getUsername() == null) {
			String resp = "Partner Not Created";
			return   ResponseEntity.ok(resp);
		}
		JwtResponseWithFUVersion jwt;
		jwt = authenticateCustomer(customer, authenticationRequest);
		
		return ResponseEntity.ok(jwt);
	}
	@RequestMapping(value = "/initUser", method = RequestMethod.POST)
	public ResponseEntity<?> initUser(HttpServletRequest request,@RequestParam(value = "token", required = false) String token,@RequestParam(value = "app_version", required = true) String app_version,@RequestParam(value = "userName", required = false) String userName) throws Exception {
		
		List<CategoryWrapper> out = new ArrayList<CategoryWrapper>();

		
		
		JwtResponseWithFUVersion jwt =null;
		CustomerWrapper customerWrapper = new CustomerWrapper();
		OrderWrapper orderWrapper = new OrderWrapper();
		Customer customer=null;
		List<Category> categories = new ArrayList<Category>(); 
		categories = enclotheCatalogService.findCategoryByExternalId("enclothe");
		out = this.getCategoryWrapperList(categories, request,"enclothe");
		if(token!=null || userName!=null) {
		try {
			if(token!=null && userName == null)
			userName = jwtTokenUtil.getUsernameFromToken(token);
			
		} catch (IllegalArgumentException e) {
			System.out.println("Unable to get JWT Token");
		} catch (ExpiredJwtException e) {
			userName = e.getClaims().getSubject();
		}finally {
			
	
		 customer = customerService.readCustomerByEmail(userName);

		if(customer.isDeactivated()) {
			return   new ResponseEntity<Object>(HttpStatus.UNAUTHORIZED);
			}
			else {
				JwtRequest authenticationRequest = new JwtRequest();
				authenticationRequest.setApp_version(app_version);
				jwt =authenticateCustomer(customer,authenticationRequest);
							
								jwt.setCategoryList(out);
								return ResponseEntity.ok(jwt);
							
					}
		

	}
		
		
		
	}else if(app_version!=null)
	{
		jwt = checkForceAndOptionalUpgrade(app_version,new JwtResponseWithFUVersion(null));
		jwt.setCategoryList(out);
		return ResponseEntity.ok(jwt);

	}
		
		return ResponseEntity.ok(jwt);
	}

	@RequestMapping(value = "/refreshTokenWithGivenExpiry", method = RequestMethod.POST)
	public ResponseEntity<?> refreshTokenWithGivenExpiry(HttpServletRequest request,@RequestParam(value = "username", required = true) String username,@RequestParam(value = "tokenExpiryMins", required = true) Long tokenExpiryMins) throws Exception {
		try {
			Customer customer = customerService.readCustomerByEmail(username);
			final UserDetails userDetails = userDetailsService
					.loadUserByUsername(customer.getUsername());
	
			final String token = jwtTokenUtil.generateTokenForGivenExpiry(userDetails,tokenExpiryMins);
			System.out.println(customer.getUsername()+" Token Expires at :"+ jwtTokenUtil.getExpirationDateFromToken(token));
			JwtResponse jwt = new JwtResponse(token);
			return ResponseEntity.ok(jwt);
		}
		catch(Exception e) {
			System.out.println("Exception in refreshTokenWithGivenExpiry STARTS");
			System.out.println(e.getMessage());
			System.out.println("Exception in refreshTokenWithGivenExpiry ENDS");
			return (ResponseEntity<?>) ResponseEntity.notFound();
		}
	}
	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
	private JwtResponseWithFUVersion checkForceAndOptionalUpgrade(String app_version, JwtResponseWithFUVersion jwt) {
		app_version = app_version.replaceAll("[.]","");
		String server_app_version = jwt.getForceUpgradeVersion();
		server_app_version = server_app_version.replaceAll("[.]", "");
		Double clientVersion = Double.parseDouble(app_version);
		Double forceUpgradeVersion = Double.parseDouble(server_app_version);
		if(clientVersion<=forceUpgradeVersion)
		{
			jwt.setForceUpgrade(true);
		jwt.setOptionalUpgrade(false);
		} 
	return jwt;	
	}
	private JwtResponseWithFUVersion authenticateCustomer(Customer customer, JwtRequest authenticationRequest) {
		// TODO Afinal UserDetails userDetails = userDetailsService
		final UserDetails userDetails = userDetailsService.loadUserByUsername(customer.getUsername());

		final String token = jwtTokenUtil.generateTokenForGivenExpiry(userDetails, 2);
		JwtResponseWithFUVersion jwt = new JwtResponseWithFUVersion(token);
		CustomerWrapper customerWrapper = new CustomerWrapper();
		EnclotheCustomerWrapper encCustomerWrapper = new EnclotheCustomerWrapper();
		
		OrderWrapper orderWrapper = new OrderWrapper();

		if (customer != null) {

			CustomerAttribute customerAttributes = new CustomerAttributeImpl();
			customerAttributes.setCustomer(customer);
			if(authenticationRequest.getAuthentication_source()!=null)
			{
			customerAttributes.setName("Authentication_Source");
			customerAttributes.setValue(authenticationRequest.getAuthentication_source());
			}
			customerAttributes.setCustomer(customer);
			if(authenticationRequest.getApp_id()!=null) {
			customerAttributes.setName("app_id");
			customerAttributes.setValue(authenticationRequest.getApp_id());
			}

			Auditable cust = customer.getAuditable();
			cust.setDateUpdated(new Date());
			cust.setUpdatedBy(customer.getId());

			customerService.saveCustomer(customer);
			encCustomerWrapper.wrapDetails(customer, null);

			jwt.setCustomerWrapper(encCustomerWrapper);

			Order cart = orderService.findCartForCustomer(customer);
			if (cart != null) {
				// orderWrapper.wrapDetails(cart, null);

				jwt.setCartCount(cart.getItemCount());
			}
			
			List<EncReseller> encReseller = enclotheResellerService.findByCustomerId(customer.getId());
			System.out.println("encReseller size="+encReseller.size());
			
			  if(encReseller != null && encReseller.size() > 0) {
			  
			  	ListIterator<EncReseller> listIterator = encReseller.listIterator();
			  
			  	while(listIterator.hasNext()) { 
				  EncReseller encResellerList = listIterator.next();
				  System.out.println("encReseller.getUserType()==="+encResellerList.getUserType()); 
				  jwt.setUserType(encResellerList.getUserType());
				  jwt.setUserPermission(encResellerList.getUserPermission()); 
			  	} 
			 }
			 
		}
		if ((authenticationRequest.getApp_version()!=null)) {

			jwt = checkForceAndOptionalUpgrade(authenticationRequest.getApp_version(), jwt);

		} else {
			jwt.setForceUpgrade(true);
			jwt.setOptionalUpgrade(false);
		}
		return jwt;
	}
	public List<CategoryWrapper> getCategoryWrapperList(List<Category> categories,HttpServletRequest request,String categoryExternalId){
		List<CategoryWrapper> out = new ArrayList<CategoryWrapper>();

		for (Category category : categories) {
			if(category.getExternalId()!=null && category.getExternalId().equalsIgnoreCase(categoryExternalId)) {
			CategoryWrapper wrapper = (CategoryWrapper) context.getBean(CategoryWrapper.class.getName());
			wrapper.wrapSummary(category, request);
			out.add(wrapper);
			}
		}
		return out;
	}
}