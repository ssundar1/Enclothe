package com.community.api.endpoint.cart;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpStatus;
import org.broadleafcommerce.common.web.controller.annotation.FrameworkMapping;
import org.broadleafcommerce.core.checkout.service.exception.CheckoutException;
import org.broadleafcommerce.core.checkout.service.workflow.CheckoutResponse;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.broadleafcommerce.rest.api.endpoint.checkout.CheckoutEndpoint;
import com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException;
import com.broadleafcommerce.rest.api.wrapper.OrderWrapper;
import com.community.api.notification.service.EnclotheFcmNotificationService;
import com.community.api.notification.service.EnclotheFcmTokenService;
import com.enclothe.core.notification.domain.FcmSessionToken;

@RestController
@RequestMapping(value = "/encCart/checkout", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })

public class EnclotheCartCheckoutEndpoint extends CheckoutEndpoint{
	
	@Resource(name = "enclotheFcmTokenService")
	EnclotheFcmTokenService enclotheFcmTokenService;
	
	@Resource(name = "enclotheFcmNotificationService")
	EnclotheFcmNotificationService enclotheFcmNotificationService;

	@RequestMapping(value = "", method = RequestMethod.POST)
    public OrderWrapper performCheckout(HttpServletRequest request,
                                        @RequestParam("cartId") Long cartId) {
        Order cart = orderService.findOrderById(cartId);
        if (cart != null) {
            try {
                CheckoutResponse response = checkoutService.performCheckout(cart);
                Order order = response.getOrder();
                String orderId= order.getOrderNumber();
                OrderWrapper wrapper = (OrderWrapper) context.getBean(OrderWrapper.class.getName());
                wrapper.wrapDetails(order, request);
                Customer customer = order.getCustomer();
                if(customer!=null && customer.isDeactivated()) {
                	throw BroadleafWebServicesException.build(HttpStatus.SC_NOT_FOUND)
                    .addMessage(BroadleafWebServicesException.CUSTOMER_NOT_FOUND);
    			}
    			else {
    				System.out.println("customer.getId() = "+customer.getId());
    				FcmSessionToken fcmSessionToken = enclotheFcmTokenService.getFcmSessionTokenByUsername(customer.getId());
                    String fcmToken = fcmSessionToken.getFcmToken();
                    String userName = fcmSessionToken.getLoginName();
                    System.out.println("User Name "+userName+" orderId "+orderId+" fcmToken "+fcmToken);
                    enclotheFcmNotificationService.sendOrderMessageToUser(orderId, userName, fcmToken);
                    System.out.println("Notification Send successfully");
    			}
                
                return wrapper;
            } catch (CheckoutException e) {
                throw BroadleafWebServicesException.build(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                        .addMessage(BroadleafWebServicesException.CHECKOUT_PROCESSING_ERROR);
            }
        }

        throw BroadleafWebServicesException.build(HttpStatus.SC_NOT_FOUND)
                .addMessage(BroadleafWebServicesException.CART_NOT_FOUND);

    }
}
