/*
 * Copyright 2008-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.community.api.endpoint.cart;

import org.apache.commons.lang.ObjectUtils;
import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.common.web.controller.annotation.FrameworkMapping;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.broadleafcommerce.core.catalog.domain.Product;
import org.broadleafcommerce.core.order.domain.DiscreteOrderItemImpl;
import org.broadleafcommerce.core.order.domain.NullOrderImpl;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.domain.OrderItem;
import org.broadleafcommerce.core.order.domain.OrderItemImpl;
import org.broadleafcommerce.core.order.service.call.OrderItemRequestDTO;
import org.broadleafcommerce.core.order.service.exception.AddToCartException;
import org.broadleafcommerce.core.order.service.exception.ItemNotFoundException;
import org.broadleafcommerce.core.order.service.exception.RemoveFromCartException;
import org.broadleafcommerce.core.order.service.exception.UpdateCartException;
import org.broadleafcommerce.core.pricing.service.exception.PricingException;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.service.CustomerService;
import org.broadleafcommerce.profile.web.core.CustomerState;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.broadleafcommerce.rest.api.endpoint.order.CartEndpoint;
import com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException;
import com.broadleafcommerce.rest.api.wrapper.CustomerAddressWrapper;
import com.broadleafcommerce.rest.api.wrapper.OrderItemWrapper;
import com.broadleafcommerce.rest.api.wrapper.OrderWrapper;
import com.broadleafcommerce.rest.api.wrapper.ProductWrapper;
import com.community.api.wrapper.EnclotheOrderItemWrapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


/**
 * This is a reference REST API endpoint for cart. This can be modified, used as is, or removed. 
 * The purpose is to provide an out of the box RESTful cart service implementation, but also 
 * to allow the implementor to have fine control over the actual API, URIs, and general JAX-RS annotations.
 * 
 * @author Sundararajan Suriyaprakash
 *
 */
@RestController
@RequestMapping(value = "/enc-cart-custid",
                produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })

public class EnclotheCartEndpoint extends CartEndpoint {
	
	@Resource(name="blCustomerService")
    protected CustomerService customerService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity findCartForEnclotheCustomer(HttpServletRequest request, @RequestParam(value = "customerId", required = true) long customerId) {
        try {
        	
            Customer customer = customerService.readCustomerById(customerId);
            OrderWrapper wrapper = (OrderWrapper) context.getBean(OrderWrapper.class.getName());

            if(customer == null)
            {
            	customer = CustomerState.getCustomer(request);
            }
        	

        	
            Order cart =  orderService.findCartForCustomer(customer);
            if (cart == null) {
            	cart = orderService.createNewCartForCustomer(customer);
            	wrapper.wrapDetails(cart, request);
                return new ResponseEntity(wrapper,HttpStatus.NO_CONTENT);
            }
            wrapper.wrapDetails(cart, request);
           
        	return new ResponseEntity(wrapper,HttpStatus.OK);
        } catch (Exception e) {
            // if we failed to find the cart, create a new one
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    
    @RequestMapping(value = "/additemtocart", method = RequestMethod.PUT)
    		//consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public OrderWrapper addItemToCart(HttpServletRequest request,@RequestParam(value = "customerId", required = true) long customerId,@RequestParam(value = "productId", required = true) long productId,@RequestParam(value = "quantity", required = true) int quantity) {
    	Customer customer = customerService.readCustomerById(customerId);
    	OrderWrapper orderwrapper = (OrderWrapper) context.getBean(OrderWrapper.class.getName());
    	Product product=  catalogService.findProductById(productId);
    	OrderItem orderItem;
    	//= orderItemService.createDiscreteOrderItem(itemRequest)

    	OrderItemRequestDTO orderItemRequestDTO = new OrderItemRequestDTO();


    	Order cart;
    	if(customer != null) {
    		cart =  orderService.findCartForCustomer(customer);
    		 if (cart == null) 
             	cart = orderService.createNewCartForCustomer(customer);
    		if (cart!=null && product!=null) {
    			orderItemRequestDTO.setProductId(productId);

    			//orderItemRequestDTO.setOrderItemId(null);
    			orderItemRequestDTO.setCategoryId(product.getCategory().getId());   
    			orderItemRequestDTO.setQuantity(new Integer(quantity)); 
    			//orderItemRequestDTO.setOverrideRetailPrice(new Money(new BigDecimal(price)));

    			try {
    				cart =orderService.addItem(cart.getId(), orderItemRequestDTO, false);
    				cart = orderService.save(cart, true);

    				orderwrapper.wrapDetails(cart, request);
    			} catch (AddToCartException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}catch (Exception e)
    			{
    				e.printStackTrace();
    			}


    		}
    	}


    	return orderwrapper;

    }
    @RequestMapping(value = "/additemtocartjason", method = RequestMethod.POST,
	consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
public OrderWrapper addItemToCartJason(HttpServletRequest request,
		@RequestBody EnclotheOrderItemWrapper enclotheOrderItemDTO) {
Customer customer = customerService.readCustomerById(enclotheOrderItemDTO.getCustomerId());
OrderWrapper orderwrapper = (OrderWrapper) context.getBean(OrderWrapper.class.getName());
Product product=  catalogService.findProductById(enclotheOrderItemDTO.getProductId());
OrderItem orderItem;
//= orderItemService.createDiscreteOrderItem(itemRequest)

OrderItemRequestDTO orderItemRequestDTO = new OrderItemRequestDTO();


Order cart;
if(customer != null) {
	cart =  super.orderService.findCartForCustomer(customer);
	 if (cart == null) 
     	cart = (Order) super.createNewCartForCustomer(request);
	if (cart!=null && product!=null) {
		orderItemRequestDTO.setProductId(product.getId());

		//orderItemRequestDTO.setOrderItemId(null);
		orderItemRequestDTO.setCategoryId(product.getCategory().getId());   
		orderItemRequestDTO.setQuantity(new Integer(enclotheOrderItemDTO.getQuantity())); 
		//orderItemRequestDTO.setOverrideRetailPrice(new Money(new BigDecimal(price)));

		try {
			cart =orderService.addItem(cart.getId(), orderItemRequestDTO, false);
			cart = orderService.save(cart, true);

			orderwrapper.wrapDetails(cart, request);
		} catch (AddToCartException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e)
		{
			e.printStackTrace();
		}

	}
}


return orderwrapper;

}	
    
    @RequestMapping(value = "{customerId}/{cartId}/items/{itemId}", method = RequestMethod.DELETE)
    public OrderWrapper removeItemFromOrder(HttpServletRequest request,
                                            @PathVariable("itemId") Long itemId,
                                            @PathVariable("cartId") Long cartId,
                                            @PathVariable("customerId") Long customerId,
                                            @RequestParam(value = "priceOrder", required = false, defaultValue = "true") Boolean priceOrder) throws Exception {

        Order cart = validateCartForCustomer(cartId,customerId);
        
        try {
            Order order = orderService.removeItem(cart.getId(), itemId, priceOrder);
            order = orderService.save(order, priceOrder);

            OrderWrapper wrapper = (OrderWrapper) context.getBean(OrderWrapper.class.getName());
            wrapper.wrapDetails(order, request);

            return wrapper;
        } catch (PricingException e) {
            throw new Exception(BroadleafWebServicesException.CART_NOT_FOUND);
        } catch (RemoveFromCartException e) {
            if (e.getCause() instanceof ItemNotFoundException) {
                throw new Exception(BroadleafWebServicesException.CART_ITEM_NOT_FOUND);

            } else {
                throw new Exception(BroadleafWebServicesException.UNKNOWN_ERROR);
            }
        }
    }

    
    @RequestMapping(value = "updatecartitemcount/{customerId}/{cartId}/items/{itemId}", method = RequestMethod.PUT)
    public OrderWrapper updateItemQuantity(HttpServletRequest request,
                                           @PathVariable("itemId") Long itemId,
                                           @PathVariable("cartId") Long cartId,
                                           @PathVariable("customerId") Long customerId,
                                           @RequestParam("quantity") Integer quantity,
                                           @RequestParam(value = "priceOrder", required = false, defaultValue = "true") Boolean priceOrder) throws Exception {

        Order cart = validateCartForCustomer(cartId,customerId);
        
       
    	   
        
        try {
            OrderItemRequestDTO orderItemRequestDTO = new OrderItemRequestDTO();
            orderItemRequestDTO.setOrderItemId(itemId);
            orderItemRequestDTO.setQuantity(quantity);
            Order order = orderService.updateItemQuantity(cart.getId(), orderItemRequestDTO, priceOrder);
            order = orderService.save(order, priceOrder);

            OrderWrapper wrapper = (OrderWrapper) context.getBean(OrderWrapper.class.getName());
            wrapper.wrapDetails(order, request);

            return wrapper;
        } catch (UpdateCartException e) {
            if (e.getCause() instanceof ItemNotFoundException) {
                throw new Exception(BroadleafWebServicesException.CART_ITEM_NOT_FOUND);
            } else {
            	 throw new Exception(BroadleafWebServicesException.UPDATE_CART_ERROR);
            }
        } catch (RemoveFromCartException e) {
            if (e.getCause() instanceof ItemNotFoundException) {
            	 throw new Exception(BroadleafWebServicesException.CART_ITEM_NOT_FOUND);
            } else {
            	throw new Exception(BroadleafWebServicesException.UPDATE_CART_ERROR);
            }
        } catch (PricingException pe) {
        	throw new Exception(BroadleafWebServicesException.CART_PRICING_ERROR);
        }
    }
    
    
    public Order validateCartForCustomer(Long cartId, Long customerId) throws Exception {
    	
        Order cart = orderService.findOrderById(cartId);
        Customer customer = customerService.readCustomerById(customerId);
        Order customerCart = orderService.findCartForCustomer(customer);
        if (cart == null || cart instanceof NullOrderImpl) {
            throw new Exception(BroadleafWebServicesException.CART_NOT_FOUND);

        }
        
        if (customer != null && ObjectUtils.notEqual(customer.getId(), cart.getCustomer().getId())) {
            throw new Exception(BroadleafWebServicesException.CART_CUSTOMER_MISMATCH);
        }
        return cart;
    }
    
  
	/*
	 * @Override
	 * 
	 * @RequestMapping(value = "/cust-cart-id", method = RequestMethod.GET) public
	 * OrderWrapper findCartForCustomer(HttpServletRequest request) { try { return
	 * super.findCartForCustomer(request); } catch (Exception e) { // if we failed
	 * to find the cart, create a new one return createNewCartForCustomer(request);
	 * } }
	 */
	 
}
