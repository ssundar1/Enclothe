
/*Copyright 2008-2012 the original author or authors.

Licensed under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy of
the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations under
the License.*/


package com.community.api.endpoint.customers;

import org.broadleafcommerce.common.web.controller.annotation.FrameworkMapping;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.broadleafcommerce.core.catalog.domain.Product;
import org.broadleafcommerce.core.catalog.service.CatalogService;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.domain.CustomerAddress;
import org.broadleafcommerce.profile.core.domain.CustomerPhone;
import org.broadleafcommerce.profile.core.domain.CustomerPhoneImpl;
import org.broadleafcommerce.profile.core.domain.Phone;
import org.broadleafcommerce.profile.core.domain.PhoneImpl;
import org.broadleafcommerce.profile.core.service.CustomerPhoneService;
import org.broadleafcommerce.profile.core.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.broadleafcommerce.rest.api.endpoint.BaseEndpoint;
import com.broadleafcommerce.rest.api.endpoint.customer.CustomerEndpoint;
import com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException;
import com.broadleafcommerce.rest.api.wrapper.CustomerAddressWrapper;
import com.broadleafcommerce.rest.api.wrapper.CustomerWrapper;
import com.broadleafcommerce.rest.api.wrapper.PhoneWrapper;
import com.broadleafcommerce.rest.api.wrapper.ProductWrapper;
import com.community.api.sms.handler.SMSHandler;
import com.community.api.sms.messages.SMSAccountActivationMessage;
import com.community.api.sms.messages.SMSAccountVerificationMessage;
import com.community.api.sms.messages.SMSMessage;
import com.community.api.wrapper.EnclotheCustomerWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource; 
import javax.servlet.http.HttpServletRequest;


/**
 * This is a reference REST API endpoint for cart. This can be modified, used as
 * is, or removed. The purpose is to provide an out of the box RESTful cart
 * service implementation, but also to allow the implementor to have fine
 * control over the actual API, URIs, and general JAX-RS annotations.
 * 
 * @author Sundararajan Suriyaprakash
 *
 */
@RestController

@RequestMapping(value = "/enclothe-customer", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })

public class EnclotheCustomerEndpoint extends CustomerEndpoint {

	@Resource(name="blCustomerService")
	protected CustomerService customerService;
	
	@Resource(name="blCustomerPhoneService")
	protected CustomerPhoneService customerPhoneService;
	
	@Autowired
	SMSHandler smsHandler;
	
	@Autowired
	SMSAccountActivationMessage smsAccountActivationMessage;
	
	@Autowired
	SMSAccountVerificationMessage smsAccountVerificationMessage;

	@RequestMapping(value = "/addaddress", method = RequestMethod.PUT,
	consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public CustomerAddressWrapper addAddressForCustomerId(HttpServletRequest request, @RequestBody CustomerAddressWrapper wrapper,@RequestParam(value = "customerId", required = true) long customerId) {
		Customer customer = customerService.readCustomerById(customerId);
		if(customer != null) {
			try {
				return addAddress(request, customerId, wrapper);
			}
			catch(Exception e) {
				System.out.println("Exception in addaddress in IF STARTS");
				System.out.println(e.getMessage());
				System.out.println("Exception in addaddress in IF ENDS");
				return null;
			}

		}
		else {
			try {
				return addAddress(request, null, wrapper);
			}
			catch(Exception e) {
				System.out.println("Exception in addaddress in ELSE STARTS");
				System.out.println(e.getMessage());
				System.out.println("Exception in addaddress in ELSE ENDS");
				return null;
			}
		}
	}
	
	
	@RequestMapping(value = "/addPhoneNumber", method = RequestMethod.PUT,
	consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public String addPhoneNumberForCustomerId(HttpServletRequest request, @RequestParam(value = "customerId", required = true) long customerId,
			@RequestParam(value = "phoneNumber", required = true) String phoneNum) {
		Customer customer = customerService.readCustomerById(customerId);
		if (customer == null) {
			throw BroadleafWebServicesException.build(HttpStatus.NOT_FOUND.value())
			.addMessage(BroadleafWebServicesException.CUSTOMER_NOT_FOUND);
		}
		
		List<CustomerPhone> customerPhone = null;
		customerPhone = customer.getCustomerPhones();
		System.out.println("size ---customerPhone.size()"+customerPhone.size());
		if(customerPhone != null && customerPhone.size() > 0) {
			
			if(customerPhone.size() == 1) {
				CustomerPhone customerPhoneOnlyOne= customerPhone.get(0);
				Phone phoneOnlyOne = customerPhoneOnlyOne.getPhone();
				phoneOnlyOne.setPhoneNumber(phoneNum);
				phoneOnlyOne.setDefault(true);
				phoneOnlyOne.setActive(true);
				customerPhoneOnlyOne.setPhone(phoneOnlyOne);
				customerPhoneService.saveCustomerPhone(customerPhoneOnlyOne);
			}
			else {
				HashMap<Integer, CustomerPhone> collect = customerPhone
		                .stream()
		                .collect(HashMap<Integer, CustomerPhone>::new,
		                        (map, streamValue) -> map.put(map.size(), streamValue),
		                        (map, map2) -> {
		                        });

		        collect.forEach((index, customerPhoneMap) -> {
		        	System.out.println(index + ":" + customerPhoneMap);
		        	if (index== 0) {
		        		Phone phone =  customerPhoneMap.getPhone();
		        		phone.setPhoneNumber(phoneNum);
		        		phone.setDefault(true);
		        		phone.setActive(true);
		        		customerPhoneMap.setPhone(phone);
						customerPhoneService.saveCustomerPhone(customerPhoneMap);
		        	}else {
		        		Phone phone =  customerPhoneMap.getPhone();
		        		phone.setDefault(false);
		        		phone.setActive(false);
		        		customerPhoneMap.setPhone(phone);
						customerPhoneService.saveCustomerPhone(customerPhoneMap);
		        	}
		        });
			}
			
		}else {
			String phoneNumber = phoneNum;
			Phone phone = new PhoneImpl();
			phone.setPhoneNumber(phoneNumber);
			phone.setDefault(true);
			phone.setActive(true);
			CustomerPhone customerPhoneNew =  new CustomerPhoneImpl();
			customerPhoneNew.setCustomer(customer);
			customerPhoneNew.setPhone(phone);
			customerPhoneService.saveCustomerPhone(customerPhoneNew);
		}
		
		
		
		return "Success";
	}
	public CustomerAddressWrapper addAddress(HttpServletRequest request, Long customerId, CustomerAddressWrapper wrapper) {
		Customer customer = getCustomer(customerId);
		if (customer == null) {
			throw BroadleafWebServicesException.build(HttpStatus.NOT_FOUND.value())
			.addMessage(BroadleafWebServicesException.CUSTOMER_NOT_FOUND);
		}
		try {
			CustomerAddress address = wrapper.unwrap(request, context);
			address.setCustomer(customer);
			address = customerAddressService.saveCustomerAddress(address);
			CustomerAddressWrapper response = (CustomerAddressWrapper) context.getBean(CustomerAddressWrapper.class.getName());
			response.wrapDetails(address, request);	
			return response;
		}
		catch(Exception e) {
			System.out.println("Exception in addaddress in CustomerAddressWrapper STARTS");
			System.out.println(e.getMessage());
			System.out.println("Exception in addaddress in CustomerAddressWrapper ENDS");
			return null;
		}
	}
	@RequestMapping(value = "/getAddressForCustomerByAddressName", method = RequestMethod.GET)
			public List<CustomerAddressWrapper> getAddressForCustomerId(HttpServletRequest request,@RequestParam(value = "customerId", required = true) long customerId,@RequestParam(value = "addressName", required = true) String addressName) {
				Customer customer = customerService.readCustomerById(customerId);
				List<CustomerAddressWrapper> listWrapper = new ArrayList<CustomerAddressWrapper>();
				if(customer != null && !addressName.isEmpty()) {
					CustomerAddressWrapper address= super.findAddress(request, customerId, addressName);
					listWrapper.add(address);
				}
				else {
					listWrapper = findAllAddresses(request, customerId); 
							
				}
				
				return listWrapper;

			}

	@RequestMapping(value = "/getAllAddressesForCustomer", method = RequestMethod.GET)
	public List<CustomerAddressWrapper> getAllAddressForCustomerId(HttpServletRequest request,@RequestParam(value = "customerId", required = true) long customerId) {
		Customer customer = customerService.readCustomerById(customerId);
		List<CustomerAddressWrapper> listWrapper = new ArrayList<CustomerAddressWrapper>();

		if(customer != null ) {
			
				listWrapper = super.findAllAddresses(request, customerId);	
		}
		else {
			listWrapper = super.findAllAddresses(request, null); 
					
		}
		
		return listWrapper;

	}



	//	@RequestMapping(value = "category/{name}", method = RequestMethod.GET)
	//	public ResponseEntity<List<?>> findEnclotheProductsByCategoryName(HttpServletRequest request,@PathVariable("name") String name) {
	//
	//		try {
	//
	//			List<Product> products = new ArrayList<Product>();
	//			Category category = catalogService.findCategoryByName(name);
	//
	//			if (category == null) {
	//
	//				return 	new ResponseEntity<List<?>>(getProductWrapperList(catalogService.findAllProducts(), request),HttpStatus.PARTIAL_CONTENT);
	//
	//
	//			} else {
	//				products = catalogService.findActiveProductsByCategory(category);
	//				return new ResponseEntity<List<?>>(getProductWrapperList(products, request),HttpStatus.FOUND);
	//			}
	//
	//		} catch (Exception e) { // if we failed to find the cart, create a new one
	//			return new ResponseEntity<List<?>>(HttpStatus.INTERNAL_SERVER_ERROR); 
	//			}
	//	}
	//	 

	/*
	 * @RequestMapping(value = "category/{id}", method = RequestMethod.GET) public
	 * List<ProductWrapper> findEnclotheProductsByCategoryIdUrI(HttpServletRequest
	 * request, @PathVariable("id") long categoryId) {
	 * 
	 * try {
	 * 
	 * List<Product> products = new ArrayList<Product>();
	 * 
	 * Category category= catalogService.findCategoryById(categoryId);
	 * 
	 * 
	 * if(category == null) {
	 * 
	 * return getProductWrapperList(catalogService.findAllProducts(),request);
	 * 
	 * }else { products = catalogService.findActiveProductsByCategory(category);
	 * return getProductWrapperList(products,request); }
	 * 
	 * 
	 * } catch (Exception e) { // if we failed to find the cart, create a new one
	 * return null; } }
	 */


	public List<ProductWrapper> getProductWrapperList(List<Product> products,HttpServletRequest request){
		List<ProductWrapper> out = new ArrayList<ProductWrapper>();

		for (Product prod : products) {
			ProductWrapper wrapper = (ProductWrapper) context.getBean(ProductWrapper.class.getName());
			wrapper.wrapSummary(prod, request);
			out.add(wrapper);
		}
		return out;
	}
}






