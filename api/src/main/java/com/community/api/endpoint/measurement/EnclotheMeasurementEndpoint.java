
/*Copyright 2008-2012 the original author or authors.

Licensed under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy of
the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations under
the License.*/

package com.community.api.endpoint.measurement;

import org.broadleafcommerce.common.web.controller.annotation.FrameworkMapping;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.broadleafcommerce.core.catalog.domain.Product;
import org.broadleafcommerce.core.catalog.service.CatalogService;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.domain.CustomerAddress;
import org.broadleafcommerce.profile.core.domain.CustomerPhone;
import org.broadleafcommerce.profile.core.domain.CustomerPhoneImpl;
import org.broadleafcommerce.profile.core.domain.Phone;
import org.broadleafcommerce.profile.core.domain.PhoneImpl;
import org.broadleafcommerce.profile.core.service.CustomerPhoneService;
import org.broadleafcommerce.profile.core.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.broadleafcommerce.rest.api.endpoint.BaseEndpoint;
import com.broadleafcommerce.rest.api.endpoint.customer.CustomerEndpoint;
import com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException;
import com.broadleafcommerce.rest.api.wrapper.CustomerAddressWrapper;
import com.broadleafcommerce.rest.api.wrapper.CustomerWrapper;
import com.broadleafcommerce.rest.api.wrapper.PhoneWrapper;
import com.broadleafcommerce.rest.api.wrapper.ProductWrapper;
import com.community.api.enccustomer.service.EncCustomerService;
import com.community.api.measurement.service.CustomerMeasurementService;
import com.community.api.measurement.service.MeasurementService;
import com.community.api.sms.handler.SMSHandler;
import com.community.api.sms.messages.SMSAccountActivationMessage;
import com.community.api.sms.messages.SMSAccountVerificationMessage;
import com.community.api.sms.messages.SMSMessage;
import com.community.api.wrapper.EncCustomerMeasurementWrapper;
import com.community.api.wrapper.EncMeasurementWrapper;
import com.community.api.wrapper.EncOrderWrapper;
import com.community.api.wrapper.EnclotheCustomerWrapper;
import com.enclothe.core.customer.domain.EncCustomer;
import com.enclothe.core.dm.order.domain.EncOrder;
import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.Measurement;
import com.enclothe.core.measurement.domain.MeasurementImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * This is a reference REST API endpoint for cart. This can be modified, used as
 * is, or removed. The purpose is to provide an out of the box RESTful cart
 * service implementation, but also to allow the implementor to have fine
 * control over the actual API, URIs, and general JAX-RS annotations.
 * 
 * @author Sundararajan Suriyaprakash
 *
 */
@RestController

@RequestMapping(value = "/enclothe-measurement", produces = { MediaType.APPLICATION_JSON_VALUE,
		MediaType.APPLICATION_XML_VALUE })

public class EnclotheMeasurementEndpoint extends CustomerEndpoint {

	@Resource(name = "encCustomerService")
	protected EncCustomerService customerService;

	@Resource(name = "encCustomerMeasurementService")
	protected CustomerMeasurementService measurementService;

	@Resource(name = "encMeasurementService")
	protected MeasurementService encmeasurementService;

	@RequestMapping(value = "/addCustomerMeasurement", method = RequestMethod.PUT, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public EncCustomerMeasurementWrapper addMeasurementForCustomerId(HttpServletRequest request,
			@RequestBody EncMeasurementWrapper wrapper,
			@RequestParam(value = "customerId", required = true) long customerId) {
		EncCustomer customer = customerService.readEncCustomerById(customerId);
		Measurement measurement = encmeasurementService.createMeasurement();
		measurement = wrapper.unwrap(request, context);

		if (customer != null) {
			try {
				EncCustomerMeasurement encCustomerMeasurement = measurementService.createEncCustomerMeasurement();
				encCustomerMeasurement.setMeasurements(measurement);
				encCustomerMeasurement.setCustomer(customer);
				encCustomerMeasurement = measurementService.saveEncCustomerMeasurement(encCustomerMeasurement);
				EncCustomerMeasurementWrapper response = new EncCustomerMeasurementWrapper();
				response.wrapDetails(encCustomerMeasurement, request);

				return response;
			} catch (Exception e) {
				System.out.println("Exception in CustomerMeasurement addCustomerMeasurement begin");
				System.out.println(e.getMessage());
				System.out.println("Exception in CustomerMeasurement addCustomerMeasurement end");
				return null;

				// No bean named 'com.enclothe.core.customer.domain.EncCustomerMeasurement'
				// available
			}

		} else
			System.out.println("Customer id in EnclotheMeasurementEndPoint is incorrect...check the request");

		return null;

	}

	@RequestMapping(value = "/deleteCustomerMeasurement", method = RequestMethod.PUT, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity deleteMeasurementByMeasurementId(HttpServletRequest request,
			@RequestParam(value = "MeasurementId", required = true) long measurementId) {
		EncCustomerMeasurement measurement = measurementService.readEncCustomerMeasurementById(measurementId);

		if (measurement != null) {
			try {
				EncCustomerMeasurement encCustomerMeasurement = measurementService
						.deleteEncCustomerMeasurement(measurement);

				return new ResponseEntity(HttpStatus.OK);

			} catch (Exception e) {
				System.out.println("Exception in CustomerMeasurement deleteCustomerMeasurement begin");
				System.out.println(e.getMessage());
				System.out.println("Exception in CustomerMeasurement deleteCustomerMeasurement end");
				return null;
			}

		} else
			System.out.println("Customer id in EnclotheMeasurementEndPoint is   incorrect...check the request");

		return null;

	}

	@RequestMapping(value = "/getAllMeasurementsForCustomer", method = RequestMethod.GET)
	public List<Measurement> findCustomerMeasurements(HttpServletRequest request,

			@RequestParam(value = "customerId", required = true) Long customerId) {

		EncCustomerMeasurementWrapper out = new EncCustomerMeasurementWrapper();

		try {

			List<Measurement> measurementList = measurementService
					.readActiveEncCustomerMeasurementsByCustomerId(customerId);

			return measurementList;

		} catch (Exception e) {
			return null;
		}
	}

}
