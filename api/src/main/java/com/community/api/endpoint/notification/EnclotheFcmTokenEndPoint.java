package com.community.api.endpoint.notification;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.community.api.notification.service.EnclotheFcmTokenService;
import com.community.api.notification.service.PushNotificationService;
import com.enclothe.core.notification.domain.FcmSessionToken;
import com.enclothe.core.notification.domain.FcmSessionTokenImpl;
import com.community.api.notification.model.PushNotificationRequest;
import com.community.api.notification.model.PushNotificationResponse;
import com.community.api.notification.service.EnclotheFcmNotificationService;


@RestController
@RequestMapping(value = "/fcm", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })

public class EnclotheFcmTokenEndPoint {
	
	@Resource(name = "enclotheFcmTokenService")
	EnclotheFcmTokenService enclotheFcmTokenService;
	
	@Resource(name = "enclotheFcmNotificationService")
	EnclotheFcmNotificationService enclotheFcmNotificationService;
	
	@Resource(name="blCustomerService")
	protected CustomerService customerService;
	
	@Resource(name="enclotheFcmNotificationService")
	EnclotheFcmNotificationService EnclotheFcmNotificationService;
	
	private PushNotificationService pushNotificationService;
	
	public EnclotheFcmTokenEndPoint(PushNotificationService pushNotificationService) {
		// TODO Auto-generated constructor stub
		this.pushNotificationService = pushNotificationService;
	}
	
	@RequestMapping(value = "/registerToken", method = RequestMethod.POST)
	public ResponseEntity<?> registerToken(HttpServletRequest request,
			@RequestParam(value = "loginName", required = true) String loginName,
			@RequestParam(value = "loginType", required = true) String loginType,
			@RequestParam(value = "fcmToken", required = true) String fcmToken) throws Exception {
	
		FcmSessionToken fcmSessionToken = new FcmSessionTokenImpl();
		if(loginName !=null && loginType != null && (loginType.equalsIgnoreCase("email") || loginType.equalsIgnoreCase("phone"))) {
			Customer customer = customerService.readCustomerByEmail(loginName);
			if(customer!=null && customer.isDeactivated()) {
				return   new ResponseEntity<Object>(HttpStatus.UNAUTHORIZED);
			}
			else {
				fcmSessionToken.setId(customer.getId());
				fcmSessionToken.setLoginName(loginName);
				fcmSessionToken.setLoginType(loginType);
				fcmSessionToken.setFcmToken(fcmToken);
				enclotheFcmTokenService.persist(fcmSessionToken);
			}
		}
		else {
			return   new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		
		return ResponseEntity.ok("Success");
	}
	
	@RequestMapping(value = "/addMessageToUser", method = RequestMethod.POST)
	public ResponseEntity<?> addMessageToUser(HttpServletRequest request,
			@RequestParam(value = "loginName", required = true) String userName,
			@RequestParam(value = "orderId", required = true) String orderId) throws Exception {
	
		if(userName !=null) {
			System.out.println("userName ="+userName);
			Customer customer = customerService.readCustomerByEmail(userName);
			if(customer!=null && customer.isDeactivated()) {
				return   new ResponseEntity<Object>(HttpStatus.UNAUTHORIZED);
			}
			else {
				FcmSessionToken fcmSessionToken = enclotheFcmTokenService.getFcmSessionTokenByUsername(customer.getId());
	            String fcmToken = fcmSessionToken.getFcmToken();
	            System.out.println("fcmToken ="+fcmToken);
	            System.out.println("User Name "+userName+" orderId "+orderId+" fcmToken "+fcmToken);
	            enclotheFcmNotificationService.sendOrderMessageToUser(orderId, userName, fcmToken);
	            System.out.println("Notification Send successfully");
			}
		}
		else {
			return   new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		
		return ResponseEntity.ok("Success");
	}
	
	@PostMapping("/notification/topic")
    public ResponseEntity sendNotification(@RequestBody PushNotificationRequest request) {
		System.out.println("Topic is ===="+request.getTopic());
        pushNotificationService.sendPushNotificationWithoutData(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }
    @PostMapping("/notification/token")
    public ResponseEntity sendTokenNotification(@RequestBody PushNotificationRequest request) {
    	System.out.println("Token Topic is ===="+request.getTopic());
    	pushNotificationService.sendPushNotificationToToken(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }
    @PostMapping("/notification/data")
    public ResponseEntity sendDataNotification(@RequestBody PushNotificationRequest request) {
        pushNotificationService.sendPushNotification(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }  
	
}
