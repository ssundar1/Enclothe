
/*Copyright 2008-2012 the original author or authors.

Licensed under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy of
the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations under
the License.*/

package com.community.api.endpoint.order;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.broadleafcommerce.rest.api.endpoint.order.CartEndpoint;
import com.community.api.measurement.service.CustomerMeasurementService;
import com.community.api.order.service.EnclotheOrderService;
import com.community.api.reseller.service.EnclotheOrderResellerXrefService;
import com.community.api.reseller.service.EnclotheResellerService;
import com.community.api.wrapper.EncOrderWrapper;
import com.community.api.wrapper.EncOrderWrapperEnhanced;
import com.enclothe.core.dm.order.domain.EncOrder;
import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.Measurement;
import com.enclothe.core.reseller.domain.EncReseller;
import com.enclothe.core.xref.domain.EncOrderResellerXref;

/**
 * This is a reference REST API endpoint for cart. This can be modified, used as
 * is, or removed. The purpose is to provide an out of the box RESTful cart
 * service implementation, but also to allow the implementor to have fine
 * control over the actual API, URIs, and general JAX-RS annotations.
 * 
 * @author Sundararajan Suriyaprakash
 *
 */
@RestController

@RequestMapping(value = "/encOrders", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })

public class EnclotheOrderEndpoint extends CartEndpoint {


	@Resource(name = "encOrderService")
	protected EnclotheOrderService enclotheOrderService;
	
	@Resource(name = "encCustomerMeasurementService")
	protected CustomerMeasurementService measurementService;
	
	@Resource(name = "enclotheResellerService")
	EnclotheResellerService enclotheResellerService;

	@Resource(name = "enclotheOrderResellerXrefService")
	EnclotheOrderResellerXrefService enclotheOrderResellerXrefService;
	

	@RequestMapping(value = "/getOrderPaymentReference", method = RequestMethod.GET)
	public ResponseEntity<EncOrderWrapper> findOrderPaymentReference(HttpServletRequest request,
			@RequestParam(value = "orderId", required = true) String orderId) {

		EncOrderWrapper out = (EncOrderWrapper) context.getBean(EncOrderWrapper.class.getName());
		EncOrder order = null;

		try {

			String paymentrefno = null;
			order = enclotheOrderService.findOrderByID(Long.parseLong(orderId));

			if(!(order == null)) {
			paymentrefno = order.getOrderPaymentReference();
			
			}
			System.out.println("Inside EnclotheOrderEndPoint...checking paymentreference no:"+paymentrefno);
			
out.wrapDetails(order, request);
			
				

				return new ResponseEntity<EncOrderWrapper>(out, HttpStatus.FOUND);
			

		} catch (Exception e) {
			// if we failed to find the cart, create a new one
			return new ResponseEntity<EncOrderWrapper>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	
	 @RequestMapping(value = "/addPaymentPickupToOrder", method = RequestMethod.PUT)
		//consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
public ResponseEntity<EncOrderWrapper> addPaymentRefToOrder(HttpServletRequest request,@RequestParam(value = "OrderNo", required = true) long orderNo,
		@RequestParam(value = "paymentReference", required = true) String paymentReference,
		@RequestParam(value = "pickupDateTime", required = false) String pickupDateTime,
		@RequestParam(value = "additionalInfo", required = false) String additionalInfo) {
			EncOrderWrapper out = (EncOrderWrapper) context.getBean(EncOrderWrapper.class.getName());
			EncOrder order = null;

			try {

				String paymentrefno = null;
				order = enclotheOrderService.findOrderByID(orderNo);

				if(!(order == null)) {
				order.setOrderPaymentReference(paymentReference);
				order.setAdditionalInfo(additionalInfo);
				if((!(pickupDateTime == null)|| !(pickupDateTime.isEmpty())))
				{
						//order.setAdditionalInfo(pickupDateTime);
						java.util.Date tempDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
				                .parse(pickupDateTime);
						order.setMaterial_pickup_date_time(tempDate);
				}
				//order.setAdditionalInfo(pickupDateTime);
				order = enclotheOrderService.persist(order);
				
				}
				System.out.println(" Inside EnclotheOrderEndPoint...checking paymentreference no:"+paymentrefno + "...Date"+order.getMaterial_pickup_date_time());
				
	out.wrapDetails(order, request);
				
					

					return new ResponseEntity<EncOrderWrapper>(out, HttpStatus.OK);
				

			} catch (Exception e) {
				return new ResponseEntity<EncOrderWrapper>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		 
	 }
	 
	/*
	 * @RequestMapping(value = "/addCpMeaasurementToOrder", method =
	 * RequestMethod.PUT) //consumes = { MediaType.APPLICATION_JSON_VALUE,
	 * MediaType.APPLICATION_XML_VALUE }) public
	 * ResponseEntity<EncOrderWrapperEnhanced>
	 * addCpMeasurementToOrder(HttpServletRequest request,@RequestParam(value =
	 * "OrderNo", required = true) long orderNo,
	 * 
	 * @RequestParam(value = "CpId", required = true) Long CpId,
	 * 
	 * @RequestParam(value = "measurementId", required = false) Long measurementId)
	 * { EncOrderWrapperEnhanced out = (EncOrderWrapperEnhanced)
	 * context.getBean(EncOrderWrapperEnhanced.class.getName()); EncOrder order =
	 * null;
	 * 
	 * EncCustomerMeasurement encCustomerMeasurement =
	 * measurementService.readEncCustomerMeasurementById(measurementId); EncReseller
	 * encReseller = enclotheResellerService.findByResellerId(CpId); try {
	 * 
	 * String paymentrefno = null; order =
	 * enclotheOrderService.findOrderByID(orderNo);
	 * 
	 * if(!(order == null)) {
	 * 
	 * if(!(encCustomerMeasurement == null)) {
	 * order.setCustomerMeasurement(encCustomerMeasurement); } if(!(encReseller ==
	 * null)) { EncOrderResellerXref encOrderResellerXref =
	 * enclotheOrderResellerXrefService.create();
	 * encOrderResellerXref.setOrder(order);
	 * encOrderResellerXref.setReseller(encReseller); encOrderResellerXref =
	 * enclotheOrderResellerXrefService.persist(encOrderResellerXref);
	 * List<EncOrderResellerXref> xrefList = new ArrayList<EncOrderResellerXref>();
	 * xrefList.add(encOrderResellerXref); order.setOrderResellerXref(xrefList); }
	 * order = enclotheOrderService.persist(order);
	 * 
	 * }
	 * 
	 * out.wrapDetails(order, request);
	 * 
	 * 
	 * 
	 * return new ResponseEntity<EncOrderWrapperEnhanced>(out, HttpStatus.OK);
	 * 
	 * 
	 * } catch (Exception e) { return new
	 * ResponseEntity<EncOrderWrapperEnhanced>(HttpStatus.INTERNAL_SERVER_ERROR); }
	 * 
	 * }
	 */
	public List<EncOrderWrapper> getEncWrapperList(List<EncOrder> orders, HttpServletRequest request) {
		List<EncOrderWrapper> out = new ArrayList<EncOrderWrapper>();

		for (EncOrder order : orders) {
			EncOrderWrapper wrapper = (EncOrderWrapper) context.getBean(EncOrderWrapper.class.getName());
			wrapper.wrapSummary(order, request);
			out.add(wrapper);
		}
		return out;
	}

}
