
/*Copyright 2008-2012 the original author or authors.

Licensed under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy of
the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations under
the License.*/


package com.community.api.endpoint.products;

import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.core.catalog.dao.CategoryDao;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.broadleafcommerce.core.catalog.domain.Product;
import org.broadleafcommerce.core.catalog.service.CatalogService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable; 
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.broadleafcommerce.rest.api.endpoint.BaseEndpoint;
import com.broadleafcommerce.rest.api.endpoint.catalog.CatalogEndpoint;
import com.broadleafcommerce.rest.api.wrapper.CategoryWrapper;
import com.broadleafcommerce.rest.api.wrapper.ProductWrapper;
import com.community.api.catalog.services.EnclotheCatalogService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource; 
import javax.servlet.http.HttpServletRequest;


/**
 * This is a reference REST API endpoint for cart. This can be modified, used as
 * is, or removed. The purpose is to provide an out of the box RESTful cart
 * service implementation, but also to allow the implementor to have fine
 * control over the actual API, URIs, and general JAX-RS annotations.
 * 
 * @author Sundararajan Suriyaprakash
 *
 */
@RestController

@RequestMapping(value = "/encproducts", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })

public class EnclotheProductsEndpoint extends CatalogEndpoint {

	/*
	 * @Resource(name="blCatalogService") protected CatalogService catalogService;
	 */
	
	
	  @Resource(name="encCatalogService")
	  protected EnclotheCatalogService enclotheCatalogService;
	 

	@RequestMapping(value = "/bycategory", method = RequestMethod.GET)
	public ResponseEntity<List<ProductWrapper>> findEnclotheProductsByCategoryId(HttpServletRequest request, @RequestParam(value = "categoryId", required = true) long categoryId) 
	{
		
		List<ProductWrapper> out = new ArrayList<ProductWrapper>();

		try {

			List<Product> products; Category category= catalogService.findCategoryById(categoryId);



			if(category == null)
			{
				products = catalogService.findAllProducts();
                return new ResponseEntity<List<ProductWrapper>>(out,HttpStatus.PARTIAL_CONTENT);

			}
			else {
				products = catalogService.findActiveProductsByCategory(category);
			}
			if (products != null) {

				out = this.getProductWrapperList(products, request);
				return new ResponseEntity<List<ProductWrapper>>(out,HttpStatus.OK);
			}else
			{
				return new ResponseEntity<List<ProductWrapper>>(out,HttpStatus.NOT_FOUND);

			}




		} catch (Exception e) {
			// if we failed to find the cart, create a new one
			return new ResponseEntity<List<ProductWrapper>>(HttpStatus.INTERNAL_SERVER_ERROR); 
		}
	}

	@RequestMapping(value = "/getallcategories", method = RequestMethod.GET)
	public ResponseEntity<List<CategoryWrapper>> findAllEnclotheCategory(HttpServletRequest request, @RequestParam(value = "externalId", required = true) String externalId) 
	{
		List<CategoryWrapper> out = new ArrayList<CategoryWrapper>();

		try {

			List<Category> categories = new ArrayList<Category>(); 
			
			categories = enclotheCatalogService.findCategoryByExternalId(externalId);



			if(categories == null || categories.isEmpty())
			{
				return new ResponseEntity<List<CategoryWrapper>>(out,HttpStatus.NOT_FOUND);

			}
			else {
				

				out = this.getCategoryWrapperList(categories, request,externalId);
				return new ResponseEntity<List<CategoryWrapper>>(out,HttpStatus.OK);
			}


		} catch (Exception e) {
			// if we failed to find the cart, create a new one
			return new ResponseEntity<List<CategoryWrapper>>(HttpStatus.INTERNAL_SERVER_ERROR); 
		}
	}

	@RequestMapping(value = "/getProductsMultipleCategories", method = RequestMethod.GET)
	public ResponseEntity<List<ProductWrapper>> findProductsMultipleCategory(HttpServletRequest request, @RequestParam(value = "categoryids", required = true)List <Long> categoryids, @RequestParam(value = "externalId", required = true) String externalId )
	{
		
		//EnclotheCatalogService encCatalogService = new EnclotheCatalogService();

		List<ProductWrapper> out = new ArrayList<ProductWrapper>();

		try {

			List<Product> products = new ArrayList<Product>(); 
			
			products = enclotheCatalogService.findProductsMultipleCategory(categoryids,externalId);



			if(products == null || products.isEmpty())
			{
				return new ResponseEntity<List<ProductWrapper>>(out,HttpStatus.NOT_FOUND);

			}
			else {
				

				out = this.getProductWrapperList(products, request);
				return new ResponseEntity<List<ProductWrapper>>(out,HttpStatus.OK);
			}


		} catch (Exception e) {
			// if we failed to find the cart, create a new one
			return new ResponseEntity<List<ProductWrapper>>(HttpStatus.INTERNAL_SERVER_ERROR); 
		}
	}


	@RequestMapping(value = "/getProductsByCategoryExternalId", method = RequestMethod.GET)
	public ResponseEntity<List<ProductWrapper>> findProductsByCategoryExternalId(HttpServletRequest request,
			@RequestParam(value = "externalId", required = true) String externalId) {

		// EnclotheCatalogService encCatalogService = new EnclotheCatalogService();

		List<ProductWrapper> out = new ArrayList<ProductWrapper>();

		try {

			List<Product> products = new ArrayList<Product>();

			products = enclotheCatalogService.findProductsByCategoryExternalId(externalId);
			

			if (products == null || products.isEmpty()) {
				return new ResponseEntity<List<ProductWrapper>>(out, HttpStatus.NOT_FOUND);

			} else {
				
				List<Product> listUnique = products.stream().distinct().collect(Collectors.toList());
				  listUnique.sort(Comparator.comparing(Product::getRetailPrice,(price1, price2) -> {
					    if(price1 == price2){
					         return 0;
					    }
					    return price1.compareTo(price2) ;
					}));

				out = this.getProductWrapperList(listUnique, request);
				return new ResponseEntity<List<ProductWrapper>>(out, HttpStatus.OK);
			}

		} catch (Exception e) {
			// if we failed to find the cart, create a new one
			return new ResponseEntity<List<ProductWrapper>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@RequestMapping(value = "bycategory/{name}", method = RequestMethod.GET)
	public ResponseEntity<List<?>> findEnclotheProductsByCategoryName(HttpServletRequest request,@PathVariable("name") String name) {

		try {

			List<Product> products = new ArrayList<Product>();
			Category category = catalogService.findCategoryByName(name);

			if (category == null) {

				return 	new ResponseEntity<List<?>>(getProductWrapperList(catalogService.findAllProducts(), request),HttpStatus.PARTIAL_CONTENT);


			} else {
				products = catalogService.findActiveProductsByCategory(category);
				return new ResponseEntity<List<?>>(getProductWrapperList(products, request),HttpStatus.FOUND);
			}

		} catch (Exception e) { // if we failed to find the cart, create a new one
			return new ResponseEntity<List<?>>(HttpStatus.INTERNAL_SERVER_ERROR); 
			}
	}
	 

	/*
	 * @RequestMapping(value = "category/{id}", method = RequestMethod.GET) public
	 * List<ProductWrapper> findEnclotheProductsByCategoryIdUrI(HttpServletRequest
	 * request, @PathVariable("id") long categoryId) {
	 * 
	 * try {
	 * 
	 * List<Product> products = new ArrayList<Product>();
	 * 
	 * Category category= catalogService.findCategoryById(categoryId);
	 * 
	 * 
	 * if(category == null) {
	 * 
	 * return getProductWrapperList(catalogService.findAllProducts(),request);
	 * 
	 * }else { products = catalogService.findActiveProductsByCategory(category);
	 * return getProductWrapperList(products,request); }
	 * 
	 * 
	 * } catch (Exception e) { // if we failed to find the cart, create a new one
	 * return null; } }
	 */


	public List<ProductWrapper> getProductWrapperList(List<Product> products,HttpServletRequest request){
		List<ProductWrapper> out = new ArrayList<ProductWrapper>();

		for (Product prod : products) {
			ProductWrapper wrapper = (ProductWrapper) context.getBean(ProductWrapper.class.getName());
			wrapper.wrapSummary(prod, request);
			out.add(wrapper);
		}
		return out;
	}
	public List<CategoryWrapper> getCategoryWrapperList(List<Category> categories,HttpServletRequest request,String categoryExternalId){
		List<CategoryWrapper> out = new ArrayList<CategoryWrapper>();

		for (Category category : categories) {
			if(category.getExternalId()!=null && category.getExternalId().equalsIgnoreCase(categoryExternalId)) {
			CategoryWrapper wrapper = (CategoryWrapper) context.getBean(CategoryWrapper.class.getName());
			wrapper.wrapSummary(category, request);
			out.add(wrapper);
			}
		}
		return out;
	}
}






