package com.community.api.endpoint.promocode;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.core.order.service.OrderService;
import org.broadleafcommerce.jwt.model.JwtRequest;
import org.broadleafcommerce.jwt.model.JwtResponseWithFUVersion;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.service.CustomerPhoneService;
import org.broadleafcommerce.profile.core.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.broadleafcommerce.rest.api.endpoint.BaseEndpoint;
import com.broadleafcommerce.rest.api.endpoint.order.CartEndpoint;
import com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException;
import com.community.api.configuration.CodeConfig;
import com.community.api.order.service.EnclotheOrderService;
import com.community.api.promocode.service.EnclothePromoCodeService;
import com.community.api.reseller.service.EnclotheResellerService;
import com.community.api.service.JwtUserDetailsService;
import com.community.api.sms.handler.SMSHandler;
import com.community.api.sms.messages.SMSAccountActivationMessage;
import com.community.api.sms.messages.SMSAccountVerificationMessage;
import com.community.api.sms.messages.SMSMessage;
import com.community.api.sms.modal.SMSResponse;
import com.community.api.wrapper.EncOrderWrapper;
import com.enclothe.core.dm.order.domain.EncOrder;
import com.enclothe.core.promocode.domain.EncPromoCode;
import com.enclothe.core.promocode.domain.EncPromoCodeImpl;
import com.enclothe.core.reseller.domain.EncReseller;

@RestController
@CrossOrigin
public class PromoCodeEndpoint extends BaseEndpoint{

	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@Resource(name="blCustomerService")
	protected CustomerService customerService;
	
	@Resource(name = "enclotheResellerService")
	protected EnclotheResellerService enclotheResellerService;
	
	@Resource(name = "enclothePromoCodeService")
	protected EnclothePromoCodeService enclothePromoCodeService;
	
	@Resource(name = "encOrderService")
	protected EnclotheOrderService enclotheOrderService;
	
	@Autowired
	SMSHandler smsHandler;
	
	@Autowired
	SMSAccountActivationMessage smsAccountActivationMessage;
	
	@Autowired
	SMSAccountVerificationMessage smsAccountVerificationMessage;
	
	@RequestMapping(value = "/applyPromoCode", method = RequestMethod.PUT,
			consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<EncOrderWrapper>  applyPromocode(HttpServletRequest request,
			@RequestParam(value = "orderId", required = true) String orderId,
			@RequestParam(value = "promoCode", required = true) String promoCode) throws Exception {
			
		EncOrderWrapper out = (EncOrderWrapper) context.getBean(EncOrderWrapper.class.getName());
		
			EncOrder order = null;
			try {

				String paymentrefno = null;
				String discountNumberPercentage = null;
				String discountNumber = null;
				order = enclotheOrderService.findOrderByID(Long.parseLong(orderId));
				System.out.println("promoCode="+promoCode);
				System.out.println("orderId="+orderId);
				if(!(order == null)) {
				paymentrefno = order.getOrderPaymentReference();
				System.out.println("paymentrefno="+paymentrefno);
				}
				try {
					List encPromoCodeList = enclothePromoCodeService.findByPromoCode(promoCode);
					System.out.println("encPromoCodeList="+encPromoCodeList.size());
					if (encPromoCodeList != null && encPromoCodeList.size() > 0) {
						EncPromoCode encPromoCode = (EncPromoCode) encPromoCodeList.get(0);
						System.out.println("encPromoCode="+encPromoCode.getDiscountType());
						String promoType = encPromoCode.getDiscountType();
						if (promoType != null && promoType.equalsIgnoreCase("PERCENTAGE")) {
							discountNumberPercentage = encPromoCode.getDiscountNumber();
						}
						else {
							discountNumber = encPromoCode.getDiscountNumber();
						}
						Money money = order.getTotal();
						BigDecimal amount  = money.getAmount();
						if(discountNumberPercentage != null) {
							Double revisedAmount = Double.valueOf(amount.toString()) * Integer.parseInt(discountNumberPercentage)/100;
							Money revisedMoney = new Money(revisedAmount);
							money.subtract(revisedMoney);
							order.setTotal(money);
						}else if(discountNumber != null) {
							Money revisedMoney = new Money(discountNumber);
							money.subtract(revisedMoney);
							order.setTotal(money);
						}else {
							// No Change
						}
						System.out.println("order="+order.toString());
					}
				}
				catch(Exception e) {
					
				}
				System.out.println("Inside EnclotheOrderEndPoint...checking paymentreference no:"+paymentrefno);
			
				out.wrapDetails(order, request);
				//return ResponseEntity.ok(order);
				return new ResponseEntity<EncOrderWrapper>(out, HttpStatus.OK);
		} catch (Exception e) {
			// if we failed to find the cart, create a new one
			return new ResponseEntity<EncOrderWrapper>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/validatePromoCode", method = RequestMethod.GET,
			consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?>  validatePromocode(HttpServletRequest request, @RequestParam(value = "customerId", required = true) long customerId,
			@RequestParam(value = "promoCode", required = true) String promoCode) throws Exception {
		
		Customer customer = customerService.readCustomerById(customerId);
		
		if (customer == null) {
			throw BroadleafWebServicesException.build(HttpStatus.NOT_FOUND.value())
			.addMessage(BroadleafWebServicesException.CUSTOMER_NOT_FOUND);
		}
		
		return (ResponseEntity<?>) ResponseEntity.ok("success");
	}
	@RequestMapping(value = "/addPromoCode", method = RequestMethod.PUT,
			consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<EncPromoCode> addPromoCode (HttpServletRequest request, 
		@RequestParam(value = "customerId", required = true) long customerId,
		@RequestParam(value = "phoneNumber", required = true) String phoneNum ) throws Exception {
		// given
				CodeConfig config = CodeConfig.pattern("##-###-##");
			    // when
				String code = VoucherCodes.generate(config);
				System.out.println("PromoCode="+code);
				Customer customer = customerService.readCustomerById(customerId);
				
				if (customer == null) {
					throw BroadleafWebServicesException.build(HttpStatus.NOT_FOUND.value())
					.addMessage(BroadleafWebServicesException.CUSTOMER_NOT_FOUND);
				}
				EncPromoCode encPromoCode = null;
				encPromoCode = new EncPromoCodeImpl();
				List<EncReseller> encReseller = enclotheResellerService.findByCustomerId(customer.getId());
				if(encReseller != null && encReseller.size() > 0) {
					  
				  	ListIterator<EncReseller> listIterator = encReseller.listIterator();
				  
				  	while(listIterator.hasNext()) { 
					  EncReseller encResellerList = listIterator.next();
					 encPromoCode.setResellerId(encResellerList.getId());
					  
				  	} 
				 }
				encPromoCode.setPromoCode(code);
				encPromoCode.setMobileNumber(phoneNum);
				encPromoCode.setDiscountNumber("5");
				encPromoCode.setDiscountType("Number");
				encPromoCode.setStatus("Active");
				long millis=System.currentTimeMillis();  
		        Date date=new Date(millis);
		        
				encPromoCode.setUpdatedDate(date);
				
				date.setTime(date.getTime() + 30L * 24 * 60 * 60 * 1000);
		
				encPromoCode.setExpiryDate(date);
				try {
					enclothePromoCodeService.persist(encPromoCode);
				}
				catch(Exception e) {
					System.out.println("Inside Catch Block Exception in Promo Code START");
					System.out.println(e.getMessage());
					System.out.println("Inside Catch Block Exception in Promo Code END");
					return null;
				}
				return ResponseEntity.ok(encPromoCode);
	}
	
	@RequestMapping(value = "/sendPromoCode", method = RequestMethod.PUT,
				consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<EncPromoCode>  sendPromocode(HttpServletRequest request, @RequestParam(value = "customerId", required = true) long customerId,
				@RequestParam(value = "phoneNumber", required = true) String phoneNum) throws Exception {
		 

		System.out.println("sendSMS phoneNum-->"+phoneNum);
		System.out.println("sendSMS customerId-->"+customerId);
		boolean smsResponse = false;
		// given
		CodeConfig config = CodeConfig.pattern("##-###-##");
	    // when
		String code = VoucherCodes.generate(config);
		System.out.println("PromoCode="+code);
		Customer customer = customerService.readCustomerById(customerId);
		
		if (customer == null) {
			throw BroadleafWebServicesException.build(HttpStatus.NOT_FOUND.value())
			.addMessage(BroadleafWebServicesException.CUSTOMER_NOT_FOUND);
		}
		//Send activation code in Text message
		smsAccountActivationMessage.setActivationCode(code);
		smsAccountActivationMessage.setMobile(phoneNum);
		
		smsAccountActivationMessage.setUserId(customerId);
		
		List<SMSMessage> lstMessages = new ArrayList<SMSMessage>();
		lstMessages.add(smsAccountActivationMessage);
		smsResponse = smsHandler.send(lstMessages);
		System.out.println("smsResponse="+smsResponse);
		EncPromoCode encPromoCode = null;
		if(smsResponse) {
			//add to the reseller mapping promocode table
			encPromoCode = new EncPromoCodeImpl();
			List<EncReseller> encReseller = enclotheResellerService.findByCustomerId(customer.getId());
			if(encReseller != null && encReseller.size() > 0) {
				  
			  	ListIterator<EncReseller> listIterator = encReseller.listIterator();
			  
			  	while(listIterator.hasNext()) { 
				  EncReseller encResellerList = listIterator.next();
				 encPromoCode.setResellerId(encResellerList.getId());
				  
			  	} 
			 }
			encPromoCode.setPromoCode(code);
			encPromoCode.setMobileNumber(phoneNum);
			encPromoCode.setDiscountNumber("5");
			encPromoCode.setDiscountType("Number");
			encPromoCode.setStatus("Active");
			long millis=System.currentTimeMillis();  
	        Date date=new Date(millis);
	        
			encPromoCode.setUpdatedDate(date);
			
			date.setTime(date.getTime() + 30L * 24 * 60 * 60 * 1000);
	
			encPromoCode.setExpiryDate(date);
			try {
				enclothePromoCodeService.persist(encPromoCode);
			}
			catch(Exception e) {
				System.out.println("Inside Catch Block Exception in Promo Code START");
				System.out.println(e.getMessage());
				System.out.println("Inside Catch Block Exception in Promo Code END");
				return null;
			}
		}
		return ResponseEntity.ok(encPromoCode);
	}
	 @RequestMapping(value = "/sendSMS", method = RequestMethod.PUT,
				consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
				public String sendSMS(HttpServletRequest request, @RequestParam(value = "customerId", required = true) long customerId,
						@RequestParam(value = "phoneNumber", required = true) String phoneNum,
						@RequestParam(value = "activationCode", required = true) String activationCode) {
			System.out.println("sendSMS phoneNum-->"+phoneNum);
			System.out.println("sendSMS activationCode-->"+activationCode);
			System.out.println("sendSMS customerId-->"+customerId);
			Customer customer = customerService.readCustomerById(customerId);
			if (customer == null) {
				throw BroadleafWebServicesException.build(HttpStatus.NOT_FOUND.value())
				.addMessage(BroadleafWebServicesException.CUSTOMER_NOT_FOUND);
			}
			//Send activation code in Text message
			smsAccountActivationMessage.setActivationCode(activationCode);
			smsAccountActivationMessage.setMobile(phoneNum);
			
			smsAccountActivationMessage.setUserId(customerId);
			
			List<SMSMessage> lstMessages = new ArrayList<SMSMessage>();
			lstMessages.add(smsAccountActivationMessage);
			smsHandler.send(lstMessages);
			return "Success";
		}
}

