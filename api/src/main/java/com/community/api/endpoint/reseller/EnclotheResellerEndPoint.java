package com.community.api.endpoint.reseller;

import java.sql.Date;

import javax.annotation.Resource;

import org.broadleafcommerce.common.audit.Auditable;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.OrderService;
import org.broadleafcommerce.jwt.model.JwtRequest;
import org.broadleafcommerce.jwt.model.JwtResponse;
import org.broadleafcommerce.jwt.model.JwtResponseWithFUVersion;
import org.broadleafcommerce.profile.core.domain.Address;
import org.broadleafcommerce.profile.core.domain.AddressImpl;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.domain.CustomerAddress;
import org.broadleafcommerce.profile.core.domain.CustomerAddressImpl;
import org.broadleafcommerce.profile.core.domain.CustomerAttribute;
import org.broadleafcommerce.profile.core.domain.CustomerAttributeImpl;
import org.broadleafcommerce.profile.core.domain.Phone;
import org.broadleafcommerce.profile.core.domain.PhoneImpl;
import org.broadleafcommerce.profile.core.service.AddressService;
import org.broadleafcommerce.profile.core.service.CustomerAddressService;
import org.broadleafcommerce.profile.core.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.broadleafcommerce.rest.api.endpoint.BaseEndpoint;
import com.broadleafcommerce.rest.api.endpoint.customer.CustomerEndpoint;
import com.broadleafcommerce.rest.api.wrapper.CustomerWrapper;
import com.broadleafcommerce.rest.api.wrapper.OrderWrapper;
import com.community.api.configuration.JwtTokenUtil;
import com.community.api.reseller.service.EnclotheResellerService;
import com.community.api.service.JwtUserDetailsService;
import com.community.api.wrapper.EnclotheCustomerWrapper;
import com.enclothe.core.customer.domain.EncCustomer;
import com.enclothe.core.reseller.domain.EncReseller;
import com.enclothe.core.reseller.domain.EncResellerImpl;

@RestController
@RequestMapping(value = "/enclothe-reseller", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })


public class EnclotheResellerEndPoint extends CustomerEndpoint{
	
	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Resource(name = "enclotheResellerService")
	EnclotheResellerService enclotheResellerService;
	
	@Resource(name = "blCustomerService")
	CustomerService customerService; 
	
	@Resource(name="blOrderService")
	protected OrderService orderService;
	
	public EnclotheResellerEndPoint() {
		
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> authenticateReseller(@RequestBody JwtRequest authenticationRequest) throws Exception {
		System.out.println("Find user details for :"+ authenticationRequest.getUsername() );
		Customer customer = customerService.readCustomerByEmail(authenticationRequest.getUsername());
		String checkingCustomer = customer.getPassword();
		System.out.println("checkingCustomer="+checkingCustomer);
		String authenticatingCustomer = authenticationRequest.getPassword();
		System.out.println("authenticatingCustomer="+authenticatingCustomer);
		if(checkingCustomer != null && authenticatingCustomer != null && checkingCustomer.equals(authenticatingCustomer)) {
		if(customer!=null && customer.isDeactivated()) {
			return   new ResponseEntity<Object>(HttpStatus.UNAUTHORIZED);
		}
		
		JwtResponseWithFUVersion jwt;
		jwt = authenticateCustomer(customer, authenticationRequest);

		return ResponseEntity.ok(jwt);
		}
		else {

			System.out.println("Match fails");
			return (ResponseEntity<?>) ResponseEntity.notFound();
		
		}
	}
	private JwtResponseWithFUVersion authenticateCustomer(Customer customer, JwtRequest authenticationRequest) {
		// TODO Afinal UserDetails userDetails = userDetailsService
		final UserDetails userDetails = userDetailsService.loadUserByUsername(customer.getUsername());

		final String token = jwtTokenUtil.generateTokenForGivenExpiry(userDetails, 2);
		JwtResponseWithFUVersion jwt = new JwtResponseWithFUVersion(token);
		EnclotheCustomerWrapper encCustomerWrapper = new EnclotheCustomerWrapper();
		
		
		if (customer != null) {

			CustomerAttribute customerAttributes = new CustomerAttributeImpl();
			customerAttributes.setCustomer(customer);
			if(authenticationRequest.getAuthentication_source()!=null)
			{
			customerAttributes.setName("Authentication_Source");
			customerAttributes.setValue(authenticationRequest.getAuthentication_source());
			}
			customerAttributes.setCustomer(customer);
			if(authenticationRequest.getApp_id()!=null) {
			customerAttributes.setName("app_id");
			customerAttributes.setValue(authenticationRequest.getApp_id());
			}

			Auditable cust = customer.getAuditable();
			cust.setDateUpdated(new java.util.Date());
			cust.setUpdatedBy(customer.getId());

			customerService.saveCustomer(customer);
			encCustomerWrapper.wrapDetails(customer, null);

			jwt.setCustomerWrapper(encCustomerWrapper);

			Order cart = orderService.findCartForCustomer(customer);
			if (cart != null) {
				// orderWrapper.wrapDetails(cart, null);

				jwt.setCartCount(cart.getItemCount());
			}
		}
		if ((authenticationRequest.getApp_version()!=null)) {

			jwt = checkForceAndOptionalUpgrade(authenticationRequest.getApp_version(), jwt);

		} else {
			jwt.setForceUpgrade(true);
			jwt.setOptionalUpgrade(false);
		}
		return jwt;
	}
	private JwtResponseWithFUVersion checkForceAndOptionalUpgrade(String app_version, JwtResponseWithFUVersion jwt) {
		app_version = app_version.replaceAll("[.]","");
		String server_app_version = jwt.getForceUpgradeVersion();
		server_app_version = server_app_version.replaceAll("[.]", "");
		Double clientVersion = Double.parseDouble(app_version);
		Double forceUpgradeVersion = Double.parseDouble(server_app_version);
		if(clientVersion<=forceUpgradeVersion)
		{
			jwt.setForceUpgrade(true);
		jwt.setOptionalUpgrade(false);
		} 
	return jwt;	
	}
	@RequestMapping(value = "/registerPartner", method = RequestMethod.POST)
	public ResponseEntity<?>  addPartner(
			@RequestParam(value = "userName", required = true) String userName,
			@RequestParam(value = "emailAddress", required = true) String emailAddress,
			@RequestParam(value = "businessType", required = true) String businessType,
			@RequestParam(value = "addressLine1", required = true) String addressLine1,
			@RequestParam(value = "addressLine2", required = false) String addressLine2,
			@RequestParam(value = "city", required = true) String addressLine3,
			@RequestParam(value = "postalCode", required = true) String postalCode,
			@RequestParam(value = "phoneNumber", required = true) String phoneNumber,
			@RequestParam(value = "gstNumber", required = true) String gstNumber,
			@RequestParam(value = "adhaarNumber", required = true) String adhaarNumber,
			@RequestParam(value = "authToken", required = true) String authToken) throws Exception {
		System.out.println("Find user details for userName:"+ userName );
		System.out.println("Find user details for emailAddress:"+ emailAddress );
		System.out.println("Find user details for businessType:"+ businessType );
		System.out.println("Find user details for addressLine1:"+ addressLine1 );
		System.out.println("Find user details for addressLine2:"+ addressLine2 );
		System.out.println("Find user details for addressLine3:"+ addressLine3 );
		System.out.println("Find user details for postalCode:"+ postalCode );
		System.out.println("Find user details for phoneNumber:"+ phoneNumber );
		System.out.println("Find user details for gstNumber:"+ gstNumber );
		System.out.println("Find user details for adhaarNumber:"+ adhaarNumber );
		System.out.println("Find user details for authToken:"+ authToken );
		// Need to verify Token from firebase
		EncCustomer customer = (EncCustomer) customerService.createNewCustomer();
		customer.setUsername(emailAddress);
		customer.setEmailAddress(emailAddress);
		
		JwtRequest authenticationRequest = new JwtRequest();
		authenticationRequest.setApp_version("app_version");
		try {
			customer =    (EncCustomer) customerService.registerCustomer(customer, "test",
				"test");
			 
		}
		catch(Exception e) {
			System.out.println("Inside Catch Block Exception in partner START");
			System.out.println(e.getMessage());
			System.out.println("Inside Catch Block Exception in partner END");
			return null;
		}
		if(customer != null) {
			Address address = new AddressImpl();
			AddressService addressService = (AddressService) context.getBean("blAddressService");
			
			address.setAddressLine1(addressLine1);
			address.setAddressLine2(addressLine2);
			address.setAddressLine3(addressLine3);
			address.setCity(addressLine3);
			address.setPostalCode(postalCode);
			Phone phone = new PhoneImpl();
			phone.setPhoneNumber(phoneNumber);
			address.setPhonePrimary(phone);
			Address addressAdded = addressService.saveAddress(address);
			CustomerAddressService customerAddressService = (CustomerAddressService) context.getBean("blCustomerAddressService");
			CustomerAddress customerAddress = new CustomerAddressImpl();
			customerAddress.setAddress(addressAdded);
			customerAddress.setAddressName("partner_address");
			customerAddress.setCustomer(customer);
			customerAddressService.saveCustomerAddress(customerAddress);
			
		}
		
		customer =    (EncCustomer) customerService.saveCustomer(customer);
		
		EncReseller encReseller = new EncResellerImpl();
		encReseller.setBusinessType(businessType);
		encReseller.setGstNumber(gstNumber);
		encReseller.setAdhaarCode(adhaarNumber);
		encReseller.setUserType("Partner");
		encReseller.setUserPermission("Pending");
		long millis=System.currentTimeMillis();  
        Date date=new Date(millis);  
		encReseller.setUpdatedDate(date);
		encReseller.setCustomer(customer);
		EncReseller encResellerResponse = null;
		try {
			encResellerResponse= enclotheResellerService.persist(encReseller);
			System.out.println("encResellerResponse="+encResellerResponse.toString());
		}
		catch(Exception e) {
			System.out.println("Inside Catch Block Exception in partner reseller START");
			System.out.println(e.getMessage());
			System.out.println("Inside Catch Block Exception in partner reseller END");
			return null;
		}
		/*
		 * final UserDetails userDetails = userDetailsService
		 * .loadUserByUsername(customer.getUsername());
		 */

		/*
		 * final String tokenWithJWT =
		 * jwtTokenUtil.generateTokenForGivenExpiry(userDetails,10);
		 * System.out.println(customer.getUsername()+" Token Expires at :"+
		 * jwtTokenUtil.getExpirationDateFromToken(tokenWithJWT)); JwtResponse jwt = new
		 * JwtResponse(tokenWithJWT);
		 * System.out.println("END of the addPartner"+tokenWithJWT);
		 */
		
		return ResponseEntity.ok("Success");
		}
}
