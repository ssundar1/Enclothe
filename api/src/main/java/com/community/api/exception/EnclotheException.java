package com.community.api.exception;

public class EnclotheException  extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	private String errorCode = null;
	private String errorDesc = null;
	
	public EnclotheException(String errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
		this.errorDesc = message;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public String toJSON() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("{");
		builder.append("\"");
		builder.append("errorcode");
		builder.append("\"");
		builder.append(":");
		builder.append("\"");
		builder.append(errorCode);
		builder.append("\"");
		builder.append(",");
		builder.append("\"");
		builder.append("errordescription");
		builder.append("\"");
		builder.append(":");
		builder.append("\"");
		builder.append(errorDesc);
		builder.append("\"");
		builder.append("}");
		
		return builder.toString();
	}
	
}
