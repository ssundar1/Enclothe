package com.community.api.measurement.dao;

import java.util.List;

import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.Measurement;

public interface EncCustomerMeasurementDao {
	
	EncCustomerMeasurement save(EncCustomerMeasurement measurement);

	void delete(EncCustomerMeasurement measurement);

	EncCustomerMeasurement create(EncCustomerMeasurement measurement);

	Measurement readMeasurementById(Long measurementId);
	
	List<Measurement> readActiveCustomerMeasurementsByCustomerId(Long customerId);

	EncCustomerMeasurement readEncCustomerMeasurementById(Long measurementId);

	EncCustomerMeasurement create();

}
