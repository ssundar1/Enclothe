package com.community.api.measurement.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.broadleafcommerce.common.persistence.ArchiveStatus;
import org.broadleafcommerce.common.persistence.EntityConfiguration;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.EncCustomerMeasurementImpl;
import com.enclothe.core.measurement.domain.Measurement;

@Repository("encCustomerMeasurementDao")
public class EncCustomerMeasurementDaoImpl implements EncCustomerMeasurementDao {

	@PersistenceContext(unitName = "blPU")
    protected EntityManager em;
	
	@Resource(name = "blEntityConfiguration")
    protected EntityConfiguration entityConfiguration;
	
    @SuppressWarnings("unchecked")
    public List<Measurement> readActiveCustomerMeasurementsByCustomerId(Long customerId) {
    	Character archived = 'N';
    	ArchiveStatus as = new ArchiveStatus();
    	as.setArchived(archived);
    	Query query = em.createNamedQuery("BC_READ_ACTIVE_CUSTOMER_MEASUREMENTS_BY_CUSTOMER_ID");
        query.setParameter("customerId", customerId);
        query.setParameter("status",as );
        return query.getResultList();
        //
    }
    
	@Override
	public Measurement readMeasurementById(Long measurementId) {
		Measurement measurement = null;
		final Query query = em.createNamedQuery("BC_READ_MEASUREMENT_BY_ID",Measurement.class);
        query.setParameter("measurementId", measurementId);
        @SuppressWarnings("rawtypes")
        final List temp = query.getResultList();
        if (temp != null && !temp.isEmpty()) {
        	measurement = (Measurement) temp.get(0);
        }
        return measurement;
	}		
	
	@Override
	public EncCustomerMeasurement readEncCustomerMeasurementById(Long measurementId) {
		EncCustomerMeasurement measurement = null;
		final Query query = em.createNamedQuery("BC_READ_CUSTOMER_MEASUREMENT_BY_ID",EncCustomerMeasurement.class);
        query.setParameter("measurementId", measurementId);
        @SuppressWarnings("rawtypes")
        final List temp = query.getResultList();
        if (temp != null && !temp.isEmpty()) {
        	measurement = (EncCustomerMeasurement) temp.get(0);
        }
        return measurement;
	}
			
	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public EntityConfiguration getEntityConfiguration() {
		return entityConfiguration;
	}

	public void setEntityConfiguration(EntityConfiguration entityConfiguration) {
		this.entityConfiguration = entityConfiguration;
	}

	@Override
    @Transactional("blTransactionManager")
    public EncCustomerMeasurement save(EncCustomerMeasurement measurement) {
        return em.merge(measurement);
    }
	
	@Override
    @Transactional("blTransactionManager")
	public void delete(EncCustomerMeasurement measurement){
		ArchiveStatus as = new ArchiveStatus();
		as.setArchived('Y');
		measurement.setArchiveStatus(as);
		em.merge(measurement);
	}


    @Override
    public EncCustomerMeasurement create() {
    	
    	return (EncCustomerMeasurement) entityConfiguration.createEntityInstance(EncCustomerMeasurement.class.getName());
    }

    @Override
    public EncCustomerMeasurement create(EncCustomerMeasurement measurement) {
    	System.out.println("entiteyconf"+entityConfiguration);
    	
    	
    	
    	return (EncCustomerMeasurement) entityConfiguration.createEntityInstance("com.enclothe.core.measurement.domain.EncCustomerMeasurement");
    			//(EncCustomerMeasurement) entityConfiguration.createEntityInstance(EncCustomerMeasurementImpl.class.getName());
    }
}
