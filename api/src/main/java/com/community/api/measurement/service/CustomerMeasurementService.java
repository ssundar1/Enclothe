package com.community.api.measurement.service;

import java.util.List;

import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.Measurement;

public interface CustomerMeasurementService {
	

	EncCustomerMeasurement createEncCustomerMeasurement();

		EncCustomerMeasurement createEncCustomerMeasurementFromId(Long measurementId);

		EncCustomerMeasurement createNewEncCustomerMeasurement();

		EncCustomerMeasurement readEncCustomerMeasurementById(Long measurementId);

		Long findNextMeasurementId();

		EncCustomerMeasurement saveEncCustomerMeasurement(EncCustomerMeasurement measurement);

		List<Measurement> readActiveEncCustomerMeasurementsByCustomerId(Long customerId);

		EncCustomerMeasurement deleteEncCustomerMeasurement(EncCustomerMeasurement measurement);
		
		
		

	


}
