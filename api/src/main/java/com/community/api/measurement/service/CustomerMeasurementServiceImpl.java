package com.community.api.measurement.service;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import java.util.List;

import org.springframework.stereotype.Service;
import org.broadleafcommerce.common.id.domain.IdGenerationImpl;
//import org.broadleafcommerce.profile.core.service.IdGenerationService;

import com.community.api.measurement.dao.EncCustomerMeasurementDao;
import com.community.api.measurement.dao.MeasurementDao;
import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.Measurement;

import javax.annotation.Resource;

@Service("encCustomerMeasurementService")
public class CustomerMeasurementServiceImpl implements CustomerMeasurementService {
	
    @Resource(name="encCustomerMeasurementDao")
    protected EncCustomerMeasurementDao encCustomerMeasurementDao;
    
	/*
	 * @Resource(name="blIdGenerationService") protected  IdGenerationImpl
	 * idGenerationService;
	 */

    @Override
	public EncCustomerMeasurement createEncCustomerMeasurement() {
		return encCustomerMeasurementDao.create();
	}

	@Override
	public EncCustomerMeasurement createEncCustomerMeasurementFromId(Long measurementId) {
		EncCustomerMeasurement measurement = measurementId != null ? readEncCustomerMeasurementById(measurementId) : null;
        if (measurement == null) {
        	measurement = encCustomerMeasurementDao.create();
            if (measurementId != null) {
            	measurement.setCustomer_measurement_id(measurementId);
            } else {
            	measurement.setCustomer_measurement_id(findNextMeasurementId());
            }
        }
        return measurement;
	}

	@Override
	public EncCustomerMeasurement createNewEncCustomerMeasurement() {
		return createEncCustomerMeasurementFromId(null);
	}
	
	@Override
	public Long findNextMeasurementId() {
		//return idGenerationService.findNextId("com.enclothe.core.measurement.domain.Measurement");
		return null;
	}

	@Override
	public EncCustomerMeasurement readEncCustomerMeasurementById(Long measurementId) {
		return encCustomerMeasurementDao.readEncCustomerMeasurementById(measurementId);
	}
	
	@Override
	public EncCustomerMeasurement saveEncCustomerMeasurement(EncCustomerMeasurement measurement) {
		return encCustomerMeasurementDao.save(measurement);
	}
    	
	@Override	
	public List<Measurement> readActiveEncCustomerMeasurementsByCustomerId(Long customerId){
		
		return encCustomerMeasurementDao.readActiveCustomerMeasurementsByCustomerId(customerId);
	}

	@Override
	public EncCustomerMeasurement deleteEncCustomerMeasurement(EncCustomerMeasurement measurement) {
		encCustomerMeasurementDao.delete(measurement);		
		return null;
	}
}
