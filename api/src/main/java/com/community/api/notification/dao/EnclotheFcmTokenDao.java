package com.community.api.notification.dao;

import com.enclothe.core.notification.domain.FcmSessionToken;

public interface EnclotheFcmTokenDao {

	FcmSessionToken create();

	FcmSessionToken save(FcmSessionToken fcmSessionToken);

	void delete(FcmSessionToken fcmSessionToken);
	
	FcmSessionToken findFcmTokenByUserName(Long id);

}
