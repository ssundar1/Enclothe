package com.community.api.notification.dao;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.broadleafcommerce.common.persistence.EntityConfiguration;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enclothe.core.notification.domain.FcmSessionToken;
import com.enclothe.core.notification.domain.FcmSessionTokenImpl;

@Repository("enclotheFcmTokenDao")
public class EnclotheFcmTokenDaoImpl implements EnclotheFcmTokenDao{
	
	@PersistenceContext(unitName = "blPU")
    protected EntityManager em;
	
	@Resource(name = "blEntityConfiguration")
    protected EntityConfiguration entityConfiguration;
	
		
	
	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
	public EntityConfiguration getEntityConfiguration() {
		return entityConfiguration;
	}

	public void setEntityConfiguration(EntityConfiguration entityConfiguration) {
		this.entityConfiguration = entityConfiguration;
	}
	
	@Override
    public FcmSessionToken create() {
    	
    	return (FcmSessionToken) entityConfiguration.createEntityInstance("com.enclothe.core.notification.domain.FcmSessionToken");
    }
	@Override
    @Transactional("blTransactionManager")
    public FcmSessionToken save(FcmSessionToken fcmSessionToken) {
        return em.merge(fcmSessionToken);
    }
	
	@Override
    @Transactional("blTransactionManager")
	public void delete(FcmSessionToken fcmSessionToken){
		if (!em.contains(fcmSessionToken)) {
			fcmSessionToken = findFcmTokenByUserName(fcmSessionToken.getId());
        }
        em.remove(fcmSessionToken);		
	}

	@Override
    @Transactional("blTransactionManager")
	public FcmSessionToken findFcmTokenByUserName(Long id) {
		// TODO Auto-generated method stub
		return (FcmSessionToken) em.find(FcmSessionTokenImpl.class,id);
	}
}
