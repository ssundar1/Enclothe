package com.community.api.notification.service;

public interface EnclotheFcmNotificationService {

	void sendOrderMessageToUser(String orderId, String userName, String token);

}
