package com.community.api.notification.service;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

@Service("enclotheFcmNotificationService")
public class EnclotheFcmNotificationServiceImpl implements EnclotheFcmNotificationService{

	private static final String PROJECT_ID = "stitchcart-fa3dd";
	private static final String BASE_URL = "https://fcm.googleapis.com";
	private static final String FCM_SEND_ENDPOINT = "/v1/projects/" + PROJECT_ID + "/messages:send";

	private static final String MESSAGING_SCOPE = "https://www.googleapis.com/auth/firebase.messaging";
	private static final String[] SCOPES = { MESSAGING_SCOPE };

	private static final String TITLE = "FCM Notification";
	private static final String BODY = "Notification from FCM";
	public static final String MESSAGE_KEY = "message";

	private static final String ELMT_MSG = "message";
	private static final String ELMT_DATA = "data";
	private static final String ELMT_NOTIFICATION = "notification";
	private static final String ELMT_TOKEN = "token";
	
	@Value("${app.firebase-configuration-file}")
    private String firebaseConfigPath;
	
	public EnclotheFcmNotificationServiceImpl() {
		
	}
	/**
	 * Retrieve a valid access token that can be use to authorize requests to the FCM REST
	 * API.
	 *
	 * @return Access token.
	 * @throws IOException
	 */
	private String getAccessToken() throws IOException {
		GoogleCredential googleCredential = GoogleCredential
				.fromStream(new ClassPathResource(firebaseConfigPath).getInputStream())
				.createScoped(Arrays.asList(SCOPES));
		googleCredential.refreshToken();
		return googleCredential.getAccessToken();
	}

	/**
	 * Create HttpURLConnection that can be used for both retrieving and publishing.
	 *
	 * @return Base HttpURLConnection.
	 * @throws IOException
	 */
	private HttpURLConnection getConnection() throws IOException {
		URL url = new URL(BASE_URL + FCM_SEND_ENDPOINT);
		HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
		httpURLConnection.setRequestProperty("Authorization", "Bearer " + getAccessToken());
		httpURLConnection.setRequestProperty("Content-Type", "application/json; UTF-8");
		return httpURLConnection;
	}

	/**
	 * Send request to FCM message using HTTP.
	 *
	 * @param fcmMessage Body of the HTTP request.
	 * @throws IOException
	 */
	private  void sendMessage(JsonObject fcmMessage) throws IOException {
		HttpURLConnection connection = getConnection();
		connection.setDoOutput(true);
		DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
		outputStream.writeBytes(fcmMessage.toString());
		outputStream.flush();
		outputStream.close();

		int responseCode = connection.getResponseCode();
		if (responseCode == 200) {
			String response = inputstreamToString(connection.getInputStream());
			System.out.println("Message sent to Firebase for delivery, response:");
			System.out.println(response);
		} else {
			System.out.println("Unable to send message to Firebase:");
			String response = inputstreamToString(connection.getErrorStream());
			System.out.println(response);
		}
	}

	/**
	 * Send a message that uses the common FCM fields to send a notification message to all
	 * platforms. Also platform specific overrides are used to customize how the message is
	 * received on Android and iOS.
	 *
	 * @throws IOException
	 */
	private  void sendOverrideMessage() throws IOException {
		JsonObject overrideMessage = buildOverrideMessage();
		System.out.println("FCM request body for override message:");
		prettyPrint(overrideMessage);
		sendMessage(overrideMessage);
	}

	/**
	 * Build the body of an FCM request. This body defines the common notification object
	 * as well as platform specific customizations using the android and apns objects.
	 *
	 * @return JSON representation of the FCM request body.
	 */
	private JsonObject buildOverrideMessage() {
		JsonObject jNotificationMessage = buildNotificationMessage();

		JsonObject messagePayload = jNotificationMessage.get(MESSAGE_KEY).getAsJsonObject();
		messagePayload.add("android", buildAndroidOverridePayload());

		JsonObject apnsPayload = new JsonObject();
		apnsPayload.add("headers", buildApnsHeadersOverridePayload());
		apnsPayload.add("payload", buildApsOverridePayload());

		messagePayload.add("apns", apnsPayload);

		jNotificationMessage.add(MESSAGE_KEY, messagePayload);

		return jNotificationMessage;
	}

	/**
	 * Build the android payload that will customize how a message is received on Android.
	 *
	 * @return android payload of an FCM request.
	 */
	private JsonObject buildAndroidOverridePayload() {
		JsonObject androidNotification = new JsonObject();
		androidNotification.addProperty("click_action", "android.intent.action.MAIN");

		JsonObject androidNotificationPayload = new JsonObject();
		androidNotificationPayload.add("notification", androidNotification);

		return androidNotificationPayload;
	}

	/**
	 * Build the apns payload that will customize how a message is received on iOS.
	 *
	 * @return apns payload of an FCM request.
	 */
	private JsonObject buildApnsHeadersOverridePayload() {
		JsonObject apnsHeaders = new JsonObject();
		apnsHeaders.addProperty("apns-priority", "10");

		return apnsHeaders;
	}

	/**
	 * Build aps payload that will add a badge field to the message being sent to
	 * iOS devices.
	 *
	 * @return JSON object with aps payload defined.
	 */
	private JsonObject buildApsOverridePayload() {
		JsonObject badgePayload = new JsonObject();
		badgePayload.addProperty("badge", 1);

		JsonObject apsPayload = new JsonObject();
		apsPayload.add("aps", badgePayload);

		return apsPayload;
	}

	/**
	 * Send notification message to FCM for delivery to registered devices.
	 *
	 * @throws IOException
	 */
	public void sendCommonMessage() throws IOException {
		JsonObject notificationMessage = buildNotificationMessage();
		System.out.println("FCM request body for message using common notification object:");
		prettyPrint(notificationMessage);
		sendMessage(notificationMessage);
	}

	/**
	 * Construct the body of a notification message request.
	 *
	 * @return JSON of notification message.
	 */
	private JsonObject buildNotificationMessage() {

		JsonObject jNotification = new JsonObject();
		jNotification.addProperty("title", TITLE);
		jNotification.addProperty("body", BODY);

		JsonObject jData = new JsonObject();
		jData.addProperty("service", "FUTURE");
		jData.addProperty("route", "UserCalls");

		JsonObject jMessage = new JsonObject();
		jMessage.add("data", jData);
		jMessage.add("notification", jNotification);
		jMessage.addProperty("token", "cIZ2Oj1QXY0:APA91bFS7r50HF2bttKyLnY_aXHeUj4EJfeLlC36ngyJLY2gr04JuZq4eM0rGWCV38jMfKpiEW_7Mi-1coJbbb1RZ5lcDTAC61nh244noPjF4A4RzMurD07oKi66f54cy8YIDl31-NJtIwQ70b48RyOgUIDwSi1kGw");

		JsonObject jFcm = new JsonObject();
		jFcm.add(MESSAGE_KEY, jMessage);

		return jFcm;
	}

	/**
	 * Construct the body of a notification message request.
	 *
	 * @return JSON of notification message.
	 */
	private JsonObject buildNotificationMessage(
			String title,
			String body,
			String token,
			Map<String, String> data) {

		JsonObject jMessage = new JsonObject();
		JsonObject jData = null;

		JsonObject jNotification = new JsonObject();
		jNotification.addProperty("title", title);
		jNotification.addProperty("body", body);

		if (data != null) {
			jData = new JsonObject();
			Set<String> keys = data.keySet();
			for(String key : keys) {
				String val = data.get(key);
				jData.addProperty(key, val);
			}
			jMessage.add(ELMT_DATA, jData);
		}
		
		jMessage.add(ELMT_NOTIFICATION, jNotification);
		jMessage.addProperty(ELMT_TOKEN, token);

		JsonObject jFcm = new JsonObject();
		jFcm.add(ELMT_MSG, jMessage);

		return jFcm;
	}

	/**
	 * Read contents of InputStream into String.
	 *
	 * @param inputStream InputStream to read.
	 * @return String containing contents of InputStream.
	 * @throws IOException
	 */
	private static String inputstreamToString(InputStream inputStream) throws IOException {
		StringBuilder stringBuilder = new StringBuilder();
		Scanner scanner = new Scanner(inputStream);
		while (scanner.hasNext()) {
			stringBuilder.append(scanner.nextLine());
		}
		return stringBuilder.toString();
	}

	/**
	 * Pretty print a JsonObject.
	 *
	 * @param jsonObject JsonObject to pretty print.
	 */
	private static void prettyPrint(JsonObject jsonObject) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		System.out.println(gson.toJson(jsonObject) + "\n");
	}
	
	
	@Override
	@Async
	public void sendOrderMessageToUser(String orderId, String userName, String token) {
		
		System.out.println("Sending personal message to: " + orderId + " token: " + token);
		
		Map<String, String> data = new HashMap<>();
		data.put("route","Home");

		String msg = orderId + " is order reference number for " + userName;
		JsonObject notificationMessage = buildNotificationMessage("StitchCart", msg, token, data);
		System.out.println("notificationMessage: " + notificationMessage);
	    prettyPrint(notificationMessage);
	    try {
	    	System.out.println("Enclothe FcmNotification Before sending Message");
	    	sendMessage(notificationMessage);
	    	System.out.println("Enclothe FcmNotification After sending Message");
	    }catch(Exception exception){
	    	System.out.println(exception);
	    }

	}
}
