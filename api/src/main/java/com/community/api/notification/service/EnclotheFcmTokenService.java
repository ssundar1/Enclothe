package com.community.api.notification.service;

import com.enclothe.core.notification.domain.FcmSessionToken;

public interface EnclotheFcmTokenService {

	FcmSessionToken persist(FcmSessionToken fcmSessionToken);

	FcmSessionToken getFcmSessionTokenByUsername(Long id);

}
