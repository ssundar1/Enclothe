package com.community.api.notification.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.community.api.notification.dao.EnclotheFcmTokenDaoImpl;
import com.enclothe.core.notification.domain.FcmSessionToken;


@Service("enclotheFcmTokenService")
public class EnclotheFcmTokenServiceImpl implements EnclotheFcmTokenService {
	
	@Resource(name = "enclotheFcmTokenDao")
	protected EnclotheFcmTokenDaoImpl enclotheFcmTokenDao;
	
	@Override
	public FcmSessionToken persist(FcmSessionToken fcmSessionToken) {
		return enclotheFcmTokenDao.save(fcmSessionToken)	;
	}
	
	@Override
	public FcmSessionToken getFcmSessionTokenByUsername(Long id) {
		return enclotheFcmTokenDao.findFcmTokenByUserName(id);
	}

}
