package com.community.api.order.dao;

import java.util.Date;
import java.util.List;

import org.broadleafcommerce.core.catalog.dao.ProductDao;
import org.broadleafcommerce.core.catalog.domain.Product;

import com.enclothe.core.dm.order.domain.EncOrder;
//import com.enclothe.core.payment.domain.Payment;


public interface EncOrderDao{
   public List<EncOrder> getAllOrdersForCustomer(String customerId); 
  // public void setPaymentReferenceForOrder(String paymentRefernce);
  // public void setAdditionalInfoForOrder(String paymentRefernce);

	public String getPaymentReferenceForOrder(String orderNo) ;
	public String getAdditionalInfoForOrder(String orderNo) ;
	public Date getMaterialPickupDateTime(String orderNo);
	
	public void setMaterialPickupDateTime(String orderNo,Date pickupDateTime);
	public  EncOrder create();
	public EncOrder save(EncOrder encOrder);
	public void delete(EncOrder encOrder);
	public EncOrder findOrderByID(Long orderID);
	
}
