package com.community.api.order.dao;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.broadleafcommerce.common.persistence.EntityConfiguration;
import org.broadleafcommerce.common.util.DateUtil;
import org.broadleafcommerce.core.catalog.dao.ProductDaoImpl;
import org.broadleafcommerce.core.catalog.domain.Product;
import org.broadleafcommerce.core.order.domain.OrderImpl;
import org.hibernate.jpa.QueryHints;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.community.api.product.dao.EncProductDao;
import com.enclothe.core.dm.order.domain.EncOrder;
import com.enclothe.core.dm.order.domain.EncOrderImpl;
//import com.enclothe.core.payment.domain.Payment;
//import com.enclothe.core.payment.domain.PaymentImpl;

@Repository("encOrderDao")
public class EncOrderDaoImpl extends OrderImpl implements EncOrderDao {

	/*
	 * @Override public List<Product> readActiveProductsByCategoryExternalId(String
	 * externalId) { Date currentDate =
	 * DateUtil.getCurrentDateAfterFactoringInDateResolution(cachedDate,
	 * getCurrentDateResolution()); TypedQuery<Product> query =
	 * em.createNamedQuery("EC_READ_PRODUCTS_BY_CATEGORY_EXTERNAL_ID",
	 * Product.class); query.setParameter("externalId", externalId);
	 * query.setParameter("currentDate", currentDate);
	 * query.setHint(QueryHints.HINT_CACHEABLE, true);
	 * query.setHint(QueryHints.HINT_CACHE_REGION, "query.Catalog");
	 * 
	 * return query.getResultList(); }
	 */
	@PersistenceContext(unitName = "blPU")
    protected EntityManager em;
	
	@Resource(name = "blEntityConfiguration")
    protected EntityConfiguration entityConfiguration;
	
		
	
	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
	public EntityConfiguration getEntityConfiguration() {
		return entityConfiguration;
	}

	public void setEntityConfiguration(EntityConfiguration entityConfiguration) {
		this.entityConfiguration = entityConfiguration;
	}
	 @Override
	    public EncOrder create() {
	    	
	    	return (EncOrder) entityConfiguration.createEntityInstance("com.enclothe.core.dm.order.domain.EncOrder");
	    }
	@Override
	public List<EncOrder> getAllOrdersForCustomer(String customerId) {
		// TODO Auto-generated method stub
		return (List<EncOrder>) em.find(EncOrderImpl.class, customerId);
	}
	@Override
	public EncOrder findOrderByID(Long orderID) {
		// TODO Auto-generated method stub
		return (EncOrder) em.find(EncOrderImpl.class, orderID);
	}
	@Override
    @Transactional("blTransactionManager")
    public EncOrder save(EncOrder encOrder) {
        return em.merge(encOrder);
    }
	
	@Override
    @Transactional("blTransactionManager")
	public void delete(EncOrder encOrder){
		if (!em.contains(encOrder)) {
			encOrder = findOrderByID(encOrder.getId());
        }
        em.remove(encOrder);		
	}
	

	

	@Override
	public String getPaymentReferenceForOrder(String orderNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAdditionalInfoForOrder(String orderNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getMaterialPickupDateTime(String orderNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setMaterialPickupDateTime(String orderNo, Date pickupDateTime) {
		// TODO Auto-generated method stub
		
	}

	

}
