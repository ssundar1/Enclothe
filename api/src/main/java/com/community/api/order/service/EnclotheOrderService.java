package com.community.api.order.service;

import java.util.Date;

import org.broadleafcommerce.core.order.domain.Order;

import com.enclothe.core.dm.order.domain.EncOrder;

public interface EnclotheOrderService {
	
	public void setPaymentReferenceForOrder(String orderNo,String paymentRefernce) ;
	public String getPaymentReferenceForOrder(String orderNo) ;

	public void setMaterial_pickup_date_time(String orderNo,Date material_pickup_date_time);
	public Date getMaterial_pickup_date_time(String orderNo);
	
	 public String getAdditionalInfo(String orderNo);
	    
	    public void setAdditionalInfo( String orderNo,String additionalInfo );
	    public EncOrder findOrderByID(Long orderID);
	    public EncOrder persist(EncOrder order);


}
