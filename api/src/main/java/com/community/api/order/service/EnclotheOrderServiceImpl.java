package com.community.api.order.service;

import java.util.Date;

import javax.annotation.Resource;

import org.broadleafcommerce.core.order.domain.Order;
import org.springframework.stereotype.Service;

import com.community.api.order.dao.EncOrderDaoImpl;
import com.community.api.product.dao.EncProductDaoImpl;
import com.enclothe.core.dm.order.domain.EncOrder;
@Service("encOrderService")
public class EnclotheOrderServiceImpl implements EnclotheOrderService {

	@Resource(name = "encOrderDao")
	protected EncOrderDaoImpl encOrderDao;

	public void setPaymentReferenceForOrder(String orderNo,String paymentRefernce) {
		if(!(orderNo == null) && !orderNo.isEmpty() ) {
			
			
			EncOrder order = encOrderDao.findOrderByID(Long.parseLong(orderNo));
			order.setOrderPaymentReference(paymentRefernce);
			encOrderDao.save(order);

			}
	}

	public String getPaymentReferenceForOrder(String orderNo) {
		
		String paymentrefNo =null;
		if(!(orderNo == null) && !orderNo.isEmpty() ) {
			
		
		EncOrder order = encOrderDao.findOrderByID(Long.parseLong(orderNo));
		paymentrefNo = order.getOrderPaymentReference();
		}
		
		return paymentrefNo;
	}

	@Override
	public Date getMaterial_pickup_date_time(String orderNo) {
		Date pickupDateTime =null;
		if(!(orderNo == null) && !orderNo.isEmpty() ) {
			
		
		EncOrder order = encOrderDao.findOrderByID(Long.parseLong(orderNo));
		pickupDateTime = order.getMaterial_pickup_date_time();
		}
		
		return pickupDateTime;
	}

	@Override
	public void setMaterial_pickup_date_time(String orderNo,Date material_pickup_date_time) {
		// TODO Auto-generated method stub
if(!(orderNo == null) && !orderNo.isEmpty() ) {
			
			
			EncOrder order = encOrderDao.findOrderByID(Long.parseLong(orderNo));
			order.setMaterial_pickup_date_time(material_pickup_date_time);
			encOrderDao.save(order);
			}
	}

	@Override
	public String getAdditionalInfo(String orderNo) {
		String additionalInfo =null;
		if(!(orderNo == null) && !orderNo.isEmpty() ) {
			
		
		EncOrder order = encOrderDao.findOrderByID(Long.parseLong(orderNo));
		additionalInfo = order.getAdditionalInfo();
		}
		
		return additionalInfo;
	}

	@Override
	public void setAdditionalInfo(String orderNo, String additionalInfo) {
if(!(orderNo == null) && !orderNo.isEmpty() ) {
			
			
			EncOrder order = encOrderDao.findOrderByID(Long.parseLong(orderNo));
			order.setAdditionalInfo(additionalInfo);
			encOrderDao.save(order);
			}
		
	}

	@Override
	public EncOrder findOrderByID(Long orderID) {
		EncOrder order =null;
		
if(!(orderID == null)  ) {
			
			
	order = encOrderDao.findOrderByID(orderID);
			
			}
		
		return order;
	}

	@Override
	public EncOrder persist(EncOrder order) {
return encOrderDao.save(order)	;
	}
}
