package com.community.api.product.dao;

import java.util.Date;
import java.util.List;

import org.broadleafcommerce.core.catalog.dao.ProductDao;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.broadleafcommerce.core.catalog.domain.Product;


public interface EncProductDao{
   public List<Product> readActiveProductsByCategoryExternalId(String externalId);

public List<Category> readActiveCategoryByExternalId(String externalId); 

}
