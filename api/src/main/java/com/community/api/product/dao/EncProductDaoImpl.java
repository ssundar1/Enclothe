package com.community.api.product.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.broadleafcommerce.common.util.DateUtil;
import org.broadleafcommerce.core.catalog.dao.ProductDaoImpl;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.broadleafcommerce.core.catalog.domain.Product;
import org.hibernate.jpa.QueryHints;
import org.springframework.stereotype.Repository;


@Repository("EncProductDao")
public class EncProductDaoImpl extends ProductDaoImpl implements EncProductDao {

	@Override
	public List<Product> readActiveProductsByCategoryExternalId(String externalId) {
		 Date currentDate = DateUtil.getCurrentDateAfterFactoringInDateResolution(cachedDate, getCurrentDateResolution());
		 TypedQuery<Product> query = em.createNamedQuery("EC_READ_PRODUCTS_BY_CATEGORY_EXTERNAL_ID", Product.class);
	        query.setParameter("externalId", externalId);
	        query.setParameter("currentDate", currentDate);
	        query.setHint(QueryHints.HINT_CACHEABLE, true);
	        query.setHint(QueryHints.HINT_CACHE_REGION, "query.Catalog");
		 
	        return query.getResultList();
	}
	@Override
	public List<Category> readActiveCategoryByExternalId(String externalId) {
		 Date currentDate = DateUtil.getCurrentDateAfterFactoringInDateResolution(cachedDate, getCurrentDateResolution());
		 TypedQuery<Category> query = em.createNamedQuery("EC_READ_CATEGORY_BY_EXTERNAL_ID", Category.class);
	        query.setParameter("externalId", externalId);
	        query.setParameter("currentDate", currentDate);
	        query.setHint(QueryHints.HINT_CACHEABLE, true);
	        query.setHint(QueryHints.HINT_CACHE_REGION, "query.Catalog");
		 
	        return query.getResultList();
	}

	

}
