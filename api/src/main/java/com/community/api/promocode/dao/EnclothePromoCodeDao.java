package com.community.api.promocode.dao;

import java.util.List;

import com.enclothe.core.promocode.domain.EncPromoCode;

public interface EnclothePromoCodeDao {

	EncPromoCode create();

	EncPromoCode save(EncPromoCode encPromoCode);

	void delete(EncPromoCode encPromoCode);

	EncPromoCode findByPromoCodeId(Long id);

	List<EncPromoCode> findByResellerId(Long resellerId);
	
	public List<EncPromoCode> findByPromoCode(String promoCode);
}
