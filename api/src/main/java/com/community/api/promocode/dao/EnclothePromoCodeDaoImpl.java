package com.community.api.promocode.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.broadleafcommerce.common.persistence.EntityConfiguration;
import org.hibernate.jpa.QueryHints;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enclothe.core.promocode.domain.EncPromoCode;
import com.enclothe.core.reseller.domain.EncReseller;

@Repository("enclothePromoCodeDao")
public class EnclothePromoCodeDaoImpl implements EnclothePromoCodeDao {

	@PersistenceContext(unitName = "blPU")
    protected EntityManager em;
	
	@Resource(name = "blEntityConfiguration")
    protected EntityConfiguration entityConfiguration;
	
		
	
	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
	public EntityConfiguration getEntityConfiguration() {
		return entityConfiguration;
	}

	public void setEntityConfiguration(EntityConfiguration entityConfiguration) {
		this.entityConfiguration = entityConfiguration;
	}
	@Override
	public EncPromoCode create() {
		// TODO Auto-generated method stub
		return (EncPromoCode) entityConfiguration.createEntityInstance("com.enclothe.core.promocode.domain.EncPromoCode");
	}

	@Override
    @Transactional("blTransactionManager")
	public EncPromoCode save(EncPromoCode encPromoCode) {
		// TODO Auto-generated method stub
		return em.merge(encPromoCode);
	}

	@Override
	@Transactional("blTransactionManager")
	public void delete(EncPromoCode encPromoCode) {
		// TODO Auto-generated method stub
		if (!em.contains(encPromoCode)) {
			encPromoCode = findByPromoCodeId(encPromoCode.getId());
        }
        em.remove(encPromoCode);
	}

	@Override
	@Transactional("blTransactionManager")
	public EncPromoCode findByPromoCodeId(Long id) {
		// TODO Auto-generated method stub
		return (EncPromoCode) em.find(EncPromoCode.class,id);
	}
	
	@Override
	@Transactional("blTransactionManager")
	public List<EncPromoCode> findByResellerId(Long resellerId) {
		// TODO Auto-generated method stub
		TypedQuery<EncPromoCode> query = em.createNamedQuery("BC_READ_PROMOCODE_BY_RESELLER_ID", EncPromoCode.class);
        query.setParameter("resellerId", resellerId);
        query.setHint(QueryHints.HINT_CACHEABLE, true);
        query.setHint(QueryHints.HINT_CACHE_REGION, "query.EncPromoCode");
        return query.getResultList();
	}
	
	@Override
	@Transactional("blTransactionManager")
	public List<EncPromoCode> findByPromoCode(String promoCode) {
		// TODO Auto-generated method stub
		System.out.println("promoCode====="+promoCode);
		TypedQuery<EncPromoCode> query = em.createNamedQuery("BC_READ_PROMOCODE_BY_PROMO_CODE", EncPromoCode.class);
        query.setParameter("promoCode", promoCode);
        query.setHint(QueryHints.HINT_CACHEABLE, true);
        query.setHint(QueryHints.HINT_CACHE_REGION, "query.EncPromoCode");
        //System.out.println("query.getResultList()====="+query.getResultList().size());
        return query.getResultList();
	}
}
