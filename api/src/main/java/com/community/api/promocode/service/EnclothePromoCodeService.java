package com.community.api.promocode.service;

import java.util.List;

import com.enclothe.core.promocode.domain.EncPromoCode;

public interface EnclothePromoCodeService {

	EncPromoCode persist(EncPromoCode encPromoCode);

	EncPromoCode findByPromoCodeId(Long id);

	List<EncPromoCode> findByResellerId(Long resellerId);
	
	List<EncPromoCode> findByPromoCode(String promocode);

}
