package com.community.api.promocode.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.community.api.promocode.dao.EnclothePromoCodeDao;
import com.enclothe.core.promocode.domain.EncPromoCode;

@Service("enclothePromoCodeService")
public class EnclothePromoCodeServiceImpl implements EnclothePromoCodeService {
	
	@Resource(name = "enclothePromoCodeDao")
	protected EnclothePromoCodeDao enclothePromoCodeDao;

	@Override
	public EncPromoCode persist(EncPromoCode encPromoCode) {
		// TODO Auto-generated method stub
		return enclothePromoCodeDao.save(encPromoCode);
	}

	@Override
	public EncPromoCode findByPromoCodeId(Long id) {
		// TODO Auto-generated method stub
		return enclothePromoCodeDao.findByPromoCodeId(id);
	}

	@Override
	public List<EncPromoCode> findByResellerId(Long resellerId) {
		// TODO Auto-generated method stub
		return (List<EncPromoCode>) enclothePromoCodeDao.findByResellerId(resellerId);
	}
	
	@Override
	public List<EncPromoCode> findByPromoCode(String promocode) {
		// TODO Auto-generated method stub
		return (List<EncPromoCode>) enclothePromoCodeDao.findByPromoCode(promocode);
	}
}
