package com.community.api.reseller.dao;

import java.util.List;

import com.enclothe.core.reseller.domain.EncReseller;
import com.enclothe.core.xref.domain.EncOrderResellerXref;

public interface EnclotheOrderResellerXrefDao {

	EncOrderResellerXref create();
	EncOrderResellerXref save(EncOrderResellerXref encOrderResellerXref);
	void delete(EncOrderResellerXref encReseller);
	EncOrderResellerXref findByEncOrderResellerXrefId(Long id);
}
