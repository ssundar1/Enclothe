package com.community.api.reseller.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.broadleafcommerce.common.persistence.EntityConfiguration;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.hibernate.jpa.QueryHints;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enclothe.core.dm.order.domain.EncOrder;
import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.Measurement;
import com.enclothe.core.reseller.domain.EncReseller;
import com.enclothe.core.xref.domain.EncOrderResellerXref;

@Repository("enclotheOrderResellerXrefDao")
public class EnclotheOrderResellerXrefDaoImpl implements EnclotheOrderResellerXrefDao {
	
	@PersistenceContext(unitName = "blPU")
    protected EntityManager em;
	
	@Resource(name = "blEntityConfiguration")
    protected EntityConfiguration entityConfiguration;
	
		
	
	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
	public EntityConfiguration getEntityConfiguration() {
		return entityConfiguration;
	}

	public void setEntityConfiguration(EntityConfiguration entityConfiguration) {
		this.entityConfiguration = entityConfiguration;
	}

	
	@Override
    @Transactional("blTransactionManager")
	public EncOrderResellerXref save(EncOrderResellerXref encOrderResellerXref) {
		// TODO Auto-generated method stub
		return em.merge(encOrderResellerXref);
	}


	
	
	
	@Override
	@Transactional("blTransactionManager")
	public void delete(EncOrderResellerXref encOrderResellerXref) {
		// TODO Auto-generated method stub
		if (!em.contains(encOrderResellerXref)) {
			encOrderResellerXref = findByEncOrderResellerXrefId(encOrderResellerXref.getOrder_reseller_id());
        }
        em.remove(encOrderResellerXref);
	}

	@Override
	public EncOrderResellerXref findByEncOrderResellerXrefId(Long order_reseller_id) {
		// TODO Auto-generated method stub
		EncOrderResellerXref resellerXref =null;
		TypedQuery<EncOrderResellerXref> query = em.createNamedQuery("BC_READ_ENC_ORDER_RESELLERS_XREF_ID", EncOrderResellerXref.class);
        query.setParameter("order_reseller_id", order_reseller_id);
        query.setHint(QueryHints.HINT_CACHEABLE, true);
        query.setHint(QueryHints.HINT_CACHE_REGION, "query.EncOrderResellerXref");
        
        final List temp = query.getResultList();
        if (temp != null && !temp.isEmpty()) {
        	resellerXref = (EncOrderResellerXref) temp.get(0);
        }
	 
        return resellerXref;	}

	@Override
	public EncOrderResellerXref create() {
		// TODO Auto-generated method stub
    	return (EncOrderResellerXref) entityConfiguration.createEntityInstance("com.enclothe.core.xref.domain.EncOrderResellerXref");

	}

}
