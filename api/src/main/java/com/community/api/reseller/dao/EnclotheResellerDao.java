package com.community.api.reseller.dao;

import java.util.List;

import com.enclothe.core.reseller.domain.EncReseller;

public interface EnclotheResellerDao {

	EncReseller create();
	EncReseller save(EncReseller encReseller);
	void delete(EncReseller encReseller);
	EncReseller findByResellerId(Long id);
	EncReseller authenticateReseller(String userName, String pass);
	List<EncReseller> findByCustomerId(Long id);
}
