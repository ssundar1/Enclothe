package com.community.api.reseller.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.broadleafcommerce.common.persistence.EntityConfiguration;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.hibernate.jpa.QueryHints;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enclothe.core.dm.order.domain.EncOrder;
import com.enclothe.core.reseller.domain.EncReseller;

@Repository("enclotheResellerDao")
public class EnclotheResellerDaoImpl implements EnclotheResellerDao {
	
	@PersistenceContext(unitName = "blPU")
    protected EntityManager em;
	
	@Resource(name = "blEntityConfiguration")
    protected EntityConfiguration entityConfiguration;
	
		
	
	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
	public EntityConfiguration getEntityConfiguration() {
		return entityConfiguration;
	}

	public void setEntityConfiguration(EntityConfiguration entityConfiguration) {
		this.entityConfiguration = entityConfiguration;
	}

	@Override
	public EncReseller create() {
		// TODO Auto-generated method stub
		return (EncReseller) entityConfiguration.createEntityInstance("com.enclothe.core.reseller.domain.EncReseller");
	}

	@Override
    @Transactional("blTransactionManager")
	public EncReseller save(EncReseller encReseller) {
		System.out.println("enc reseller aadhar"+encReseller.getAdhaarCode());
		// TODO Auto-generated method stub
		return em.merge(encReseller);
	}

	@Override
	@Transactional("blTransactionManager")
	public void delete(EncReseller encReseller) {
		// TODO Auto-generated method stub
		if (!em.contains(encReseller)) {
			encReseller = findByResellerId(encReseller.getId());
        }
        em.remove(encReseller);
	}

	@Override
	@Transactional("blTransactionManager")
	public EncReseller findByResellerId(Long id) {
		// TODO Auto-generated method stub
		return (EncReseller) em.find(EncReseller.class,id);
	}
	@Override
	@Transactional("blTransactionManager")
	public List<EncReseller> findByCustomerId(Long customerId) {
		// TODO Auto-generated method stub
		TypedQuery<EncReseller> query = em.createNamedQuery("BC_READ_RESELLERS_BY_CUSTOMER_ID", EncReseller.class);
        query.setParameter("customerId", customerId);
        query.setHint(QueryHints.HINT_CACHEABLE, true);
        query.setHint(QueryHints.HINT_CACHE_REGION, "query.EncReseller");
	 
        return query.getResultList();
		
	}
	@Override
	@Transactional("blTransactionManager")
	public EncReseller authenticateReseller(String userName, String pass) {
		// TODO Auto-generated method stub
		return (EncReseller) em.createNativeQuery("select * from enclothe.ENC_RESELLER where RESELLER_NAME='"+userName+"' AND PASSWORD="+pass, EncReseller.class);
	}

}
