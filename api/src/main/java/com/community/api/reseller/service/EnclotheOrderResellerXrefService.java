package com.community.api.reseller.service;

import java.util.List;

import org.broadleafcommerce.profile.core.service.CustomerService;

import com.enclothe.core.reseller.domain.EncReseller;
import com.enclothe.core.xref.domain.EncOrderResellerXref;

public interface EnclotheOrderResellerXrefService{

	EncOrderResellerXref create();
	public EncOrderResellerXref persist(EncOrderResellerXref encOrderResellerXref);
	void delete(EncOrderResellerXref encReseller);
	EncOrderResellerXref findByEncOrderResellerXrefId(Long id);
}
