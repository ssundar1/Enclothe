package com.community.api.reseller.service;

import java.util.List;

import javax.annotation.Resource;

import org.broadleafcommerce.profile.core.service.CustomerServiceImpl;
import org.springframework.stereotype.Service;

import com.community.api.reseller.dao.EnclotheOrderResellerXrefDao;
import com.community.api.reseller.dao.EnclotheResellerDao;
import com.enclothe.core.reseller.domain.EncReseller;
import com.enclothe.core.xref.domain.EncOrderResellerXref;

@Service("enclotheOrderResellerXrefService")
public class EnclotheOrderResellerXrefServiceImpl implements EnclotheOrderResellerXrefService{
	
	@Resource(name = "enclotheOrderResellerXrefDao")
	protected EnclotheOrderResellerXrefDao enclotheOrderResellerXrefDao;

	
	@Override
	public EncOrderResellerXref persist(EncOrderResellerXref encOrderResellerXref) {
		// TODO Auto-generated method stub
		return enclotheOrderResellerXrefDao.save(encOrderResellerXref);
	}

	
	

	@Override
	public EncOrderResellerXref create() {
		// TODO Auto-generated method stub
		return enclotheOrderResellerXrefDao.create();
	}

	@Override
	public void delete(EncOrderResellerXref encOrderResellerXref) {
		enclotheOrderResellerXrefDao.delete(encOrderResellerXref);		
	}

	@Override
	public EncOrderResellerXref findByEncOrderResellerXrefId(Long id) {
		// TODO Auto-generated method stub
		return enclotheOrderResellerXrefDao.findByEncOrderResellerXrefId(id);
	}

}
