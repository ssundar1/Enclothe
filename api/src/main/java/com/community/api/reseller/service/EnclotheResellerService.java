package com.community.api.reseller.service;

import java.util.List;

import org.broadleafcommerce.profile.core.service.CustomerService;

import com.enclothe.core.reseller.domain.EncReseller;

public interface EnclotheResellerService{

	EncReseller authenticateReseller(String userName, String pass);
	EncReseller persist(EncReseller encReseller);
	EncReseller findByResellerId(Long id);
	List<EncReseller> findByCustomerId(Long id);
}
