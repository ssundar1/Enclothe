package com.community.api.reseller.service;

import java.util.List;

import javax.annotation.Resource;

import org.broadleafcommerce.profile.core.service.CustomerServiceImpl;
import org.springframework.stereotype.Service;

import com.community.api.reseller.dao.EnclotheResellerDao;
import com.enclothe.core.reseller.domain.EncReseller;

@Service("enclotheResellerService")
public class EnclotheResellerServiceImpl implements EnclotheResellerService{
	
	@Resource(name = "enclotheResellerDao")
	protected EnclotheResellerDao enclotheResellerDao;

	@Override
	public EncReseller authenticateReseller(String userName, String pass) {
		// TODO Auto-generated method stub
		return enclotheResellerDao.authenticateReseller(userName, pass);
	}

	@Override
	public EncReseller persist(EncReseller encReseller) {
		// TODO Auto-generated method stub
		System.out.println("service encReseller aadhar"+encReseller.getAdhaarCode());
		return enclotheResellerDao.save(encReseller);
	}

	@Override
	public EncReseller findByResellerId(Long id) {
		// TODO Auto-generated method stub
		return enclotheResellerDao.findByResellerId(id);
	}
	
	@Override
	public List<EncReseller> findByCustomerId(Long customerId) {
		// TODO Auto-generated method stub
		return enclotheResellerDao.findByCustomerId(customerId);
	}

}
