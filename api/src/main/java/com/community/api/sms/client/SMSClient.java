package com.community.api.sms.client;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;

import com.community.api.sms.handler.SMSProperties;
import com.community.api.sms.modal.SMSRequest;
import com.community.api.sms.modal.SMSResponse;


@Component
public class SMSClient {
	
	private final RestOperations rest;
	private final String smsServiceUrl;
	private final String apiKey;
	private final String sendSmsMethod;
	
	public SMSClient(final RestTemplateBuilder builder, final SMSProperties props){
		this.rest = builder.setReadTimeout(props.getReadTimeout())
						.setConnectTimeout(props.getConnectTimeout())
						.build();
		this.smsServiceUrl = props.getSmsServiceUrl();
		this.apiKey = props.getApiKey();
		this.sendSmsMethod = props.getSendSmsMethod();
	}
	
	public SMSResponse sendSMS(SMSRequest smsRequest) {
		System.out.println(smsRequest.toString());
		SMSResponse smsResponse = rest.postForObject(smsServiceUrl, smsRequest, SMSResponse.class, this.apiKey,this.sendSmsMethod);
		if (smsResponse != null){
			System.out.println(smsResponse.toString());
		}
		return smsResponse;
	}

}
