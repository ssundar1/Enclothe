package com.community.api.sms.handler;

import java.util.List;

import com.community.api.sms.messages.SMSMessage;


public interface SMSHandler {
	
	public boolean send(List<SMSMessage> smsMessages);

}
