package com.community.api.sms.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.community.api.sms.client.SMSClient;
import com.community.api.sms.messages.SMSMessage;
import com.community.api.sms.modal.SMSDetail;
import com.community.api.sms.modal.SMSRequest;


@Component
public class SMSHandlerImpl implements SMSHandler {
	
	@Autowired
	SMSProperties smsProperties;
	
	@Autowired
	SMSClient smsClient;

	public boolean send(List<SMSMessage> smsMessages) {
		
		
		System.out.println("Sending SMS");
		boolean flag = false;
		
		String sender = smsProperties.getSender();
		
		if (smsMessages != null && smsMessages.size()>0) {
			int noOfMessages = smsMessages.size();
			SMSDetail[] smsDetails = new SMSDetail[noOfMessages];
			int idx = 0;
			for (SMSMessage smsMessage:smsMessages) {
				SMSDetail smsDetail = new SMSDetail();
				
				System.out.println("SMSHandlerImpl Mobile():"+smsMessage.getMobile());
				
				
				String message = smsMessage.getMessage();
				
				smsDetail.setCustom(1);
				smsDetail.setMessage(message);
				smsDetail.setSender(sender);
				smsDetail.setTo(smsMessage.getMobile());
				smsDetails[idx] = smsDetail;
				idx++;
			}
			
			SMSRequest smsRequest = new SMSRequest();
			smsRequest.setSms(smsDetails);
			
			smsClient.sendSMS(smsRequest);
			flag = true;
		}
		return flag;
	}
}
