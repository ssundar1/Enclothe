package com.community.api.sms.handler;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("sms")
public class SMSProperties {

	private int readTimeout;
	private int connectTimeout;
	private String apiKey;
	private String smsServiceUrl;
	private String sendSmsMethod;
	private String sender;
	
	public int getReadTimeout() {
		return readTimeout;
	}
	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}
	public int getConnectTimeout() {
		return connectTimeout;
	}
	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getSmsServiceUrl() {
		return smsServiceUrl;
	}
	public void setSmsServiceUrl(String smsServiceUrl) {
		this.smsServiceUrl = smsServiceUrl;
	}
	public String getSendSmsMethod() {
		return sendSmsMethod;
	}
	public void setSendSmsMethod(String sendSmsMethod) {
		this.sendSmsMethod = sendSmsMethod;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	
}
