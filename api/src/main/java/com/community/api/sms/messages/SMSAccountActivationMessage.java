package com.community.api.sms.messages;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@ConfigurationProperties("sms")
public class SMSAccountActivationMessage extends SMSMessage {

	@Value("${sms.activate-code-message}")
	private String activateCodeMessage;
	private String activationCode;
	
	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	@Override
	public String getMessage() {
		Map<String,String> values = new HashMap<String, String>();
		values.put("activation-code", this.activationCode);
		return super.interpolate(this.activateCodeMessage, values);
	}

	@Override
	public String toString() {
		return "SMSAccountActivationMessage [activateCodeMessage=" + activateCodeMessage + ", activationCode="
				+ activationCode + ", getUserId()=" + getUserId() + ", getMobile()=" + getMobile() + "]";
	}

}
