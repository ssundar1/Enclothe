package com.community.api.sms.messages;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@ConfigurationProperties("sms")
public class SMSAccountVerificationMessage extends SMSMessage {

	@Value("${sms.verification-code-message}")
	private String verficationCodeMessage;
	private String verificationCode;
	
	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	@Override
	public String getMessage() {
		Map<String,String> values = new HashMap<String, String>();
		values.put("verification-code", this.verificationCode);
		return super.interpolate(this.verficationCodeMessage, values);
	}

	@Override
	public String toString() {
		return "SMSAccountVerificationMessage [verficationCodeMessage=" + verficationCodeMessage + ", verificationCode="
				+ verificationCode + "]";
	}

}
