package com.community.api.sms.messages;

import java.util.Map;

import org.springframework.util.PropertyPlaceholderHelper;
import org.springframework.util.PropertyPlaceholderHelper.PlaceholderResolver;

public abstract class SMSMessage {

	private long userId;
	private String mobile;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public abstract String getMessage();
	
	protected String interpolate(String msg, Map<String, String> values){
		
		System.out.println("msg>>"+msg);
		
		final PlaceholderResolver resolver = new PlaceholderResolver()
	    {
	        public String resolvePlaceholder( final String key )
	        {
	            String value = values.get(key);
	            System.out.println("Key:"+key+"  Val:"+value);
	            if ( value != null )
	            {
	                return value;
	            }

	            return "";
	        }
	    };
		final PropertyPlaceholderHelper helper = new PropertyPlaceholderHelper("{", "}", "", true);
		
		return helper.replacePlaceholders(msg, resolver);
				
	}
}
