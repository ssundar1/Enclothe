package com.community.api.sms.modal;

public class SMSDetail {

	private String to;
	private int custom;
	private String message;
	private String sender;
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public int getCustom() {
		return custom;
	}
	public void setCustom(int custom) {
		this.custom = custom;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	@Override
	public String toString() {
		return "SMSDetail [to=" + to + ", custom=" + custom + ", message=" + message + ", sender=" + sender + "]";
	}
	
	
}
