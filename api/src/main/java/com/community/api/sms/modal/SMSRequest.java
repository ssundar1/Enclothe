package com.community.api.sms.modal;

import java.util.Arrays;

public class SMSRequest {

	private SMSDetail[] sms;

	public SMSDetail[] getSms() {
		return sms;
	}

	public void setSms(SMSDetail[] sms) {
		this.sms = sms;
	}

	@Override
	public String toString() {
		return "SMSRequest [sms=" + Arrays.toString(sms) + "]";
	}
	
}
