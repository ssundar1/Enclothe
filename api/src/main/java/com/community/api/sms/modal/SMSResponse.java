package com.community.api.sms.modal;

import java.util.Arrays;

public class SMSResponse {

	private String status;
	private SMSResponseDetail[] data;
	private String message;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public SMSResponseDetail[] getData() {
		return data;
	}
	public void setData(SMSResponseDetail[] data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "SMSResponse [status=" + status + ", data=" + Arrays.toString(data) + ", message=" + message + "]";
	}
	
}
