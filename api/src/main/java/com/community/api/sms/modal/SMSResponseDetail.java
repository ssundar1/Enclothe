package com.community.api.sms.modal;

public class SMSResponseDetail {

	private String id;
	private String customid;
	private String customid1;
	private String customid2;
	private String mobile;
	private String status;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCustomid() {
		return customid;
	}
	public void setCustomid(String customid) {
		this.customid = customid;
	}
	public String getCustomid1() {
		return customid1;
	}
	public void setCustomid1(String customid1) {
		this.customid1 = customid1;
	}
	public String getCustomid2() {
		return customid2;
	}
	public void setCustomid2(String customid2) {
		this.customid2 = customid2;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "SMSResponseDetail [id=" + id + ", customid=" + customid + ", customid1=" + customid1 + ", customid2="
				+ customid2 + ", mobile=" + mobile + ", status=" + status + "]";
	}
	
}
