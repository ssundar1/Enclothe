package com.community.api.wrapper;
import java.util.Date;

import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.common.rest.api.wrapper.APIUnwrapper;
import org.broadleafcommerce.common.rest.api.wrapper.APIWrapper;
import org.broadleafcommerce.common.rest.api.wrapper.BaseWrapper;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.broadleafcommerce.rest.api.wrapper.OrderWrapper;
import com.community.api.dto.EnclotheOrderItemDTO;
import com.enclothe.core.customer.domain.EncCustomer;
import com.enclothe.core.dm.order.domain.EncOrder;
import com.enclothe.core.dm.order.domain.EncOrderImpl;
import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.EncCustomerMeasurementImpl;
import com.enclothe.core.measurement.domain.Measurement;

@XmlRootElement(name = "EncCustomerMeasurement")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class EncCustomerMeasurementWrapper  extends BaseWrapper implements APIWrapper<EncCustomerMeasurement>, APIUnwrapper<EncCustomerMeasurement>{

    public EncCustomer getCustomer() {
		return customer;
	}




	public void setCustomer(EncCustomer customer) {
		this.customer = customer;
	}




	public Measurement getMeasurement() {
		return measurement;
	}




	public void setMeasurement(Measurement measurement) {
		this.measurement = measurement;
	}




	public Date getMeasurementEndDate() {
		return measurementEndDate;
	}




	public void setMeasurementEndDate(Date measurementEndDate) {
		this.measurementEndDate = measurementEndDate;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2352353944374900018L;


    @XmlElement
    protected Long id;
    
	@XmlElement
	 protected EncCustomer customer;

    @XmlElement
	    protected Measurement measurement;
	    
    @XmlElement
 	    protected Date measurementEndDate;
    
    @XmlElement
	    protected Long customer_id;

	
	public Long getCustomer_id() {
		return customer_id;
	}




	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}




	public EncCustomerMeasurementWrapper() {
		super();
	}




	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	

	@Override
	public EncCustomerMeasurement unwrap(HttpServletRequest request, ApplicationContext context) {
EncCustomerMeasurement encCustomerMeasurement = new EncCustomerMeasurementImpl();
encCustomerMeasurement.setMeasurements(this.measurement);
encCustomerMeasurement.setCustomer(this.customer);

return encCustomerMeasurement;
	}

@Override
	public void wrapDetails(EncCustomerMeasurement model, HttpServletRequest request) {
		//this.customer = model.getCustomer();	
	this.customer_id = model.getCustomer().getId();
		this.measurement = model.getMeasurements();
		this.id = model.getCustomer_measurement_id();
	}

@Override
	public void wrapSummary(EncCustomerMeasurement model, HttpServletRequest request) {
this.wrapDetails(model, request);		
	}


	

}
