package com.community.api.wrapper;
import java.util.Date;

import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.common.rest.api.wrapper.APIUnwrapper;
import org.broadleafcommerce.common.rest.api.wrapper.APIWrapper;
import org.broadleafcommerce.common.rest.api.wrapper.BaseWrapper;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.broadleafcommerce.rest.api.wrapper.OrderWrapper;
import com.community.api.dto.EnclotheOrderItemDTO;
import com.enclothe.core.customer.domain.EncCustomer;
import com.enclothe.core.dm.order.domain.EncOrder;
import com.enclothe.core.dm.order.domain.EncOrderImpl;
import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.EncCustomerMeasurementImpl;
import com.enclothe.core.measurement.domain.Measurement;
import com.enclothe.core.measurement.domain.MeasurementImpl;

@XmlRootElement(name = "Measurement")
@XmlAccessorType(value = XmlAccessType.FIELD)
@Component
public class EncMeasurementWrapper  extends BaseWrapper implements APIWrapper<Measurement>, APIUnwrapper<Measurement>{

   




	public Long getCustomer_id() {
		return customer_id;
	}




	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}




	public Measurement getMeasurement() {
		return measurement;
	}




	public void setMeasurement(Measurement measurement) {
		this.measurement = measurement;
	}




	public Date getMeasurementEndDate() {
		return measurementEndDate;
	}




	public void setMeasurementEndDate(Date measurementEndDate) {
		this.measurementEndDate = measurementEndDate;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2352353944374900018L;


    @XmlElement
    protected Long id;
    
	@XmlElement
	 protected Long customer_id;

    @XmlElement
	    protected Measurement measurement;
	    
    @XmlElement
 	    protected Date measurementEndDate;
	
	public EncMeasurementWrapper() {
		super();
	}




	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	

	@Override
	public Measurement unwrap(HttpServletRequest request, ApplicationContext context) {
Measurement encCustomerMeasurement = new MeasurementImpl();
encCustomerMeasurement = this.measurement;

return encCustomerMeasurement;
	}

@Override
	public void wrapDetails(Measurement model, HttpServletRequest request) {
		//this.customer_id = model.getCustomer();		
		this.measurement = model;
	}

@Override
	public void wrapSummary(Measurement model, HttpServletRequest request) {
this.wrapDetails(model, request);		
	}




	

}
