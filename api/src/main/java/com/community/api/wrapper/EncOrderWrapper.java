package com.community.api.wrapper;
import java.util.Date;

import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.common.rest.api.wrapper.APIUnwrapper;
import org.broadleafcommerce.common.rest.api.wrapper.APIWrapper;
import org.broadleafcommerce.common.rest.api.wrapper.BaseWrapper;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.broadleafcommerce.rest.api.wrapper.OrderWrapper;
import com.community.api.dto.EnclotheOrderItemDTO;
import com.enclothe.core.dm.order.domain.EncOrder;
import com.enclothe.core.dm.order.domain.EncOrderImpl;

@XmlRootElement(name = "EncOrder")
@XmlAccessorType(value = XmlAccessType.FIELD)
@Component
public class EncOrderWrapper  extends BaseWrapper implements APIWrapper<EncOrder>, APIUnwrapper<EncOrder>{

    /**
	 * 
	 */
	private static final long serialVersionUID = -2352353944374900018L;


    @XmlElement
    protected Long id;
    
	@XmlElement
	 protected Date material_pickup_date_time;

    @XmlElement
	    protected String additionalInfo;
	    
    @XmlElement
 	    protected String orderPaymentReference;
	
	public EncOrderWrapper() {
		super();
	}


	public Date getMaterial_pickup_date_time() {
		return material_pickup_date_time;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public void setMaterial_pickup_date_time(Date material_pickup_date_time) {
		this.material_pickup_date_time = material_pickup_date_time;
	}


	public String getAdditionalInfo() {
		return additionalInfo;
	}


	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}


	public String getOrderPaymentReference() {
		return orderPaymentReference;
	}


	public void setOrderPaymentReference(String orderPaymentReference) {
		this.orderPaymentReference = orderPaymentReference;
	}

	@Override
	public EncOrder unwrap(HttpServletRequest request, ApplicationContext context) {
EncOrder order = new EncOrderImpl();
order.setAdditionalInfo(this.additionalInfo);
order.setOrderPaymentReference(this.orderPaymentReference);
order.setId(this.id );
order.setMaterial_pickup_date_time(this.material_pickup_date_time);
return order;
	}

@Override
	public void wrapDetails(EncOrder model, HttpServletRequest request) {
		this.material_pickup_date_time = model.getMaterial_pickup_date_time();		
		this.orderPaymentReference = model.getOrderPaymentReference();
		this.additionalInfo = model.getAdditionalInfo();
		this.id = model.getId();
	}

@Override
	public void wrapSummary(EncOrder model, HttpServletRequest request) {
this.wrapDetails(model, request);		
	}


	

}
