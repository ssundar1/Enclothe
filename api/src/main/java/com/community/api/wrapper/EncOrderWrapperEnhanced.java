package com.community.api.wrapper;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.common.rest.api.wrapper.APIUnwrapper;
import org.broadleafcommerce.common.rest.api.wrapper.APIWrapper;
import org.broadleafcommerce.common.rest.api.wrapper.BaseWrapper;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.broadleafcommerce.rest.api.wrapper.OrderWrapper;
import com.community.api.dto.EnclotheOrderItemDTO;
import com.enclothe.core.dm.order.domain.EncOrder;
import com.enclothe.core.dm.order.domain.EncOrderImpl;
import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.reseller.domain.EncReseller;
import com.enclothe.core.xref.domain.EncOrderResellerXref;

@XmlRootElement(name = "EncOrder")
@XmlAccessorType(value = XmlAccessType.FIELD)
@Component
public class EncOrderWrapperEnhanced  extends EncOrderWrapper implements APIWrapper<EncOrder>, APIUnwrapper<EncOrder>{

    /**
	 * 
	 */
	private static final long serialVersionUID = -2352353944374900018L;


    @XmlElement
    protected EncCustomerMeasurement encCustomerMeasurement;
    
	@XmlElement
	 protected EncReseller encReseller;

    @XmlElement
	    protected Long id;
	    
  
	
	public EncOrderWrapperEnhanced() {
		super();
	}




	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}



	


	public EncCustomerMeasurement getEncCustomerMeasurement() {
		return encCustomerMeasurement;
	}




	public void setEncCustomerMeasurement(EncCustomerMeasurement encCustomerMeasurement) {
		this.encCustomerMeasurement = encCustomerMeasurement;
	}




	public EncReseller getEncReseller() {
		return encReseller;
	}




	public void setEncReseller(EncReseller encReseller) {
		this.encReseller = encReseller;
	}




	@Override
	public EncOrder unwrap(HttpServletRequest request, ApplicationContext context) {
EncOrder order = new EncOrderImpl();
order.setOrderResellerXref((List<EncOrderResellerXref>) this.encReseller);
order.setCustomerMeasurement(this.encCustomerMeasurement);
return order;
	}

@Override
	public void wrapDetails(EncOrder model, HttpServletRequest request) {
		this.encCustomerMeasurement = model.getCustomerMeasurement();
		this.id = model.getId();
		this.encReseller = (EncReseller) model.getOrderResellerXref();
	}

@Override
	public void wrapSummary(EncOrder model, HttpServletRequest request) {
this.wrapDetails(model, request);		
	}


	

}
