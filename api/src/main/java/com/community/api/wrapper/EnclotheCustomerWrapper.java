/*
 * #%L
 * BroadleafCommerce Framework Web
 * %%
 * Copyright (C) 2009 - 2013 Broadleaf Commerce
 * %%
 * Licensed under the Broadleaf End User License Agreement (EULA), Version 1.1
 * (the "Commercial License" located at http://license.broadleafcommerce.org/commercial_license-1.1.txt).
 * 
 * Alternatively, the Commercial License may be replaced with a mutually agreed upon license (the "Custom License")
 * between you and Broadleaf Commerce. You may not use this file except in compliance with the applicable license.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of Broadleaf Commerce, LLC
 * The intellectual and technical concepts contained
 * herein are proprietary to Broadleaf Commerce, LLC
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Broadleaf Commerce, LLC.
 * #L%
 */

package com.community.api.wrapper;

import org.broadleafcommerce.common.rest.api.wrapper.APIUnwrapper;
import org.broadleafcommerce.common.rest.api.wrapper.APIWrapper;
import org.broadleafcommerce.common.rest.api.wrapper.BaseWrapper;
import org.broadleafcommerce.common.persistence.Status;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.domain.CustomerAttribute;
import org.broadleafcommerce.profile.core.domain.CustomerPhone;
import org.broadleafcommerce.profile.core.domain.Phone;
import org.broadleafcommerce.profile.core.service.CustomerService;
import org.springframework.context.ApplicationContext;

import com.broadleafcommerce.rest.api.wrapper.CustomerAttributeWrapper;
import com.broadleafcommerce.rest.api.wrapper.CustomerWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is a JAXB wrapper around FulfillmentGroupItem.
 *
 * User: Elbert Bautista
 * Date: 4/18/12
 */
@XmlRootElement(name = "customer")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class EnclotheCustomerWrapper extends CustomerWrapper implements APIWrapper<Customer>, APIUnwrapper<Customer> {

    @XmlElement
    protected Long id;

    @XmlElement
    protected String firstName;

    @XmlElement
    protected String lastName;

    @XmlElement
    protected String emailAddress;

    @XmlElement
    protected String phoneNumber;
    
    @XmlElement
    protected boolean phoneAuthenticated;
    
    @XmlElement
    protected boolean phoneVerified;
    
    @XmlElement
    protected boolean registered;

    @XmlElement(name = "customerAttribute")
    @XmlElementWrapper(name = "customerAttributes")
    protected List<CustomerAttributeWrapper> customerAttributes;
    
    @XmlElement
    protected Character archived;

    @Override
    public void wrapDetails(Customer model, HttpServletRequest request) {
        this.id = model.getId();
        this.firstName = model.getFirstName();
        this.lastName = model.getLastName();
        this.emailAddress = model.getEmailAddress();
        this.registered = model.isRegistered();
        List<CustomerPhone> customerPhone = null;
		customerPhone = model.getCustomerPhones();
		
        if(customerPhone != null && customerPhone.size() > 0) {
        	customerPhone.forEach((customerPh)->{
        		Phone phone = customerPh.getPhone();
        		if(phone.isDefault()) {
	        		this.phoneNumber =phone.getPhoneNumber();
	        		this.phoneAuthenticated = false;
	        		this.phoneVerified = true;
        		}
        	});
        }
        if (model.getCustomerAttributes() != null && !model.getCustomerAttributes().isEmpty()) {
            Map<String, CustomerAttribute> itemAttributes = model.getCustomerAttributes();
            this.customerAttributes = new ArrayList<CustomerAttributeWrapper>();
            Set<String> keys = itemAttributes.keySet();
            for (String key : keys) {
                CustomerAttributeWrapper customerAttributeWrapper =
                        (CustomerAttributeWrapper) context.getBean(CustomerAttributeWrapper.class.getName());
                customerAttributeWrapper.wrapDetails(itemAttributes.get(key), request);
                this.customerAttributes.add(customerAttributeWrapper);
            }
        }

        if (model instanceof Status) {
            this.archived = ((Status) model).getArchived();
        }
    }

    @Override
    public void wrapSummary(Customer model, HttpServletRequest request) {
        wrapDetails(model, request);
    }

    @Override
    public Customer unwrap(HttpServletRequest request, ApplicationContext context) {
        CustomerService customerService = (CustomerService) context.getBean("blCustomerService");
        Customer customer = customerService.createCustomerFromId(this.id);
        customer.setFirstName(this.firstName);
        customer.setLastName(this.lastName);
        customer.setEmailAddress(this.emailAddress);
        if (customerAttributes != null) {
            for (CustomerAttributeWrapper customerAttributeWrapper : customerAttributes) {
                CustomerAttribute attribute = customerAttributeWrapper.unwrap(request, context);
                attribute.setCustomer(customer);
                customer.getCustomerAttributes().put(attribute.getName(), attribute);
            }
        }
        return customer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isPhoneAuthenticated() {
		return phoneAuthenticated;
	}

	public void setPhoneAuthenticated(boolean phoneAuthenticated) {
		this.phoneAuthenticated = phoneAuthenticated;
	}

	public boolean isPhoneVerified() {
		return phoneVerified;
	}

	public void setPhoneVerified(boolean phoneVerified) {
		this.phoneVerified = phoneVerified;
	}
    public List<CustomerAttributeWrapper> getCustomerAttributes() {
        return customerAttributes;
    }

    public void setCustomerAttributes(List<CustomerAttributeWrapper> customerAttributes) {
        this.customerAttributes = customerAttributes;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }
}
