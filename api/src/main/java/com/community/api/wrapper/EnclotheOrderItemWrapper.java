package com.community.api.wrapper;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.common.persistence.Status;
import org.broadleafcommerce.common.rest.api.wrapper.APIUnwrapper;
import org.broadleafcommerce.common.rest.api.wrapper.APIWrapper;
import org.broadleafcommerce.common.rest.api.wrapper.BaseWrapper;
import org.broadleafcommerce.core.order.service.OrderItemService;
import org.broadleafcommerce.core.order.service.OrderService;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.domain.CustomerAttribute;
import org.broadleafcommerce.profile.core.service.CustomerService;
import org.springframework.context.ApplicationContext;

import com.broadleafcommerce.rest.api.wrapper.CustomerAttributeWrapper;
import com.community.api.dto.EnclotheOrderItemDTO;

@XmlRootElement(name = "EnclotheOrderItem")
@XmlAccessorType(value = XmlAccessType.FIELD)

public class EnclotheOrderItemWrapper   extends BaseWrapper implements APIWrapper<EnclotheOrderItemDTO>, APIUnwrapper<EnclotheOrderItemDTO> {

	 
	 @XmlElement
	    protected Long customerId;
	 
	 @XmlElement
	    protected Long productId;
	 @XmlElement
	    protected Long categoryId;

	    @XmlElement
	    protected Long skuId;

	 @XmlElement
	    protected int quantity;
	    
	    @XmlElement
	    protected Money totalPrice;
	    
	public EnclotheOrderItemWrapper() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	


	public int getQuantity() {
		return quantity;
	}





	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}





	public Long getProductId() {
		return productId;
	}


	public void setProductId(Long productId) {
		this.productId = productId;
	}


	public Long getCustomerId() {
		return customerId;
	}


	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}


	public Long getCategoryId() {
		return categoryId;
	}


	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}


	public Long getSkuId() {
		return skuId;
	}


	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}


	public Money getTotalPrice() {
		return totalPrice;
	}


	public void setTotalPrice(Money totalPrice) {
		this.totalPrice = totalPrice;
	}


	@Override
    public void wrapDetails(EnclotheOrderItemDTO model, HttpServletRequest request) {
        this.quantity = model.getQuantity();
        this.productId = model.getProductId();
        this.customerId = model.getCustomerId();
        this.categoryId = model.getCategoryId();
        this.skuId = model.getSkuId();
       

       
    }

    @Override
    public void wrapSummary(EnclotheOrderItemDTO model, HttpServletRequest request) {
        wrapDetails(model, request);
    }

    @Override
    public EnclotheOrderItemDTO unwrap(HttpServletRequest request, ApplicationContext context) {
        /*OrderService orderService = (OrderService) context.getBean("blOrderService");
        CustomerService customerService = (CustomerService) context.getBean("blCustomerService");
        
        Customer customer = customerService.createCustomerFromId(this.customerId);
        */
    	
    	EnclotheOrderItemDTO dto = new EnclotheOrderItemDTO();
    	dto.setProductId(this.productId);
    	dto.setCustomerId(this.customerId);
    	dto.setSkuId(this.skuId);
    	
        
        return dto;
    }

	
}
	
	
