package org.broadleafcommerce.jwt.model;

import java.io.Serializable;

import org.broadleafcommerce.core.geolocation.GeolocationDTO;

public class JwtRequest implements Serializable {

	private static final long serialVersionUID = 5926468583005150707L;
	
	private String username;
	private String password;
	private String locale_code;
	private String deviceid;
	private String phoneno;
	private String app_id;
	private String app_version;
	private String customer_type;
	private String authentication_source;
	private String authentication_source_id;
	private String authentication_source_token;
	private GeolocationDTO geolocation;
	private String firstName;
	private String LastName;
	private String deviceManufacturer;
	private String deviceModel;
	private String authToken;
	private boolean isMobileNoVerified;
	private String businessType;
	private String userType;
	private String adhaarCode;
	private String gstNumber;
	private String emailAddress;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String postalCode;
	
	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getAdhaarCode() {
		return adhaarCode;
	}

	public void setAdhaarCode(String adhaarCode) {
		this.adhaarCode = adhaarCode;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getLocale_code() {
		return locale_code;
	}

	public void setLocale_code(String locale_code) {
		this.locale_code = locale_code;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public String getApp_id() {
		return app_id;
	}

	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}

	public String getCustomer_type() {
		return customer_type;
	}

	public void setCustomer_type(String customer_type) {
		this.customer_type = customer_type;
	}

	public String getAuthentication_source() {
		return authentication_source;
	}

	public void setAuthentication_source(String authentication_source) {
		this.authentication_source = authentication_source;
	}

	public GeolocationDTO getGeolocation() {
		return geolocation;
	}

	public void setGeolocation(GeolocationDTO geolocation) {
		this.geolocation = geolocation;
	}

	
	
	
	//need default constructor for JSON Parsing
	public JwtRequest()
	{
		
	}

	public JwtRequest(String username, String password) {
		this.setUsername(username);
		this.setPassword(password);
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuthentication_source_id() {
		return authentication_source_id;
	}

	public void setAuthentication_source_id(String authentication_source_id) {
		this.authentication_source_id = authentication_source_id;
	}

	public String getAuthentication_source_token() {
		return authentication_source_token;
	}

	public void setAuthentication_source_token(String authentication_source_token) {
		this.authentication_source_token = authentication_source_token;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getDeviceManufacturer() {
		return deviceManufacturer;
	}

	public void setDeviceManufacturer(String deviceManufacturer) {
		this.deviceManufacturer = deviceManufacturer;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public boolean isMobileNoVerified() {
		return isMobileNoVerified;
	}

	public void setMobileNoVerified(boolean isMobileNoVerified) {
		this.isMobileNoVerified = isMobileNoVerified;
	}

	public String getApp_version() {
		return app_version;
	}

	public void setApp_version(String app_version) {
		this.app_version = app_version;
	}

	}