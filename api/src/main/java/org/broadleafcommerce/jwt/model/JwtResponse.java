package org.broadleafcommerce.jwt.model;

import java.io.Serializable;

import com.broadleafcommerce.rest.api.wrapper.CustomerWrapper;
import com.community.api.wrapper.EnclotheCustomerWrapper;

public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;
	private final String jwttoken;
	private CustomerWrapper customerWrapper;
	private EnclotheCustomerWrapper enclotheCustomerWrapper;
	private Integer cartCount;
	private boolean forceUpgrade = false;
	private boolean optionalUpgrade =false;
	private String userType;
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserPermission() {
		return userPermission;
	}

	public void setUserPermission(String userPermission) {
		this.userPermission = userPermission;
	}

	private String userPermission;
	
	public CustomerWrapper getCustomerWrapper() {
		return customerWrapper;
	}

	public void setCustomerWrapper(CustomerWrapper customerWrapper) {
		this.customerWrapper = customerWrapper;
	}


	public JwtResponse(String jwttoken) {
		this.jwttoken = jwttoken;
	}

	public String getToken() {
		return this.jwttoken;
	}

	public Integer getCartCount() {
		return cartCount;
	}

	public void setCartCount(Integer cartCount) {
		this.cartCount = cartCount;
	}

	public boolean isForceUpgrade() {
		return forceUpgrade;
	}

	public void setForceUpgrade(boolean forceUpgrade) {
		this.forceUpgrade = forceUpgrade;
	}

	public boolean isOptionalUpgrade() {
		return optionalUpgrade;
	}

	public void setOptionalUpgrade(boolean optionalUpgrade) {
		this.optionalUpgrade = optionalUpgrade;
	}

	public EnclotheCustomerWrapper getEnclotheCustomerWrapper() {
		return enclotheCustomerWrapper;
	}

	public void setEnclotheCustomerWrapper(EnclotheCustomerWrapper enclotheCustomerWrapper) {
		this.enclotheCustomerWrapper = enclotheCustomerWrapper;
	}
	
}