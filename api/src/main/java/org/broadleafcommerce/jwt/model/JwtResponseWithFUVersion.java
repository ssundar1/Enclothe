package org.broadleafcommerce.jwt.model;

import java.io.Serializable;
import java.util.List;

import com.broadleafcommerce.rest.api.wrapper.CategoryWrapper;
import com.broadleafcommerce.rest.api.wrapper.CustomerWrapper;
import com.community.api.wrapper.EnclotheCustomerWrapper;

public class JwtResponseWithFUVersion extends JwtResponse implements Serializable {

	public JwtResponseWithFUVersion(String jwttoken) {
		
		super(jwttoken);
		// TODO Auto-generated constructor stub
	}
	private static final long serialVersionUID = -8091879091924046844L;
	private String forceUpgradeVersion =  "1.0.12.1.15";
	private CustomerWrapper customerWrapper;
	private EnclotheCustomerWrapper enclotheCustomerWrapper;
	private List<CategoryWrapper> categoryList;
	public String getForceUpgradeVersion() {
		return this.forceUpgradeVersion;
	}
	public void setForceUpgradeVersion(String d) {
		forceUpgradeVersion = d;
	}
	public List<CategoryWrapper> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(List<CategoryWrapper> categoryList) {
		this.categoryList = categoryList;
	}
}