package com.enclothe.core.customer.domain;

import java.util.List;
import java.util.Map;

import org.broadleafcommerce.profile.core.domain.Customer;

import com.enclothe.core.measurement.domain.EncCustomerMeasurementImpl;
import com.enclothe.core.measurement.domain.Measurement;
//import com.enclothe.core.product.domain.EncTailor;

public interface EncCustomer extends Customer{
	
	/*
	 * public List<Measurement> getMeasurements();
	 * 
	 * public void setMeasurements(List<Measurement> measurements) ;
	 */
	
	  public List<EncCustomerMeasurementImpl> getMeasurements();
	  
	  public void setMeasurements(List<EncCustomerMeasurementImpl> measurements);
	 
	

	//public List<Measurement> getCustomerMeasurements();
	
	//public void setCustomerMeasurements ( List<Measurement> measurements measurements);
	
//	public void addMeasurement (Measurement measurement);
	
	/*
	 * public Measurement getPreferredMeasurement();
	 * 
	 * public void setPreferredMeasurement(Measurement preferredMeasurement);
	 */

	//void setCustomerMeasurements(List<Measurement> measurements);

	//public EncTailor getPreferredTailor();

//s	public void setPreferredTailor(EncTailor preferredTailor);
	
}
