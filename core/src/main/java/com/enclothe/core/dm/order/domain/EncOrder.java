package com.enclothe.core.dm.order.domain;

import java.util.Date;
import java.util.List;

import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.profile.core.domain.Address;

import com.enclothe.core.measurement.domain.EncCustomerMeasurement;
import com.enclothe.core.measurement.domain.Measurement;
import com.enclothe.core.reseller.domain.EncReseller;
import com.enclothe.core.xref.domain.EncOrderResellerXref;

public interface EncOrder extends Order {

	// public Address getAddress();

	// public void setAddress(Address address);

	public Date getMaterial_pickup_date_time();

	public void setMaterial_pickup_date_time(Date material_pickup_date_time);

	public String getAdditionalInfo();

	public void setAdditionalInfo(String additionalInfo);

	 EncCustomerMeasurement getCustomerMeasurement();

	public String getOrderPaymentReference();

	public void setOrderPaymentReference(String orderPaymentReference);

	 public void setCustomerMeasurement(EncCustomerMeasurement customerMeasurement);
	/*
	 * public EncReseller getOrderReseller();
	 * 
	 * public void setOrderReseller(EncReseller orderReseller);
	 */
	// public Measurement getCustomerMeasurement();

	// public void setCustomerMeasurement(Measurement customerMeasurement);
	public List<EncOrderResellerXref> getOrderResellerXref();

	public void setOrderResellerXref(List<EncOrderResellerXref> orderResellerXref);
	/*
	 * Tried and not working...just keeping it for reference public
	 * EncOrderResellerXref getOrderResellerXref();
	 * 
	 * public void setOrderResellerXref(EncOrderResellerXref orderResellerXref);
	 */
}
