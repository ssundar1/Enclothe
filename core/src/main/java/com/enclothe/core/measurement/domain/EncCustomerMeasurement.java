package com.enclothe.core.measurement.domain;

import java.io.Serializable;
import java.util.List;

import org.broadleafcommerce.common.persistence.ArchiveStatus;
import org.broadleafcommerce.profile.core.domain.Customer;

import com.enclothe.core.customer.domain.EncCustomer;

public interface EncCustomerMeasurement extends Serializable {

	void setMeasurements(Measurement measurements);

	public Measurement getMeasurements();
	void setCustomer(EncCustomer customer);
	public EncCustomer getCustomer();
	public Long getCustomer_measurement_id();
	

	public void setCustomer_measurement_id(Long customer_measurement_id) ;

	public String getMeasurementName() ;

	public void setMeasurementName(String measurementName);

	public ArchiveStatus getArchiveStatus();
	public void setArchiveStatus(ArchiveStatus archiveStatus);

}
