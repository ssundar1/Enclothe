
package com.enclothe.core.measurement.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.InheritanceType;

import org.broadleafcommerce.cms.structure.domain.StructuredContentImpl.Presentation;
import org.broadleafcommerce.common.persistence.ArchiveStatus;
import org.broadleafcommerce.common.presentation.AdminGroupPresentation;
import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.common.presentation.AdminPresentationAdornedTargetCollection;
import org.broadleafcommerce.common.presentation.AdminPresentationClass;
import org.broadleafcommerce.common.presentation.AdminPresentationCollection;
import org.broadleafcommerce.common.presentation.AdminPresentationMap;
import org.broadleafcommerce.common.presentation.AdminTabPresentation;
import org.broadleafcommerce.common.presentation.PopulateToOneFieldsEnum;
import org.broadleafcommerce.common.presentation.client.AddMethodType;
import org.broadleafcommerce.common.presentation.client.VisibilityEnum;
import org.broadleafcommerce.core.catalog.domain.CategoryAdminPresentation.TabName;
import org.broadleafcommerce.profile.core.domain.Address;
import org.broadleafcommerce.profile.core.domain.AddressImpl;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.domain.CustomerAdminPresentation;
import org.broadleafcommerce.profile.core.domain.CustomerImpl;
import org.broadleafcommerce.profile.core.domain.CustomerAdminPresentation.FieldOrder;
import org.broadleafcommerce.profile.core.domain.CustomerAdminPresentation.GroupName;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Parameter;

import com.enclothe.core.customer.domain.EncCustomer;
import com.enclothe.core.customer.domain.EncCustomerImpl;
import com.enclothe.core.dm.order.domain.EncOrder;
//import com.enclothe.core.dm.order.domain.EncOrder; //import
import com.enclothe.core.dm.order.domain.EncOrderImpl;

@Entity

@Inheritance(strategy = InheritanceType.JOINED)

@Table(name = "ENC_CUSTOMER_MEASUREMENT_XREF")

@AdminPresentationClass(populateToOneFields = PopulateToOneFieldsEnum.TRUE, friendlyName = "Customer Measuremt", tabs = {

		@AdminTabPresentation(name = "General", order = 1000, groups = {

				@AdminGroupPresentation(name = "EncCustomerMeasurementImpl_General_Tab", order = 1000, untitled = true) }),

		@AdminTabPresentation(name = "Advanced", order = 2000, groups = {

				@AdminGroupPresentation(name = "EncCustomerMeasurementImpll_Advanced_Tab", order = 1000, untitled = true) }) })
public class EncCustomerMeasurementImpl implements EncCustomerMeasurement {

	private static final long serialVersionUID = 1L;

	@Id

	@GeneratedValue(generator = "CustomerMeasurementId")

	@GenericGenerator(name = "CustomerMeasurementId", strategy = "org.broadleafcommerce.common.persistence.IdOverrideTableGenerator", parameters = {

			@Parameter(name = "segment_value", value = "EncCustomerMeasurementImpl"),

			@Parameter(name = "entity_name", value = "com.enclothe.core.measurement.domain.EncCustomerMeasurementImpl") })

	@Column(name = "CUSTOMER_MEASUREMENT_ID")
	protected Long customer_measurement_id;

	public Long getCustomer_measurement_id() {
		return customer_measurement_id;
	}

	public void setCustomer_measurement_id(Long customer_measurement_id) {
		this.customer_measurement_id = customer_measurement_id;
	}

	@Column(name = "MEASUREMENT_NAME")

	@AdminPresentation(friendlyName = "CustomerMeasurement_Name", order = 1, group = "CustomerMeasurement", groupOrder = 1, prominent = true, gridOrder = 1)
	protected String measurementName;

	@ManyToOne(cascade = { CascadeType.PERSIST,
			CascadeType.MERGE }, targetEntity = EncCustomerImpl.class, optional = false)

	@JoinColumn(name = "CUSTOMER_ID")

	@AdminPresentation(excluded = true, visibility = VisibilityEnum.HIDDEN_ALL)
	protected EncCustomer customer;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REFRESH }, targetEntity = MeasurementImpl.class, optional = false)

	@JoinColumn(name = "MEASUREMENT_ID")
	// @Index(name="CUSTOMERADDRESS_ADDRESS_INDEX", columnNames={"ADDRESS_ID"})

	@AdminPresentation(visibility = VisibilityEnum.VISIBLE_ALL)

	protected Measurement measurements;

	/*
	 * @OneToMany(mappedBy = "customer", targetEntity = EncOrderImpl.class, cascade
	 * = { CascadeType.ALL }, orphanRemoval = true)
	 * 
	 * @Cascade(value = { org.hibernate.annotations.CascadeType.ALL,
	 * org.hibernate.annotations.CascadeType.DELETE_ORPHAN })
	 * 
	 * @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region =
	 * "blStandardElements")
	 * 
	 * protected List<EncOrder> orders = new ArrayList<EncOrder>();
	 */

	/*
	 * @OneToMany(targetEntity = OrderItemSPMeasurementXrefImpl.class, mappedBy =
	 * "id") protected List<OrderItemSPMeasurementXref> orderItemSPMXref;
	 */
	@Embedded
	protected ArchiveStatus archiveStatus = new ArchiveStatus();

	@Override
	public Measurement getMeasurements() {
		return this.measurements;
	}

	@Override
	public void setMeasurements(Measurement measurements) {
		this.measurements = measurements;
	}

	@Override
	public void setCustomer(EncCustomer customer) {
		this.customer = customer;
	}

	

	public String getMeasurementName() {
		return measurementName;
	}

	public void setMeasurementName(String measurementName) {
		this.measurementName = measurementName;
	}

	public ArchiveStatus getArchiveStatus() {
		return archiveStatus;
	}

	public void setArchiveStatus(ArchiveStatus archiveStatus) {
		this.archiveStatus = archiveStatus;
	}

	@Override
	public EncCustomer getCustomer() {
		return this.customer;
	}

}
