package com.enclothe.core.measurement.domain;

import java.io.Serializable;
import java.util.Date;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.broadleafcommerce.common.audit.Auditable;
import org.broadleafcommerce.profile.core.domain.Customer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Implementations of this interface are used to hold data about a Measurement.
 *  A measurement is used to hold details about all kinds of measurement
 * <br>
 * <br>
 * You should implement this class if you want to make significant changes to how the
 * Measurement is persisted.  If you just want to add additional fields then you should extend {@link MeasurementImpl}.
 *
 * @see {@link MeasurementImpl}
 * @author Prashanth Doraiswamy
 * 
 */
@JsonDeserialize(as=MeasurementImpl.class)

public interface Measurement extends Serializable {
	
    /**
     * Gets the primary key.
     * 
     * @return the primary key
     */
	@Nonnull
    public Long getId();

    /**
     * Sets the primary key.
     * 
     * @param id the new primary key
     */
    public void setId(@Nonnull Long id);

    /**
     * Gets the name.
     * 
     * @return the name
     */
    @Nonnull
    public String getName();

    /**
     * Sets the name.
     * 
     * @param name the new name
     */
    public void setName(@Nonnull String name);

    /**
     * Gets the description
     * 
     * @return the description
     */
    @Nullable
    public String getDescription();
    
    /**
     * Sets the description.
     * 
     * @param description the new description
     */    
    
    public void setDescription(@Nonnull String description);
    
	
	/*
	 * @Nullable public Customer getCustomer();
	 * 
	 * public void setCustomer(@Nullable Customer customer);
	 */
	 
    
    @Nullable
    public Double getHeight();
    
    public void setHeight(@Nullable Double height);
    
    @Nullable
    public void setChest(Double chest) ;

	public Double getChest();

    
    @Nullable
    public Double  getBust();
    
    public void setBust(@Nullable Double bust); 
    
    @Nullable
    public Double getDartline();
    
    public void setDartline(@Nullable Double dartline);
    
    @Nullable
    public Double getWaist();
    
    public void setWaist(@Nullable Double waist);
    
    @Nullable
    public Double getHip();
    
    public void setHip(@Nullable Double hip);
    
    @Nullable
    public Double getNWaist();
    
    public void setNWaist(@Nullable Double nWaist);
    
    @Nullable
    public Double getShoulder();
    
    public void setShoulder(@Nullable Double shoulder);
    
    @Nullable
    public Double getSleeveLength();
    
    public void setSleeveLength(@Nullable Double sleeveLength);
    
    @Nullable
    public Double getArmHole();
    
    public void setArmHole(@Nullable Double armHole);
    
    @Nullable
    public Double getTSleeve();
    
    public void setTSleeve(@Nullable Double tSleeve);
    
    @Nullable
    public Double getBSleeve();
    
    public void setBSleeve(@Nullable Double bSleeve);
    
    @Nullable
    public Double getFrontNeck();
    
    public void setFrontNeck(@Nullable Double frontNeck);
    
    @Nullable
    public Double getBackNeck();
    
    public void setBackNeck(@Nullable Double backNeck);
    
    @Nullable
    public Double getPantHeight();
    
    public void setPantHeight(@Nullable Double pantHeight);
    
    @Nullable
    public Double getSeat();
    
    public void setSeat(@Nullable Double seat);
    
    @Nullable
    public Auditable getAuditable();

    public void setAuditable(@Nullable Auditable auditable);
    
    @Nullable
    public Date getEndDate();
    
    public void setEndDate(@Nullable Date endDate);
    
}
