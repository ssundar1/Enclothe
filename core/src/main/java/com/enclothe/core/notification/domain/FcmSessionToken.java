package com.enclothe.core.notification.domain;

public interface FcmSessionToken {

	public Long getId();

	public void setId(Long id);

	public String getLoginName();

	public void setLoginName(String loginName);

	public String getLoginType();

	public void setLoginType(String loginType);

	public String getFcmToken();

	public void setFcmToken(String fcmToken);
}
