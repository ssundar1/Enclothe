package com.enclothe.core.notification.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.broadleafcommerce.common.audit.AuditableListener;
import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.common.presentation.AdminPresentationClass;
import org.broadleafcommerce.common.presentation.PopulateToOneFieldsEnum;
import org.broadleafcommerce.common.presentation.client.VisibilityEnum;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.SQLDelete;

@Entity
@EntityListeners(value = { AuditableListener.class })
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "ENC_FCMSESSIONTOKEN")
@Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region="blStandardElements")
@AdminPresentationClass(populateToOneFields = PopulateToOneFieldsEnum.TRUE, friendlyName = "FcmSessionTokenImpl_baseFcmSessionToken")
@SQLDelete(sql="UPDATE ENC_FCMSESSIONTOKEN SET END_DATE = now() WHERE FCM_ID = ?")

public class FcmSessionTokenImpl implements FcmSessionToken {
	
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(generator= "fcmId")
	@GenericGenerator(
	        name="fcmId",
	        strategy="org.broadleafcommerce.common.persistence.IdOverrideTableGenerator",
	        parameters = {
	            @Parameter(name="segment_value", value="FcmSessionTokenImpl"),
	            @Parameter(name="entity_name", value="com.enclothe.core.notification.domain.FcmSessionTokenImpl")
	        }
	    )
	@AdminPresentation(friendlyName = "FcmSessionTokenImpl_Fcm_Id", group = "FcmSessionTokenImpl_Primary_Key",
    visibility = VisibilityEnum.HIDDEN_ALL)
    @Column(name = "FCM_ID", nullable=false)
	protected Long id;
	
	@Column(name = "LOGIN_NAME", nullable=false)
    @AdminPresentation(friendlyName = "FcmSessionTokenImpl_LoginName", group = "FcmSessionTokenImpl_FcmSessionToken",
    visibility = VisibilityEnum.HIDDEN_ALL)    
 	protected String loginName;
	
	@Column(name = "LOGIN_TYPE", nullable=false)
    @AdminPresentation(friendlyName = "FcmSessionTokenImpl_LoginType", group = "FcmSessionTokenImpl_FcmSessionToken",
    visibility = VisibilityEnum.HIDDEN_ALL)    
    protected String loginType;
	
	@Column(name = "FCM_TOKEN", nullable=false)
    @AdminPresentation(friendlyName = "FcmSessionTokenImpl_FcmToken", group = "FcmSessionTokenImpl_FcmSessionToken",
    visibility = VisibilityEnum.HIDDEN_ALL)    
    protected String fcmToken;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}
	
	

}
