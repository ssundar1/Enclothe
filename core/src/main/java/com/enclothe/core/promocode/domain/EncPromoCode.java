package com.enclothe.core.promocode.domain;

import java.sql.Date;

import org.broadleafcommerce.common.audit.Auditable;

import com.enclothe.core.reseller.domain.EncReseller;

public interface EncPromoCode {

	Long getId();

	void setId(Long id);

	Date getUpdatedDate();

	void setUpdatedDate(Date updatedDate);

	String getMobileNumber();

	void setMobileNumber(String mobileNumber);

	Date getExpiryDate();

	void setExpiryDate(Date expiryDate);

	void setAuditable(Auditable auditable);

	Auditable getAuditable();

	String getPromoCode();

	void setPromoCode(String promoCode);

	String getStatus();

	void setStatus(String status);

	Long getResellerId();

	void setResellerId(Long resellerId);
	
	String getDiscountNumber();
	
	void setDiscountNumber(String discountNumber);
	
	String getDiscountType();
	
	void setDiscountType(String discountType);

}
