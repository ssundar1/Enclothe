package com.enclothe.core.promocode.domain;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.broadleafcommerce.common.audit.Auditable;
import org.broadleafcommerce.common.audit.AuditableListener;
import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.common.presentation.AdminPresentationClass;
import org.broadleafcommerce.common.presentation.PopulateToOneFieldsEnum;
import org.broadleafcommerce.common.presentation.client.VisibilityEnum;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.SQLDelete;

@Entity
@EntityListeners(value = { AuditableListener.class })
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "ENC_PROMOCODE")
@Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region="blStandardElements")
@AdminPresentationClass(populateToOneFields = PopulateToOneFieldsEnum.TRUE, friendlyName = "EncPromoCodeImpl_baseEncPromoCode")
@SQLDelete(sql="UPDATE ENC_PROMOCODE SET END_DATE = now() WHERE PROMOCODE_ID = ?")

public class EncPromoCodeImpl implements EncPromoCode{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
    @GeneratedValue(generator= "encPromoCodeId")
	@GenericGenerator(
	        name="encPromoCodeId",
	        strategy="org.broadleafcommerce.common.persistence.IdOverrideTableGenerator",
	        parameters = {
	            @Parameter(name="segment_value", value="EncPromoCodeImpl"),
	            @Parameter(name="entity_name", value="com.enclothe.core.reseller.domain.EncPromoCodeImpl")
	        }
	    )
	@AdminPresentation(friendlyName = "EncPromoCodeImpl_promocode_Id", group = "EncPromoCodeImpl_Primary_Key",
    visibility = VisibilityEnum.HIDDEN_ALL)
    @Column(name = "PROMOCODE_ID", nullable=false)
	protected Long id;
	
	@Column(name = "RESELLER_ID")
	@AdminPresentation(friendlyName = "EncPromoCodeImpl_Reseller_Id",
	            group = "EncPromoCodeImpl_EncPromoCode",
	visibility = VisibilityEnum.HIDDEN_ALL)
	protected Long resellerId;
	
	
	@Column(name = "PROMO_CODE")
	@AdminPresentation(friendlyName = "EncPromoCodeImpl_Promo_Code",
	            group = "EncPromoCodeImpl_EncPromoCode",
	visibility = VisibilityEnum.HIDDEN_ALL)
	protected String promoCode;
	     
	@Column(name = "STATUS")
	@AdminPresentation(friendlyName = "EncPromoCodeImpl_Status",
	            group = "EncPromoCodeImpl_EncPromoCode",
	visibility = VisibilityEnum.HIDDEN_ALL)
	protected String status;
	
	@Column(name = "MOBILE_NUMBER")
	@AdminPresentation(friendlyName = "EncPromoCodeImpl_Mobile_Number",
	            group = "EncPromoCodeImpl_EncPromoCode",
	visibility = VisibilityEnum.HIDDEN_ALL)
	protected String mobileNumber;
	
	@Column(name = "DISCOUNT_NUMBER")
	@AdminPresentation(friendlyName = "EncPromoCodeImpl_Discount_Number",
	            group = "EncPromoCodeImpl_EncPromoCode",
	visibility = VisibilityEnum.HIDDEN_ALL)
	protected String discountNumber;
	
	@Column(name = "DISCOUNT_TYPE")
	@AdminPresentation(friendlyName = "EncPromoCodeImpl_Discount_Type",
	            group = "EncPromoCodeImpl_EncPromoCode",
	visibility = VisibilityEnum.HIDDEN_ALL)
	protected String discountType;
	
	@Column(name = "UPDATED_DATE")
    @AdminPresentation(friendlyName = "EncPromoCodeImpl_Updated_Date", 
    order=10, group = "EncPromoCodeImpl_EncPromoCode")
    protected Date updatedDate;
    
    @Column(name = "EXPIRY_DATE")
    @AdminPresentation(friendlyName = "EncPromoCodeImpl_Expiry_Date", 
    order=10, group = "EncPromoCodeImpl_EncPromoCode")
    protected Date expiryDate;
    @Embedded
    protected Auditable auditable = new Auditable();
    
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		this.id = id;
	}
	@Override
	public Date getUpdatedDate() {
		// TODO Auto-generated method stub
		return updatedDate;
	}
	@Override
	public void setUpdatedDate(Date updatedDate) {
		// TODO Auto-generated method stub
		this.updatedDate = updatedDate;
	}
	
	
	@Override
	public String getPromoCode() {
		return promoCode;
	}
	@Override
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	@Override
	public String getMobileNumber() {
		return mobileNumber;
	}
	@Override
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	@Override
	public Date getExpiryDate() {
		return expiryDate;
	}
	@Override
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	@Override
	public String getStatus() {
		return status;
	}
	@Override
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public Long getResellerId() {
		return resellerId;
	}
	@Override
	public void setResellerId(Long resellerId) {
		this.resellerId = resellerId;
	}
	@Override
	public String getDiscountNumber() {
		return discountNumber;
	}
	@Override
	public void setDiscountNumber(String discountNumber) {
		this.discountNumber = discountNumber;
	}
	@Override
	public void setAuditable(Auditable auditable) {
		this.auditable = auditable;

	}
	@Override
	public Auditable getAuditable() {
		return auditable;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
}
