package com.enclothe.core.reseller.domain;

import com.enclothe.core.customer.domain.EncCustomer;

public interface EncCustomerResellerMapping {

	EncReseller getResellers();

	void setCustomer(EncCustomer customer);

	EncCustomer getCustomer();

	void setResellers(EncReseller resellers);
}
