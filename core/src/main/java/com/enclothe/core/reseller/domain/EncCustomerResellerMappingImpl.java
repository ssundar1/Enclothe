package com.enclothe.core.reseller.domain;

import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.broadleafcommerce.common.persistence.ArchiveStatus;
import org.broadleafcommerce.common.presentation.AdminGroupPresentation;
import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.common.presentation.AdminPresentationClass;
import org.broadleafcommerce.common.presentation.AdminTabPresentation;
import org.broadleafcommerce.common.presentation.PopulateToOneFieldsEnum;
import org.broadleafcommerce.common.presentation.client.VisibilityEnum;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.enclothe.core.customer.domain.EncCustomer;
import com.enclothe.core.customer.domain.EncCustomerImpl;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "ENC_CUSTOMER_RESELLER_XREF")
@AdminPresentationClass(populateToOneFields = PopulateToOneFieldsEnum.TRUE, friendlyName = "Customer Reseller", 
tabs = {
		@AdminTabPresentation(name = "General", order = 1000, groups = {
				@AdminGroupPresentation(name = "EncCustomerResellerMappingImpl_General_Tab", order = 1000, untitled = true) }),
		@AdminTabPresentation(name = "Advanced", order = 2000, groups = {
				@AdminGroupPresentation(name = "EncCustomerResellerMappingImpl_Advanced_Tab", order = 1000, untitled = true) })
		})

public class EncCustomerResellerMappingImpl implements EncCustomerResellerMapping {

	private static final long serialVersionUID = 1L;
	@Id
	  @GeneratedValue(generator = "CustomerResellerId")
	  @GenericGenerator( name="CustomerResellerId",
	  strategy="org.broadleafcommerce.common.persistence.IdOverrideTableGenerator",
	  parameters = {
	  
	  @Parameter(name="segment_value", value="EncCustomerResellerMappingImpl"),
	  
	  @Parameter(name="entity_name",
	  value="com.enclothe.core.reseller.domain.EncCustomerResellerMappingImpl") } )
	 
	 @Column(name = "CUSTOMER_RESELLER_ID")
	    protected Long cust_id;

	    @Column(name = "RESELLER_NAME")
	    @AdminPresentation(friendlyName = "EncCustomerResellerMappingImpl_Reseller_Name", order=1,
	            group = "EncCustomerResellerMappingImpl_Identification", groupOrder = 1, prominent = true, gridOrder = 1)
	    protected String resellerName;

	   @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, targetEntity = EncCustomerImpl.class, optional=false)
	    @JoinColumn(name = "CUSTOMER_ID")
	   @AdminPresentation(excluded = true, visibility = VisibilityEnum.HIDDEN_ALL)
	    protected EncCustomer customer;

	    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, targetEntity = EncResellerImpl.class, optional=false)
	    @JoinColumn(name = "RESELLER_ID")
	//    @Index(name="CUSTOMERADDRESS_ADDRESS_INDEX", columnNames={"ADDRESS_ID"})
	    @AdminPresentation( visibility = VisibilityEnum.VISIBLE_ALL)

	    protected EncReseller resellers;
	    
		@Embedded
	    protected ArchiveStatus archiveStatus = new ArchiveStatus();
   
	    @Override
	   public EncReseller getResellers() {
	       return this.resellers;
	   }
	
	   @Override
	   public void setResellers(EncReseller resellers) {
		   this.resellers = resellers;
	   }
	
	
		@Override
		public void setCustomer(EncCustomer customer) {
		 this.customer = customer;
		}
		
		public Long getId() {
			return cust_id;
		}
		
		public void setId(Long id) {
			this.cust_id = id;
		}
		
		public String getResellerName() {
			return resellerName;
		}
		
		public void setMeasurementName(String resellerName) {
			this.resellerName = resellerName;
		}
		
		public ArchiveStatus getArchiveStatus() {
			return archiveStatus;
		}
		
		public void setArchiveStatus(ArchiveStatus archiveStatus) {
			this.archiveStatus = archiveStatus;
		}
		
		@Override
		public EncCustomer getCustomer() {
			return this.customer;
		}
}
