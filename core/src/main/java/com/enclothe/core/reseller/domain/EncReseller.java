package com.enclothe.core.reseller.domain;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import org.broadleafcommerce.common.audit.Auditable;
import org.broadleafcommerce.profile.core.domain.Customer;

import com.enclothe.core.xref.domain.EncOrderResellerXref;

public interface EncReseller extends Serializable{

    public String getBusinessType();

    public void setBusinessType(String businessType);
    
    public String getUserType();
    
    public void setUserType(String userType);
    
    public String getAdhaarCode();
    
    public void setAdhaarCode(String adhaarCode);
    
    public String getGstNumber();
    
    public void setGstNumber(String gstNumber);
    
    public Date getUpdatedDate();
    
    public void setUpdatedDate(Date updatedDate);

	Long getId();

	void setId(Long id);

	Customer getCustomer();

	void setCustomer(Customer customer);

	void setAuditable(Auditable auditable);

	void setUserPermission(String userPermission);

	String getUserPermission();
	
	public List<EncOrderResellerXref> getOrderResellerXref();
	public void setOrderResellerXref(List<EncOrderResellerXref> orderResellerXref) ;

}
