package com.enclothe.core.reseller.domain;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.broadleafcommerce.common.audit.Auditable;
import org.broadleafcommerce.common.audit.AuditableListener;
import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.common.presentation.AdminPresentationClass;
import org.broadleafcommerce.common.presentation.AdminPresentationCollection;
import org.broadleafcommerce.common.presentation.PopulateToOneFieldsEnum;
import org.broadleafcommerce.common.presentation.client.AddMethodType;
import org.broadleafcommerce.common.presentation.client.VisibilityEnum;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.SQLDelete;

import com.enclothe.core.customer.domain.EncCustomerImpl;
import com.enclothe.core.xref.domain.EncOrderResellerXref;
import com.enclothe.core.xref.domain.EncOrderResellerXrefImpl;


@Entity
@EntityListeners(value = { AuditableListener.class })
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "ENC_RESELLER")
@Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region="blStandardElements")
@AdminPresentationClass(populateToOneFields = PopulateToOneFieldsEnum.TRUE, friendlyName = "EncResellerImpl_baseEncReseller")
@SQLDelete(sql="UPDATE ENC_RESELLER SET END_DATE = now() WHERE RESELLER_ID = ?")

public class EncResellerImpl implements EncReseller{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
    @GeneratedValue(generator= "encResellerId")
	@GenericGenerator(
	        name="encResellerId",
	        strategy="org.broadleafcommerce.common.persistence.IdOverrideTableGenerator",
	        parameters = {
	            @Parameter(name="segment_value", value="EncResellerImpl"),
	            @Parameter(name="entity_name", value="com.enclothe.core.reseller.domain.EncResellerImpl")
	        }
	    )
	@AdminPresentation(friendlyName = "EncResellerImpl_reseller_Id", group = "EncResellerImpl_Primary_Key",
    visibility = VisibilityEnum.HIDDEN_ALL)
    @Column(name = "RESELLER_ID", nullable=false)
	protected Long id;
	
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, targetEntity = EncCustomerImpl.class, optional=true)
    @JoinColumn(name = "CUSTOMER_ID")
    @Index(name="RESELLER_CUSTOMER_INDEX", columnNames={"CUSTOMER_ID"})
    @AdminPresentation(friendlyName = "EncResellerImpl_Customer", order = 10, group = "EncResellerImpl_Reseller",
    	    visibility = VisibilityEnum.HIDDEN_ALL)
    protected Customer customer;
	
	@Column(name = "BUSINESS_TYPE")
	@AdminPresentation(friendlyName = "EncResellerImpl_Business_Type",
	            group = "EncResellerImpl_EncReseller",
	visibility = VisibilityEnum.HIDDEN_ALL)
	protected String businessType;
	     
	@Column(name = "USER_TYPE")
	@AdminPresentation(friendlyName = "EncResellerImpl_User_Type",
	            group = "EncResellerImpl_EncReseller",
	visibility = VisibilityEnum.HIDDEN_ALL)
	protected String userType;
	
    @Column(name = "ADHAAR_CODE")
    @AdminPresentation(friendlyName = "EncResellerImpl_Adhaar_Code",
    order=10, group = "EncResellerImpl_EncReseller")
    protected String adhaarCode;
    
    @Column(name = "GST_NUMBER")
    @AdminPresentation(friendlyName = "EncResellerImpl_Gst_Number", 
    order=10, group = "EncResellerImpl_EncReseller")
    protected String gstNumber;
    
    @Column(name = "USER_PERMISSION")
    @AdminPresentation(friendlyName = "EncResellerImpl_User_Permission", 
    order=10, group = "EncResellerImpl_EncReseller")
    protected String userPermission;
    
    @Column(name = "UPDATED_DATE")
    @AdminPresentation(friendlyName = "EncResellerImpl_Updated_Date", 
    order=10, group = "EncResellerImpl_EncReseller")
    protected Date updatedDate;
    
    @OneToMany(targetEntity = EncOrderResellerXrefImpl.class, mappedBy = "reseller")
	@AdminPresentationCollection(friendlyName = "ORDERS", group = "ORDERS", order = 14000, addType = AddMethodType.PERSIST)
	public List<EncOrderResellerXref> orderResellerXref = new ArrayList<>(); 
    
    public List<EncOrderResellerXref> getOrderResellerXref() {
		return orderResellerXref;
	}

	public void setOrderResellerXref(List<EncOrderResellerXref> orderResellerXref) {
		this.orderResellerXref = orderResellerXref;
	}  
    
    @Embedded
    protected Auditable auditable = new Auditable();
    
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		this.id = id;
	}
	
	@Override
	public Date getUpdatedDate() {
		// TODO Auto-generated method stub
		return updatedDate;
	}
	
	
	@Override
	public String getUserType() {
		// TODO Auto-generated method stub
		return userType;
	}

	@Override
	public void setUserPermission(String userPermission) {
		// TODO Auto-generated method stub
		this.userPermission = userPermission;
	}
	
	@Override
	public String getUserPermission() {
		// TODO Auto-generated method stub
		return userPermission;
	}

	@Override
	public void setUserType(String userType) {
		// TODO Auto-generated method stub
		this.userType = userType;
	}
	
	@Override
	public String getBusinessType() {
		// TODO Auto-generated method stub
		return businessType;
	}

	@Override
	public void setBusinessType(String businessType) {
		// TODO Auto-generated method stub
		this.businessType = businessType;
	}

	@Override
	public String getAdhaarCode() {
		// TODO Auto-generated method stub
		return adhaarCode;
	}

	@Override
	public void setAdhaarCode(String adhaarCode) {
		// TODO Auto-generated method stub
		this.adhaarCode = adhaarCode;
	}

	@Override
	public String getGstNumber() {
		// TODO Auto-generated method stub
		return gstNumber;
	}

	@Override
	public void setGstNumber(String gstNumber) {
		// TODO Auto-generated method stub
		this.gstNumber = gstNumber;
	}

	@Override
	public void setUpdatedDate(Date updatedDate) {
		// TODO Auto-generated method stub
		this.updatedDate = updatedDate;
	}
	
	 
	@Override public Customer getCustomer() 
	{ // TODO Auto-generated method stub
	  return customer; 
	  }
	  
	  @Override public void setCustomer(Customer customer) { // TODO Auto-generated
	  this.customer = customer;
	  
	  }
	  @Override
		public void setAuditable(Auditable auditable) {
			this.auditable = auditable;
	
		}
		
		public Auditable getAuditable() {
			return auditable;
		}
}
