package com.enclothe.core.xref.domain;

import java.io.Serializable;

import com.enclothe.core.dm.order.domain.EncOrder;
import com.enclothe.core.reseller.domain.EncReseller;

public interface EncOrderResellerXref extends Serializable {
	

	public Long getOrder_reseller_id() ;
	public void setOrder_reseller_id(Long order_reseller_id);
	public EncOrder getOrder() ;
	public void setOrder(EncOrder order);

	public EncReseller getReseller();

	public void setReseller(EncReseller reseller);

}
