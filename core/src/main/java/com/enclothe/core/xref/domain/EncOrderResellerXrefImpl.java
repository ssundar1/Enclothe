package com.enclothe.core.xref.domain;

import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.broadleafcommerce.common.persistence.ArchiveStatus;
import org.broadleafcommerce.common.presentation.AdminGroupPresentation;
import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.common.presentation.AdminPresentationClass;
import org.broadleafcommerce.common.presentation.AdminTabPresentation;
import org.broadleafcommerce.common.presentation.PopulateToOneFieldsEnum;
import org.broadleafcommerce.common.presentation.client.VisibilityEnum;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.enclothe.core.customer.domain.EncCustomer;
import com.enclothe.core.customer.domain.EncCustomerImpl;
import com.enclothe.core.dm.order.domain.EncOrder;
import com.enclothe.core.dm.order.domain.EncOrderImpl;
import com.enclothe.core.reseller.domain.EncReseller;
import com.enclothe.core.reseller.domain.EncResellerImpl;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "ENC_ORDER_RESELLER_XREF")
@AdminPresentationClass(populateToOneFields = PopulateToOneFieldsEnum.TRUE, friendlyName = "Order Reseller", tabs = {
		@AdminTabPresentation(name = "General", order = 1000, groups = {
				@AdminGroupPresentation(name = "EncOrderResellerMappingImpl_General_Tab", order = 1000, untitled = true) }),
		@AdminTabPresentation(name = "Advanced", order = 2000, groups = {
				@AdminGroupPresentation(name = "EncOrderResellerMappingImpl_Advanced_Tab", order = 1000, untitled = true) }) })

public class EncOrderResellerXrefImpl implements EncOrderResellerXref {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "EncOrderResellerId")
	@GenericGenerator(name = "EncOrderResellerId", strategy = "org.broadleafcommerce.common.persistence.IdOverrideTableGenerator", parameters = {

			@Parameter(name = "segment_value", value = "EncOrderResellerMappingImpl"),

			@Parameter(name = "entity_name", value = "com.enclothe.core.reseller.domain.EncOrderResellerMappingImpl") })

	@Column(name = "ORDER_RESELLER_ID")
	protected Long order_reseller_id;

	public Long getOrder_reseller_id() {
		return order_reseller_id;
	}

	public void setOrder_reseller_id(Long order_reseller_id) {
		this.order_reseller_id = order_reseller_id;
	}

	public EncOrder getOrder() {
		return order;
	}

	public void setOrder(EncOrder order) {
		this.order = order;
	}

	public EncReseller getReseller() {
		return reseller;
	}

	public void setReseller(EncReseller reseller) {
		this.reseller = reseller;
	}

	public ArchiveStatus getArchiveStatus() {
		return archiveStatus;
	}

	public void setArchiveStatus(ArchiveStatus archiveStatus) {
		this.archiveStatus = archiveStatus;
	}

	@ManyToOne(cascade = { CascadeType.PERSIST,
			CascadeType.MERGE }, targetEntity = EncOrderImpl.class, optional = true)
	@JoinColumn(name = "ORDER_ID")
	@AdminPresentation(excluded = true, visibility = VisibilityEnum.HIDDEN_ALL)
	protected EncOrder order;
	//= new EncOrderImpl() ;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REFRESH }, targetEntity = EncResellerImpl.class, optional = true)
	@JoinColumn(name = "RESELLER_ID")
	// @Index(name="CUSTOMERADDRESS_ADDRESS_INDEX", columnNames={"ADDRESS_ID"})
	//@AdminPresentation(visibility = VisibilityEnum.VISIBLE_ALL)

	protected EncReseller reseller;
	//= new EncResellerImpl();

	@Embedded
	protected ArchiveStatus archiveStatus = new ArchiveStatus();

}
