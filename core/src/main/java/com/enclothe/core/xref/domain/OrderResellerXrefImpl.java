/*
 * package com.enclothe.core.xref.domain;
 * 
 * import javax.persistence.CascadeType; import javax.persistence.Column; import
 * javax.persistence.Entity; import javax.persistence.GeneratedValue; import
 * javax.persistence.Id; import javax.persistence.Inheritance; import
 * javax.persistence.InheritanceType; import javax.persistence.JoinColumn;
 * import javax.persistence.ManyToOne; import javax.persistence.Table;
 * 
 * import org.broadleafcommerce.common.presentation.AdminPresentationClass;
 * import org.broadleafcommerce.core.catalog.domain.Category; import
 * org.broadleafcommerce.core.catalog.domain.CategoryImpl; import
 * org.broadleafcommerce.core.catalog.domain.Product; import
 * org.broadleafcommerce.core.catalog.domain.ProductImpl; import
 * org.hibernate.annotations.Cache; import
 * org.hibernate.annotations.CacheConcurrencyStrategy; import
 * org.hibernate.annotations.GenericGenerator; import
 * org.hibernate.annotations.Parameter;
 * 
 * import com.enclothe.core.dm.order.domain.EncOrder; import
 * com.enclothe.core.dm.order.domain.EncOrderImpl; import
 * com.enclothe.core.reseller.domain.EncReseller; import
 * com.enclothe.core.reseller.domain.EncResellerImpl;
 * 
 * @Entity
 * 
 * @Inheritance(strategy = InheritanceType.JOINED)
 * 
 * @Table(name = "ENC_ORDER_RESELLER_XREF")
 * 
 * @AdminPresentationClass(excludeFromPolymorphism = false)
 * 
 * @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region="enc_orders")
 * public class OrderResellerXrefImpl implements OrderResellerXref {
 * 
 *//** The Constant serialVersionUID. */
/*
 * private static final long serialVersionUID = 1L;
 * 
 * @Id
 * 
 * @GeneratedValue(generator= "CategoryProductId")
 * 
 * @GenericGenerator( name="OrderResellerXrefId",
 * strategy="org.broadleafcommerce.common.persistence.IdOverrideTableGenerator",
 * parameters = {
 * 
 * @Parameter(name="segment_value", value="OrderResellerXrefImpl"),
 * 
 * @Parameter(name="entity_name",
 * value="com.enclothe.core.xref.domain.OrderResellerXrefImpl") } )
 * 
 * @Column(name = "ORDER_RESELLER_XREF_ID") protected Long id;
 * 
 * @ManyToOne(targetEntity = EncOrderImpl.class, optional=false, cascade =
 * CascadeType.REFRESH)
 * 
 * @JoinColumn(name = "ORDER_ID") protected EncOrder order = new EncOrderImpl();
 * 
 *//** The product. *//*
						 * @ManyToOne(targetEntity = EncResellerImpl.class, optional=false, cascade =
						 * CascadeType.REFRESH)
						 * 
						 * @JoinColumn(name = "RESELLER_ID") protected EncReseller reseller = new
						 * EncResellerImpl();
						 * 
						 * 
						 * }
						 */