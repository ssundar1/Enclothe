package org.broadleafcommerce.core.order.service.type;

public class EncOrderStatus extends OrderStatus{
	
	public static final OrderStatus material_pickup = new OrderStatus("material_pickup", "Material Pickup");
	public static final OrderStatus cutting = new OrderStatus("cutting", "Cutting");
	public static final OrderStatus stitching = new OrderStatus("stitching", "Stitching");
	public static final OrderStatus quality_check = new OrderStatus("quality_check", "Quality Check");
	public static final OrderStatus dispatched = new OrderStatus("dispatched", "Dispatched");
	public static final OrderStatus delivered = new OrderStatus("delivered", "Delivered");
	public static final OrderStatus newOrder = new OrderStatus("new", "New");

}
