package org.enclothe.core.catalog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.core.catalog.domain.SkuImpl;

@Entity
@Table(name = "ENCLOTHEFIELDS_SKU")
@PrimaryKeyJoinColumn(name = "SKU_ID")
public class EnclotheFieldsSkuImpl extends SkuImpl{
	
	private static final long serialVersionUID = 1L;	
	
	 @Column(name = "VENDOR_ID")	    
	 @AdminPresentation(friendlyName = "Vendor Id", tab = "Fields", tabOrder = 10000, group = "Other Fields", groupOrder = 1000)
	 protected String vendorId;

	 @Column(name = "LONG_DESC")	    
	 @AdminPresentation(friendlyName = "Long Description", tab = "Fields", tabOrder = 9000, group = "Other Fields", groupOrder = 1000)
	 protected String longDesc;
	 @Column(name = "DELIVERY_DURATIOM")	    
	 @AdminPresentation(friendlyName = "Estimated Turnaround", tab = "Fields", tabOrder = 11000, group = "Other Fields", groupOrder = 1000)
	 protected long deliveryDuration;

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		if(vendorId!=null)
			this.vendorId = vendorId;
		else
			this.vendorId = "";
	}

	public String getLongDesc() {
		return longDesc;
	}

	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}

	public long getDeliveryDuration() {
		return deliveryDuration;
	}

	public void setDeliveryDuration(long deliveryDuration) {
		this.deliveryDuration = deliveryDuration;
	}
	 
	//TODO 

}