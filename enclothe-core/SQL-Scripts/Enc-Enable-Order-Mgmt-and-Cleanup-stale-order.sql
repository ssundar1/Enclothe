
-- To fix the order with non existing customer 4135
UPDATE ENCLOTHE.BLC_ORDER
SET CUSTOMER_ID = 4135
WHERE ORDER_ID=5;
COMMIT;

--enable order management module in admin screens under customer

-- Step 1
use enclothe;
-- 'org.broadleafcommerce.core.order.domain.Order' admin_permssion_id = '-13 - all order and -12 - read order' in BLC_ADMIN_PERMISSION_ENTITY
-- add submenu to the main menu Customer -> order
insert into `blc_admin_section` (`ADMIN_SECTION_ID`, `CEILING_ENTITY`, `DISPLAY_CONTROLLER`, `DISPLAY_ORDER`, `NAME`, `SECTION_KEY`, `URL`, `USE_DEFAULT_HANDLER`, `ADMIN_MODULE_ID`) values('-20','org.broadleafcommerce.core.order.domain.Order',NULL,'1500','Customer Orders','Orders','/Orders',NULL,'-3');
-- For the section give -13 all and 12 - read
use enclothe;
 INSERT INTO BLC_ADMIN_SEC_PERM_XREF (ADMIN_SECTION_ID, ADMIN_PERMISSION_ID) VALUES (-20,-12);
INSERT INTO BLC_ADMIN_SEC_PERM_XREF (ADMIN_SECTION_ID, ADMIN_PERMISSION_ID) VALUES (-20,-13);

-- add admin role i.e -1 to have the permission for order i,e -13
INSERT INTO `enclothe`.`blc_admin_role_permission_xref`
(`ADMIN_ROLE_ID`,`ADMIN_PERMISSION_ID`) VALUES (-1,-13);
commit;


-- Populate the enc_order table with exisiting order data.

INSERT INTO `enclothe`.`enc_order`
(select null,null,null,order_id,null from enclothe.blc_order);
commit;
