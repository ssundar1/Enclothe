
use broadleaf;
INSERT INTO broadleaf.enc_user_category
(USER_CATEGORY_ID, CREATED_BY, USER_CATEGORY_DESCRIPTION, USER_CATEGORY_NAME) 
VALUES (2001, 700, 'USER_CATEGORY_DESCRIPTION', 'USER_CATEGORY_NAME');

INSERT INTO broadleaf.enc_order_item_states
(ORDER_ITEM_STATE_ID, ORDER_ITEM_STATE_SHORT_DESC, CREATED_BY, ORDER_ITEM_STATE_LONG_DESC, ORDER_ITEM_STATE_NAME) 
VALUES (1001, 'test', 700, 'test state', 'test');