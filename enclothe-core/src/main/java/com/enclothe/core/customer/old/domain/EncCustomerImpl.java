package com.enclothe.core.customer.old.domain;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.InheritanceType;

import org.broadleafcommerce.cms.structure.domain.StructuredContentImpl.Presentation;
import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.common.presentation.AdminPresentationMap;
import org.broadleafcommerce.core.geolocation.GeolocationDTO;
import org.broadleafcommerce.profile.core.domain.CustomerImpl;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.enclothe.core.measurement.old.domain.Measurement;
import com.enclothe.core.measurement.old.domain.MeasurementImpl;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "ENC_CUSTOMER_TOBREMOVED")
public class EncCustomerImpl extends CustomerImpl implements EncCustomer {

	private static final long serialVersionUID = 6545097668293683751L;
	
   @OneToMany(mappedBy = "customer", targetEntity = MeasurementImpl.class, cascade = { CascadeType.ALL }, orphanRemoval = true)
    @Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region="blStandardElements")
    @MapKey(name="name")
    @BatchSize(size = 50)
    @AdminPresentationMap(friendlyName = "MeasurementImpl_Measurement_Name",
            deleteEntityUponRemove = true, forceFreeFormKeys = true, keyPropertyFriendlyName = "MeasurementImpl_Measurement_Name",
            tab = Presentation.Tab.Name.Rules, tabOrder = Presentation.Tab.Order.Rules
    )
    protected Map<String, Measurement> measurements = new HashMap<String, Measurement>();

   @Override
   public Map<String, Measurement> getCustomerMeasurements() {
       return this.measurements;
   }

   @Override
   public void setCustomerMeasurements(Map<String, Measurement> measurements) {
	   this.measurements = measurements;
   }
   
   public void addMeasurement (Measurement measurement)
   {
	   Map<String, Measurement> m = this.measurements;
	   m.put(measurement.getName(), measurement);
	   
	   setCustomerMeasurements(m);
   }
   
	@OneToOne(targetEntity=MeasurementImpl.class, optional=true)
	@JoinColumn(name="DEF_MEASUREMENT_ID")	  
	protected Measurement preferredMeasurement;
	  
	/*@OneToOne(targetEntity=EncTailorImpl.class, optional=true)
	@JoinColumn(name="DEF_TAILOR_ID")	  
	protected EncTailor preferredTailor;
	*/
   	public Measurement getPreferredMeasurement() {
		return preferredMeasurement;
	}

	public void setPreferredMeasurement(Measurement preferredMeasurement) {
		this.preferredMeasurement = preferredMeasurement;
	}

	@Column(name = "CUSTOMER_TYPE")
		 @AdminPresentation(friendlyName = "customer_type", tab = "Details", tabOrder = 12000, group = "Reference", groupOrder = 1000)
	private String customer_type;
	
	@Column(name = "AUTHENTICATION_SOURCE")
	 @AdminPresentation(friendlyName = "authentication_source", tab = "Details", tabOrder = 13000, group = "Reference", groupOrder = 1000)
	private String authentication_source;
	
	@Column(name = "AUTHENTICATION_SOURCE_ID")
	 @AdminPresentation(friendlyName = "authentication_source_id", tab = "Details", tabOrder = 14000, group = "Reference", groupOrder = 1000)
	private String authentication_source_id;
	
	@Column(name = "AUTHENTICATION_SOURCE_TOKEN")
	 @AdminPresentation(friendlyName = "authentication_source_token", tab = "Details", tabOrder = 15000, group = "Reference", groupOrder = 1000)
	private String authentication_source_token;
	
	//private GeolocationDTO geolocation;
	@Column(name = "DEVICEMANUFACTURER")
	 @AdminPresentation(friendlyName = "deviceManufacturer", tab = "Details", tabOrder = 16000, group = "Reference", groupOrder = 1000)
	private String deviceManufacturer;
	
	@Column(name = "DEVICEMODEL")
	 @AdminPresentation(friendlyName = "deviceModel", tab = "Details", tabOrder = 17000, group = "Reference", groupOrder = 1000)
	private String deviceModel;
	
	@Column(name = "AUTHTOKEN")
	 @AdminPresentation(friendlyName = "authToken", tab = "Details", tabOrder = 18000, group = "Reference", groupOrder = 1000)
	
	private String authToken;
	
	 @Column(name = "LAST_LOGIN_DATE")
	 @AdminPresentation(friendlyName = "last_login_date", tab = "Details", tabOrder = 19000, group = "Reference", groupOrder = 1000)

    private Date last_login_date;
	 
	 @Column(name = "ISMOBILENOVERIFIED")
	 @AdminPresentation(friendlyName = "isMobileNoVerified", tab = "Details", tabOrder = 20000, group = "Reference", groupOrder = 1000)
	private boolean isMobileNoVerified;

	public String getCustomer_type() {
		return customer_type;
	}

	public void setCustomer_type(String customer_type) {
		this.customer_type = customer_type;
	}

	public String getAuthentication_source() {
		return authentication_source;
	}

	public void setAuthentication_source(String authentication_source) {
		this.authentication_source = authentication_source;
	}

	public String getAuthentication_source_id() {
		return authentication_source_id;
	}

	public void setAuthentication_source_id(String authentication_source_id) {
		this.authentication_source_id = authentication_source_id;
	}

	public String getAuthentication_source_token() {
		return authentication_source_token;
	}

	public void setAuthentication_source_token(String authentication_source_token) {
		this.authentication_source_token = authentication_source_token;
	}

	public String getDeviceManufacturer() {
		return deviceManufacturer;
	}

	public void setDeviceManufacturer(String deviceManufacturer) {
		this.deviceManufacturer = deviceManufacturer;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public Date getLast_login_date() {
		return last_login_date;
	}

	public void setLast_login_date(Date last_login_date) {
		this.last_login_date = last_login_date;
	}

	public boolean isMobileNoVerified() {
		return isMobileNoVerified;
	}

	public void setMobileNoVerified(boolean isMobileNoVerified) {
		this.isMobileNoVerified = isMobileNoVerified;
	} 
	 
	 

	/*public EncTailor getPreferredTailor() {
		return preferredTailor;
	}

	public void setPreferredTailor(EncTailor preferredTailor) {
		this.preferredTailor = preferredTailor;
	}*/
		
}
