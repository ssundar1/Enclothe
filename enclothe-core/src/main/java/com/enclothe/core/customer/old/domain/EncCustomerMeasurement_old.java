package com.enclothe.core.customer.old.domain;

import java.io.Serializable;
import java.util.List;

import org.broadleafcommerce.common.persistence.ArchiveStatus;
import org.broadleafcommerce.profile.core.domain.Customer;

import com.enclothe.core.measurement.old.domain.Measurement;

public interface EncCustomerMeasurement_old extends Serializable {

	void setMeasurements(Measurement measurements);

	public Measurement getMeasurements();
	void setCustomer(EncCustomer customer);
	public EncCustomer getCustomer();
	public Long getId();
	public void setId(Long id) ;

	public String getMeasurementName() ;

	public void setMeasurementName(String measurementName);

	public ArchiveStatus getArchiveStatus();
	public void setArchiveStatus(ArchiveStatus archiveStatus);

}
