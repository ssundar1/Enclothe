package com.enclothe.core.dm.order.old.domain;

import java.util.Date;
import java.util.List;

import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.profile.core.domain.Address;

import com.enclothe.core.customer.old.domain.EncCustomerMeasurement_old;


public interface EncOrder extends Order {

    //public Address getAddress();

    //public void setAddress(Address address);

	public Date getMaterial_pickup_date_time();
	public void setMaterial_pickup_date_time(Date material_pickup_date_time);

    public String getAdditionalInfo();
    
    public void setAdditionalInfo( String additionalInfo );
	
	  public EncCustomerMeasurement_old getCustomerMeasurement() ;
		public String getOrderPaymentReference();
		public void setOrderPaymentReference(String orderPaymentReference);

	  public void setCustomerMeasurement(EncCustomerMeasurement_old
	  customerMeasurement) ;
	 }
