/*
 * package com.enclothe.core.dm.order.old.domain;
 * 
 * import java.util.ArrayList; import java.util.Date; import java.util.List;
 * 
 * import javax.persistence.CascadeType; import javax.persistence.Column; import
 * javax.persistence.Entity; import javax.persistence.FetchType; import
 * javax.persistence.Inheritance; import javax.persistence.InheritanceType;
 * import javax.persistence.JoinColumn; import javax.persistence.ManyToOne;
 * import javax.persistence.OneToMany; import javax.persistence.OneToOne; import
 * javax.persistence.OrderBy; import javax.persistence.Table;
 * 
 * import org.broadleafcommerce.common.locale.domain.Locale; import
 * org.broadleafcommerce.common.locale.domain.LocaleImpl; import
 * org.broadleafcommerce.common.presentation.AdminPresentation; import
 * org.broadleafcommerce.common.presentation.
 * AdminPresentationAdornedTargetCollection; import
 * org.broadleafcommerce.common.presentation.AdminPresentationToOneLookup;
 * import org.broadleafcommerce.common.presentation.client.LookupType; import
 * org.broadleafcommerce.common.presentation.client.VisibilityEnum; import
 * org.broadleafcommerce.common.presentation.override.
 * AdminPresentationMergeEntry; import
 * org.broadleafcommerce.common.presentation.override.
 * AdminPresentationMergeOverride; import
 * org.broadleafcommerce.common.presentation.override.
 * AdminPresentationMergeOverrides; import
 * org.broadleafcommerce.common.presentation.override.
 * AdminPresentationOverrides; import
 * org.broadleafcommerce.common.presentation.override.PropertyType; import
 * org.broadleafcommerce.core.catalog.domain.CategoryProductXref; import
 * org.broadleafcommerce.core.catalog.domain.CategoryProductXrefImpl; import
 * org.broadleafcommerce.core.catalog.domain.CategoryAdminPresentation.TabName;
 * import org.broadleafcommerce.core.order.domain.OrderImpl; import
 * org.broadleafcommerce.profile.core.domain.Address; import
 * org.broadleafcommerce.profile.core.domain.AddressImpl; import
 * org.broadleafcommerce.profile.core.domain.Customer; import
 * org.hibernate.annotations.BatchSize; import org.hibernate.annotations.Cache;
 * import org.hibernate.annotations.CacheConcurrencyStrategy; import
 * org.hibernate.annotations.Formula; import org.hibernate.annotations.Index;
 * import org.hibernate.annotations.JoinFormula; import
 * org.hibernate.annotations.Where;
 * 
 * import com.enclothe.core.customer.old.domain.EncCustomerMeasurement; import
 * com.enclothe.core.customer.old.domain.EncCustomerMeasurementImpl; import
 * com.enclothe.core.measurement.old.domain.Measurement;
 * 
 * @Entity
 * 
 * @Inheritance(strategy = InheritanceType.JOINED)
 * 
 * @Table(name = "ENC_ORDER")
 * 
 * @AdminPresentationMergeOverrides( {
 * 
 * @AdminPresentationMergeOverride(name = "", mergeEntries =
 * 
 * @AdminPresentationMergeEntry(propertyType =
 * PropertyType.AdminPresentation.READONLY, booleanOverrideValue = false)) } )
 * public class EncOrderImpl extends OrderImpl implements EncOrder {
 * 
 * @ManyToOne(cascade = CascadeType.ALL, targetEntity = AddressImpl.class,
 * optional=false)
 * 
 * @JoinColumn(name = "ADDRESS_ID")
 * 
 * @Index(name="CUSTOMERADDRESS_ADDRESS_INDEX", columnNames={"ADDRESS_ID"})
 * protected Address billingAddress;
 * 
 * @Column(name = "MATERIAL_PICKUP_DATE_TIME")
 * 
 * @AdminPresentation(friendlyName = "Payment Reference", tab =
 * "Refernce Details", tabOrder = 11000, group = "Reference", groupOrder = 1000)
 * 
 * protected Date material_pickup_date_time;
 * 
 * @Column(name = "ADDITIONAL_INFO")
 * 
 * @AdminPresentation(friendlyName = "Payment Reference", tab =
 * "Refernce Details", tabOrder = 12000, group = "Reference", groupOrder = 1000)
 * 
 * protected String additionalInfo;
 * 
 * 
 * @Column(name = "ORDER_PAYMENT_REFERENCE")
 * 
 * @AdminPresentation(friendlyName = "Payment Reference", tab =
 * "Refernce Details", tabOrder = 10000, group = "Reference", groupOrder = 1000)
 * public String orderPaymentReference;
 * 
 * 
 * 
 * @ManyToOne(targetEntity =EncCustomerMeasurementImpl.class )
 * 
 * @JoinFormula("(SELECT xref.CUSTOMER_MEASUREMENT_ID from ENC_CUSTOMER_MEASUREMENT_XREF xref where xref.CUSTOMER_ID = ORDER_CUSTOMER_ID order by xref.CUSTOMER_MEASUREMENT_ID desc limit 1)"
 * ) protected EncCustomerMeasurementImpl customerMeasurement ;
 * 
 * 
 * public String getOrderPaymentReference() { return orderPaymentReference; }
 * 
 * public void setOrderPaymentReference(String orderPaymentReference) {
 * this.orderPaymentReference = orderPaymentReference; }
 * 
 * public Date getMaterial_pickup_date_time() { return
 * material_pickup_date_time; }
 * 
 * public void setMaterial_pickup_date_time(Date material_pickup_date_time) {
 * this.material_pickup_date_time = material_pickup_date_time; }
 * 
 * @OneToOne(targetEntity = EncCustomerMeasurementImpl.class,
 * cascade={CascadeType.ALL}, optional = true)
 * 
 * @JoinColumn(name = "Measurement_Xref_Id")
 * 
 * @AdminPresentation(friendlyName = "measurements", tab = "Fields", tabOrder =
 * 10000, group = "Other Fields", groupOrder = 1000)
 * 
 * @AdminPresentationToOneLookup(lookupDisplayProperty = "measurementName")
 * protected EncCustomerMeasurement customerMeasurement ;
 * 
 * 
 * 
 * 
 * @Override public String getAdditionalInfo() { return additionalInfo; }
 * 
 * @Override public void setAdditionalInfo(String additionalInfo) {
 * this.additionalInfo = additionalInfo; }
 * 
 * 
 * 
 * public EncCustomerMeasurement getCustomerMeasurement() { return
 * customerMeasurement; }
 * 
 * public void setCustomerMeasurement(EncCustomerMeasurement
 * customerMeasurement) { this.customerMeasurement = customerMeasurement; }
 * 
 * 
 * }
 */