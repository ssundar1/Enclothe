package com.enclothe.core.dm.order.service.workflow.add;

import java.util.List;

import javax.annotation.Resource;

import org.broadleafcommerce.core.checkout.service.workflow.CheckoutProcessContextFactory;
import org.broadleafcommerce.core.checkout.service.workflow.CheckoutSeed;
import org.broadleafcommerce.core.checkout.service.workflow.CompleteOrderActivity;
import org.broadleafcommerce.core.checkout.service.workflow.CompleteOrderRollbackHandler;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.domain.OrderItem;
import org.broadleafcommerce.core.workflow.ProcessContext;

import com.enclothe.core.dm.order.old.domain.EncOrderItem;
import com.enclothe.core.dm.order.old.domain.EncOrderItemStateDetail;
import com.enclothe.core.dm.order.old.domain.EncOrderItemStates;
import com.enclothe.core.dm.order.service.EncOrderItemService;

public class EncCompleteOrderActivity extends CompleteOrderActivity{
    
    public EncCompleteOrderActivity(CompleteOrderRollbackHandler rollbackHandler) {
		super(rollbackHandler);
		// TODO Auto-generated constructor stub
	}


	@Resource(name = "blEncOrderItemService")
    protected EncOrderItemService orderItemService;
   
   
    @Override
    public ProcessContext execute(ProcessContext context) throws Exception {
    	super.execute(context);
        CheckoutSeed seed = (CheckoutSeed) context.getSeedData();

        Order encOrder = seed.getOrder();
        List<OrderItem> encOrderItems = encOrder.getOrderItems();
        
        //For each Order Item add a state detail
        for (OrderItem orderItem: encOrderItems)
        {
        	//Retrieve Submitted State
        	EncOrderItemStates state = 
        			orderItemService.readOrderItemStatesById(EncOrderItemStates.SUBMITTED);
        	
        	//create order item state detail and save
        	EncOrderItem encOrderItem = (EncOrderItem) orderItem;
        	EncOrderItemStateDetail orderItemStateDetail = 
        			orderItemService.createOrderItemStateDetailFromId(null);
        	
        	orderItemStateDetail.setOrderItemState(state);        	
        	encOrderItem.setOrderItemState(state);
        	orderItemStateDetail.setOrderItem(encOrderItem);
        	
        	//Save Order Item State Detail
        	orderItemService.saveOrderItemStateDetail(orderItemStateDetail);
        	
        	//Save Order Item
        	encOrderItem.setOrderItemStateDetail(orderItemStateDetail);
        	orderItemService.saveOrderItem(encOrderItem);
        	//stateDetail.setCurrentOwner(paramLong);        	
        	//orderItemService.saveOrderItemStateDetail(orderItemStateDetail);       	        
        }
        return context;
    }
}
