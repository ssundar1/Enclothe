package com.enclothe.core.serviceprovider.domain;

import org.broadleafcommerce.core.search.domain.IndexField;
//import org.broadleafcommerce.common.value.Searchable;
import org.broadleafcommerce.core.search.domain.SearchConfig;
//import org.broadleafcommerce.common.value.

public interface ServiceProviderAttribute extends IndexField{

	Long getId();
	
	void setId(Long id);
	
	ServiceProvider getServiceProvider();
	
	void setServiceProvider(ServiceProvider sp);

	String getValue();

	void setValue(String value);

	Boolean getSearchable();

	void setSearchable(Boolean searchable);
		
}
