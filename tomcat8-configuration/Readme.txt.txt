
Step 1) server.xml - in your tomcat server.xml add the below ssl connector.
for your own keystore, create a new key store aand replace the credentials mentioned below.

<Connector SSLEnabled="true" clientAuth="false" keystoreFile="[/path/to/blc-example.keystore]" keystorePass="broadleaf" keyPass="broadleaf" maxThreads="150" port="8443" protocol="HTTP/1.1" scheme="https" secure="true" sslProtocol="TLS"/>
use the default key store availabe in codebase for ssl authentication
src/main/resources/blc-example.keystore

Step 2) Context.xml
uncomment the below line
<Manager pathname="" />

Step 3) Adminapplication.java 

AdminApplication would subclass BroadleafBootServletContextInitializer
to treat the mainclass as a servlet and
override  Configure method with below code.
@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AdminApplication.class);
    }
Step 4)
Admins pom.xml, change the packaging type to "war".

Step 5) generate the war with mvn install

Step 6) install admin.war - move the generated war file to tomcat/webapps.

Step 7) Start the server